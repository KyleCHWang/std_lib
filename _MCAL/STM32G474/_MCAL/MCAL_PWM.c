/*
 * PWM.c
 *
 *  Created on: Jun 25, 2019
 *      Author: KYLE.CH.WANG
 */
#include "MCAL_PWM.h"
#include "IQ_Math.h"
#include "TypeDef.h"
// user configuration=========================================================
#include "stm32g4xx_hal.h"
volatile UINT32* pwm_regs_PRD[PWM_CHL_MAX] =
{
    (UINT32*)(&TIM4->ARR),
    (UINT32*)(&TIM2->ARR),
};
volatile UINT32* pwm_regs_DBL[PWM_CHL_MAX] =
{
  0,
  0,
};
volatile UINT32* pwm_regs_DBF[PWM_CHL_MAX] =
{
    (UINT32*)(&TIM4->CCR1),
    (UINT32*)(&TIM2->CCR1),
};
volatile UINT32* pwm_regs_DC[PWM_CHL_MAX] =
{
    (UINT32*)(&TIM4->CCR1),
    (UINT32*)(&TIM2->CCR1), 
};	
UINT16 pwm_regs_PRD_temp[PWM_CHL_MAX] = {
    10000,
    10000,
};
UINT16 pwm_regs_DBL_temp[PWM_CHL_MAX] = {
    3000,
    3000,

};
UINT16 pwm_regs_DBF_temp[PWM_CHL_MAX] = {
    9000,
    9000,
};
UINT8 pwm_regs_ONOFF_temp[PWM_CHL_MAX] = {
    1,//off
    1,
};
//MCU initialization code
volatile  MCAL_PWM_t pwm_settings[PWM_CHL_MAX];
UINT8 PWMUpdate = 0;
//============================================================================

void API_PWM_Config_period_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempfreq){

    UINT64 tempU64;
    tempU64 = (UINT64)((BASE_FREQ_PRD)/tempfreq);
    pwm_settings[tempchannel].period_ticks_max = tempU64;

}

void API_PWM_Config_period_ticks_min(enumPWM_CHL_t tempchannel, UINT32 tempfreq){

    UINT64 tempU64;
    tempU64 = (UINT64)((BASE_FREQ_PRD)/tempfreq);
    pwm_settings[tempchannel].period_ticks_min = tempU64;

}

void API_PWM_Config_SR_period_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempfreq){

    UINT64 tempU64;
    tempU64 = (UINT64)((BASE_FREQ_PRD)/tempfreq);
    pwm_settings[tempchannel].period_ticks_SR_max = tempU64;

}

void API_PWM_Config_deadtime_lead_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempps){

    UINT64 tempU64;
    tempU64 = (UINT64)((tempps)/TIME_STEP);
    pwm_settings[tempchannel].deadtime_lead_ticks_max = tempU64;

}

void API_PWM_Config_deadtime_lead_ticks_min(enumPWM_CHL_t tempchannel, UINT32 tempps){

    UINT64 tempU64;
    tempU64 = (UINT64)((tempps)/TIME_STEP);
    pwm_settings[tempchannel].deadtime_lead_ticks_min = tempU64;

}

void API_PWM_Config_deadtime_follow_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempps){

    UINT64 tempU64;
    tempU64 = (UINT64)((tempps)/TIME_STEP);
    pwm_settings[tempchannel].deadtime_follow_ticks_max = tempU64;

}

void API_PWM_Config_deadtime_follow_ticks_min(enumPWM_CHL_t tempchannel, UINT32 tempps){

    UINT64 tempU64;
    tempU64 = (UINT64)((tempps)/TIME_STEP);
   // tempU64 = (tempps)/TIME_STEP;
    pwm_settings[tempchannel].deadtime_follow_ticks_min = tempU64;
}
void API_PWM_Config_dutycycle_ticks_min(enumPWM_CHL_t tempchannel, UINT32 ticks){

	pwm_settings[tempchannel].dutycycle_ticks_min = ticks;
}
void API_PWM_Config_dutycycle_ticks_max(enumPWM_CHL_t tempchannel, UINT32 ticks){

	pwm_settings[tempchannel].dutycycle_ticks_max = ticks;
}


void API_PWM_AllOFF(void){

	UINT8 cnt = 0;
	for(cnt=0;cnt<PWM_CHL_MAX;cnt++){
		pwm_regs_ONOFF_temp[cnt] = 1;
	} 
}

void API_PWM_AllON(void){

    UINT8 cnt = 0;
	for(cnt=0;cnt<PWM_CHL_MAX;cnt++){
		pwm_regs_ONOFF_temp[cnt] = 0;
	}
}

void API_PWM_SRON(void){// not in use


}

void API_PWM_SROFF(void){// not in use

    
}

UINT32 API_PWM_DutyCalc(enumPWM_CHL_t tempchannel, UINT16 out){
	
	UINT32 tempduty_ticks;
	tempduty_ticks  = (UINT32)pwm_settings[tempchannel].dutycycle_ticks_min;
    tempduty_ticks  += ((UINT32)(pwm_settings[tempchannel].dutycycle_ticks_max - pwm_settings[tempchannel].dutycycle_ticks_min)*out>>16ul);
    return tempduty_ticks;
}
void API_PWM_Set_Duty(enumPWM_CHL_t tempchannel, UINT32 ticks){

	*pwm_regs_DC[tempchannel] = ticks;
}


UINT32 API_PWM_PeriodCalc(enumPWM_CHL_t tempchannel, UINT16 tempperiod){//not in use

    UINT32 tempperiod_ticks;
    //MCU APIs
    //tempperiod_ticks = (UINT32)(pwm_settings[tempchannel].period_ticks_min + _IQmpy((pwm_settings[tempchannel].period_ticks_max - pwm_settings[tempchannel].period_ticks_min),tempperiod));

    tempperiod_ticks  = (UINT32)pwm_settings[tempchannel].period_ticks_min;
    tempperiod_ticks  += ((UINT32)(pwm_settings[tempchannel].period_ticks_max - pwm_settings[tempchannel].period_ticks_min)*tempperiod>>16ul);
    
    return tempperiod_ticks;
}


void API_PWM_Update(enumPWM_CHL_t tempchannel,UINT32 tempperiod,UINT32 tempdeadtime_lead,UINT32 tempdeadtime_follow,UINT32 tempalpha,UINT32 tempgamma,INT32 temptheta){

    if(tempdeadtime_lead > pwm_settings[tempchannel].deadtime_lead_ticks_max){
        tempdeadtime_lead = pwm_settings[tempchannel].deadtime_lead_ticks_max;
    }
    if(tempdeadtime_lead < pwm_settings[tempchannel].deadtime_lead_ticks_min){
        tempdeadtime_lead = pwm_settings[tempchannel].deadtime_lead_ticks_min;
    }
    if(tempdeadtime_follow > pwm_settings[tempchannel].deadtime_follow_ticks_max){
        tempdeadtime_follow = pwm_settings[tempchannel].deadtime_follow_ticks_max;
    }
    if(tempdeadtime_follow < pwm_settings[tempchannel].deadtime_follow_ticks_min){
        tempdeadtime_follow = pwm_settings[tempchannel].deadtime_follow_ticks_min;
    }
    tempdeadtime_follow = tempperiod  - tempdeadtime_follow;
    pwm_regs_PRD_temp[tempchannel] = tempperiod;
    pwm_regs_DBL_temp[tempchannel] = tempdeadtime_lead;
    pwm_regs_DBF_temp[tempchannel] = tempdeadtime_follow;

}


