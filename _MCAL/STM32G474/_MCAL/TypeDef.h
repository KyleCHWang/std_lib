/**************************************************************************************
*	File:			TypeDef.h
*	Description:	Type definition header file
*	Device:			TMS320F28034
*
*	Change Log:
*	/Date			/Author				/Description
*	2012/11/13		PAUL.DS.CHEN		File created
*	2012/11/21		Phil Lee			Format changed
**************************************************************************************/

#ifndef TYPEDEF_H_
#define TYPEDEF_H_

//==================================================
//		Type Definition
//==================================================
typedef unsigned char		BOOL;
typedef unsigned char		BYTE;
typedef unsigned char		WORD;

typedef unsigned char		UINT8;
typedef char				INT8;

typedef unsigned short		UINT16;
typedef short				INT16;

typedef unsigned long		UINT32;
typedef long				INT32;

typedef unsigned long long	UINT64;
typedef long long			INT64;


typedef enum{
    _OFF,
    _ON,
} ONOFF_t;

typedef enum{
    _NOK,
    _OK,
} OK_t;

//==================================================
//		Structure Definition
//==================================================
//----- Flag -----
typedef union {
    UINT32 all;                                     // All
    struct {
        UINT16 b0:1;                                // Bit0
        UINT16 b1:1;                                // Bit1
        UINT16 b2:1;                                // Bit2
        UINT16 b3:1;                                // Bit3
        UINT16 b4:1;                                // Bit4
        UINT16 b5:1;                                // Bit5
        UINT16 b6:1;                                // Bit6
        UINT16 b7:1;                                // Bit7
        UINT16 b8:1;                                // Bit8
        UINT16 b9:1;                                // Bit9
        UINT16 b10:1;                               // Bit10
        UINT16 b11:1;                               // Bit11
        UINT16 b12:1;                               // Bit12
        UINT16 b13:1;                               // Bit13
        UINT16 b14:1;                               // Bit14
        UINT16 b15:1;                               // Bit15
        UINT16 b16:1;                                // Bit0
        UINT16 b17:1;                                // Bit1
        UINT16 b18:1;                                // Bit2
        UINT16 b19:1;                                // Bit3
        UINT16 b20:1;                                // Bit4
        UINT16 b21:1;                                // Bit5
        UINT16 b22:1;                                // Bit6
        UINT16 b23:1;                                // Bit7
        UINT16 b24:1;                                // Bit8
        UINT16 b25:1;                                // Bit9
        UINT16 b26:1;                               // Bit10
        UINT16 b27:1;                               // Bit11
        UINT16 b28:1;                               // Bit12
        UINT16 b29:1;                               // Bit13
        UINT16 b30:1;                               // Bit14
        UINT16 b31:1;                               // Bit15
    } bit;
} BF_U32;
typedef union {
	UINT16 all;										// All
	struct {
		UINT16 b0:1;								// Bit0
		UINT16 b1:1;								// Bit1
		UINT16 b2:1;								// Bit2
		UINT16 b3:1;								// Bit3
		UINT16 b4:1;								// Bit4
		UINT16 b5:1;								// Bit5
		UINT16 b6:1;								// Bit6
		UINT16 b7:1;								// Bit7
		UINT16 b8:1;								// Bit8
		UINT16 b9:1;								// Bit9
		UINT16 b10:1;								// Bit10
		UINT16 b11:1;								// Bit11
		UINT16 b12:1;								// Bit12
		UINT16 b13:1;								// Bit13
		UINT16 b14:1;								// Bit14
		UINT16 b15:1;								// Bit15
	} bit;
} BF_U16;

typedef union {
	UINT8 all;										// All
	struct {
		UINT8 b0:1;									// Bit0
		UINT8 b1:1;									// Bit1
		UINT8 b2:1;									// Bit2
		UINT8 b3:1;									// Bit3
		UINT8 b4:1;									// Bit4
		UINT8 b5:1;									// Bit5
		UINT8 b6:1;									// Bit6
		UINT8 b7:1;									// Bit7
	} bit;
} BF_U8;

//----- Nibble -----
typedef union {
	UINT16 all;										// All
	struct {
		UINT16 n0:4;								// Nibble0
		UINT16 n1:4;								// Nibble1
		UINT16 n2:4;								// Nibble2
		UINT16 n3:4;								// Nibble3
	} nibble;
} NIBBLE_TYPE;

//----- Signed Double Word -----
typedef union {
	INT32 dword;									// Double word
	struct {
		UINT16 lo;									// Low word
		INT16 hi;									// High word
	} word;
} SDW_TYPE;

//----- Unsigned Double Word -----
typedef union {
	UINT32 dword;									// Double word
	struct {
		UINT16 lo;									// Low word
		UINT16 hi;									// High word
	} word;
} UDW_TYPE;

//----- CAN Bus message -----
typedef union{
	UINT32 all;										// All
	struct {
		UINT8 n0:8;									// UINT8 Byte
		UINT8 n1:8;									// UINT8 Byte
		UINT8 n2:8;									// UINT8 Byte
		UINT8 n3:8;									// UINT8 Byte
	} byte;
} BYTE_TYPE;

//----- CAN Bus message -----
typedef struct{
	BYTE_TYPE data_lo;								// Message data low
	BYTE_TYPE data_hi;								// Message data high
} DATA_TAPE;

//----- CAN Bus message -----
typedef union{
	UINT32 all;										// All
	struct {
		   UINT16	EXTMSGID_L:16;  				// 0:15
		   UINT16   EXTMSGID_H:2;   				// 16:17
		   UINT16   STDMSGID:11;    				// 18:28
		   UINT16   AAM:1;          				// 29
		   UINT16   AME:1;          				// 30
		   UINT16   IDE:1;          				// 31
		}word;
} ID_TYPE;

//==================================================
//		Constant Definition
//==================================================

#define OFF					0
#define ON					1

#define MAX_UINT8			0xFF
#define MAX_UINT16			0xFFFF
#define MAX_UINT32			0xFFFFFFFF

#ifndef SQRT_2
#define SQRT_2				1.414213562
#endif

#ifndef SQRT_2_DIV_2
#define SQRT_2_DIV_2		0.707106781
#endif

//==================================================
//		IQmath Macro
//==================================================
#define _Q0(X)				((INT32)(1 * (X)))
#define _Q1(X)				((INT32)(2 * (X)))
#define _Q2(X)				((INT32)(4 * (X)))
#define _Q3(X)				((INT32)(8 * (X)))
#define _Q4(X)				((INT32)(16 * (X)))
#define _Q5(X)				((INT32)(32 * (X)))
#define _Q6(X)				((INT32)(64 * (X)))
#define _Q7(X)				((INT32)(128 * (X)))
#define _Q8(X)				((INT32)(256 * (X)))
#define _Q9(X)				((INT32)(512 * (X)))
#define _Q10(X)				((INT32)(1024 * (X)))
#define _Q11(X)				((INT32)(2048 * (X)))
#define _Q12(X)				((INT32)(4096 * (X)))
#define _Q13(X)				((INT32)(8192 * (X)))
#define _Q14(X)				((INT32)(16384 * (X)))
#define _Q15(X)				((INT32)(32768 * (X)))
#define _Q16(X)				((INT32)(65536 * (X)))
#define _Q17(X)				((INT32)(131072 * (X)))
#define _Q18(X)				((INT32)(262144 * (X)))
#define _Q19(X)				((INT32)(524288 * (X)))
#define _Q20(X)				((INT32)(1048576 * (X)))
#define _Q21(X)				((INT32)(2097152 * (X)))
#define _Q22(X)				((INT32)(4194304 * (X)))
#define _Q23(X)				((INT32)(8388608 * (X)))
#define _Q24(X)				((INT32)(16777216 * (X)))
#define _Q25(X)				((INT32)(33554432 * (X)))
#define _Q26(X)				((INT32)(67108864 * (X)))
#define _Q27(X)				((INT32)(134217728 * (X)))
#define _Q28(X)				((INT32)(268435456 * (X)))
#define _Q29(X)				((INT32)(536870912 * (X)))
#define _Q30(X)				((INT32)(1073741824 * (X)))
#define _Q31(X)				((INT32)(2147483648 * (X)))

#endif		// TYPEDEF_H_

/**************************************************************************************
	The end of file
**************************************************************************************/
