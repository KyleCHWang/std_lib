/*
 * MCAL_UART.h
 *
 *  Created on: Jun 26, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef MCAL_CAN_H_
#define MCAL_CAN_H_
#include "TypeDef.h"

typedef enum
{
    CAN_CHL_BAT,  // Battery CAN
    CAN_CHL_MAX,

} enumCAN_CHL_t;
typedef enum{

	CAN_ID_STD,
	CAN_ID_EXT,
	CAN_ID_MAX,
} CAN_IDType_t;
typedef enum{
	
	CAN_FRAME_DATA,
	CAN_FRAME_REMOTE,
	CAN_FRAME_MAX,

} CAN_FrameType_t;
	
extern void API_CAN_Config_RxBuffer(enumCAN_CHL_t tempchannel, UINT8* ptrU8dataarray);
extern void API_CAN_Config_TxBuffer(enumCAN_CHL_t tempchannel, UINT8* ptrU8dataarray);
extern UINT32 API_CAN_Get_RxID(enumCAN_CHL_t tempchannel);
extern CAN_IDType_t API_CAN_Get_RxIDtype(enumCAN_CHL_t tempchannel);
extern CAN_FrameType_t API_CAN_Get_RxFrameType(enumCAN_CHL_t tempchannel);
extern UINT16 API_CAN_Get_RxDataLength(enumCAN_CHL_t tempchannel);
extern void API_CAN_SendData(enumCAN_CHL_t tempchannel,CAN_IDType_t IDtype, UINT32 ID, UINT16 datalength);
extern void API_CAN_RxReset(enumCAN_CHL_t tempchannel);
extern UINT8 API_CAN_GetTxFIFOFreeLevel(enumCAN_CHL_t tempchannel);
extern void API_CAN_AutoReInit(enumCAN_CHL_t tempchannel);




#endif /* MCAL_MCAL_UART_H_ */
