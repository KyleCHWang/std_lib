/*
 * MCAL_CAN.c
 *
 *  Created on: Jan 6, 2020
 *      Author: KYLE.CH.WANG
 */
#include "MCAL_CAN.h"
#include "TypeDef.h"



static UINT8* can_ptrrxdatabuffer[CAN_CHL_MAX];
static UINT8* can_ptrtxdatabuffer[CAN_CHL_MAX];
static UINT8 CANBusOFF[CAN_CHL_MAX];

// User Define function===================================================
#include "main.h"
extern FDCAN_HandleTypeDef hfdcan1;
extern void Active_can(void);
extern void MX_FDCAN1_Init(void);

FDCAN_RxHeaderTypeDef CAN_RxHeader[CAN_CHL_MAX];
FDCAN_TxHeaderTypeDef CAN_TxHeader[CAN_CHL_MAX];
static FDCAN_HandleTypeDef* CAN_channel[CAN_CHL_MAX] = {
    &hfdcan1,
};
void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
  if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_NEW_MESSAGE) != RESET)
  {
    /* Retrieve Rx messages from RX FIFO0 */
    if (HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &CAN_RxHeader[CAN_CHL_BAT], can_ptrrxdatabuffer[CAN_CHL_BAT]) != HAL_OK)
    {
    	Error_Handler();
    }
  }
}

void HAL_FDCAN_ErrorStatusCallback(FDCAN_HandleTypeDef *hfdcan, uint32_t ErrorStatusITs)
{  
  /* Prevent unused argument(s) compilation warning */
  //UNUSED(hfdcan);
  //UNUSED(ErrorStatusITs);
  UINT32 CAN_ErrorStatusITs;
  CAN_ErrorStatusITs = ErrorStatusITs;
  
  if((CAN_ErrorStatusITs & FDCAN_IT_BUS_OFF) != 0){
    if(hfdcan->Instance == FDCAN1){
      CANBusOFF[CAN_CHL_BAT] = 1;
    }
  }  
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_FDCAN_ErrorStatusCallback could be implemented in the user file
   */
}

//=========================================================================
void API_CAN_Config_RxBuffer(enumCAN_CHL_t tempchannel, UINT8* ptrU8dataarray){

    can_ptrrxdatabuffer[tempchannel] = ptrU8dataarray;

}
void API_CAN_Config_TxBuffer(enumCAN_CHL_t tempchannel, UINT8* ptrU8dataarray){

    can_ptrtxdatabuffer[tempchannel] = ptrU8dataarray;

}
UINT32 API_CAN_Get_RxID(enumCAN_CHL_t tempchannel){
	return CAN_RxHeader[tempchannel].Identifier;
}
CAN_IDType_t API_CAN_Get_RxIDtype(enumCAN_CHL_t tempchannel){

	if(CAN_RxHeader[tempchannel].IdType == FDCAN_STANDARD_ID){
		return CAN_ID_STD;
	}
	if(CAN_RxHeader[tempchannel].IdType == FDCAN_EXTENDED_ID){
		return CAN_ID_EXT;
	}
	else{
		return CAN_ID_MAX;
	}
}
CAN_FrameType_t API_CAN_Get_RxFrameType(enumCAN_CHL_t tempchannel){

	if(CAN_RxHeader[tempchannel].RxFrameType == FDCAN_DATA_FRAME){
		return CAN_FRAME_DATA;
	}
	if(CAN_RxHeader[tempchannel].RxFrameType == FDCAN_REMOTE_FRAME){
		return CAN_FRAME_REMOTE;
	}
	else{
		return CAN_FRAME_MAX;
	}
}
UINT16 API_CAN_Get_RxDataLength(enumCAN_CHL_t tempchannel){
	return CAN_RxHeader[tempchannel].DataLength>>16;
}
void API_CAN_SendData(enumCAN_CHL_t tempchannel,CAN_IDType_t IDtype, UINT32 ID, UINT16 datalength){

	if(IDtype == CAN_ID_EXT){
		CAN_TxHeader[tempchannel].IdType = FDCAN_EXTENDED_ID;
	}
	else if(IDtype == CAN_ID_STD){
		CAN_TxHeader[tempchannel].IdType = FDCAN_STANDARD_ID;
	}
	if(datalength<=8){
		CAN_TxHeader[tempchannel].DataLength = (datalength<<16);
	}
	else{
		return;
	}
	CAN_TxHeader[tempchannel].Identifier = ID;  	
  	CAN_TxHeader[tempchannel].TxFrameType = FDCAN_DATA_FRAME;
  	CAN_TxHeader[tempchannel].ErrorStateIndicator = FDCAN_ESI_ACTIVE;
  	CAN_TxHeader[tempchannel].BitRateSwitch = FDCAN_BRS_OFF;
  	CAN_TxHeader[tempchannel].FDFormat = FDCAN_CLASSIC_CAN;
  	CAN_TxHeader[tempchannel].TxEventFifoControl = FDCAN_NO_TX_EVENTS;
  	CAN_TxHeader[tempchannel].MessageMarker = 0;
	HAL_FDCAN_AddMessageToTxFifoQ(CAN_channel[tempchannel], &CAN_TxHeader[tempchannel], can_ptrtxdatabuffer[tempchannel]);

}
void API_CAN_AutoReInit(enumCAN_CHL_t tempchannel){
  volatile HAL_FDCAN_StateTypeDef CANStatus;
  volatile uint32_t CANErrorCode;
  CANStatus = HAL_FDCAN_GetState(CAN_channel[tempchannel]);
  CANErrorCode = HAL_FDCAN_GetError(CAN_channel[tempchannel]);
  if((CANStatus == HAL_FDCAN_STATE_ERROR) || (CANErrorCode != HAL_FDCAN_ERROR_NONE) || (CANBusOFF[CAN_CHL_BAT] == 1)){
        HAL_FDCAN_Stop(&hfdcan1);
        HAL_FDCAN_DeInit(&hfdcan1);
        MX_FDCAN1_Init();
        Active_can();
        CANBusOFF[CAN_CHL_BAT] = 0;
  }

}
UINT8 API_CAN_GetTxFIFOFreeLevel(enumCAN_CHL_t tempchannel){
	UINT32 u32FreeLevel;
	
	u32FreeLevel=HAL_FDCAN_GetTxFifoFreeLevel(CAN_channel[tempchannel]);
	return u32FreeLevel;
}

