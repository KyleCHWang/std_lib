/*
 * ADC.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef MCAL_MCAL_ADC_H_
#define MCAL_MCAL_ADC_H_

#include "TypeDef.h"

//Configuration
#define ADC_RESOLUTION 4096
typedef enum
{
    ADC_CHL_VOUT_DET,  		//LV Battery voltage
    ADC_CHL_IOUT_DET,  		//LLC Temperature
    ADC_CHL_NTC_EXT,   		//LV DC bus voltage
    ADC_CHL_BAT_DET,     	//LV Charge Current
    ADC_CHL_NTC_LLC,  	    //LV Charge Current
    ADC_CHL_NTC_RELAY,      //LV Charge Current
    ADC_CHL_MAX,

} enumADC_CHL_t;

//API
extern UINT16 API_ADC_Get_rawdata(enumADC_CHL_t tempchannel);

#endif /* MCAL_MCAL_ADC_H_ */
