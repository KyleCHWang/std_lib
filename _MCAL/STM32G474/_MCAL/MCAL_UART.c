/*
 * UART.c
 *
 *  Created on: Jun 26, 2019
 *      Author: KYLE.CH.WANG
 */
#include "MCAL_UART.h"
#include "TypeDef.h"


static UINT8* ptrrxdatabuffer[UART_CHL_MAX];
static UINT16 rxdatalength[UART_CHL_MAX];

static UINT8* ptrtxdatabuffer[UART_CHL_MAX];
static UINT16 txdatalength[UART_CHL_MAX];

// User Define function===================================================
#include "main.h"
#include "user_uart.h"
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;
extern UART_HandleTypeDef huart1;

extern UART_HandleTypeDef huart2;
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;


static UART_HandleTypeDef* UART_channel[UART_CHL_MAX] = {
    
    &huart1,
	&huart2,

};

static DMA_HandleTypeDef* UART_DMA_Rxchannel[UART_CHL_MAX] = {
    
    &hdma_usart1_rx,
	&hdma_usart2_rx,

};

static DMA_HandleTypeDef* UART_DMA_Txchannel[UART_CHL_MAX] = {
    
    &hdma_usart1_tx,
	&hdma_usart2_tx,

};
//=========================================================================

void API_UART_Config_RxBuffer(enumUART_CHL_t tempchannel, UINT8* ptrU8dataarray, UINT8 U8datalength){

    ptrrxdatabuffer[tempchannel] = ptrU8dataarray;
    rxdatalength[tempchannel] = U8datalength;

}
void API_UART_Config_TxBuffer(enumUART_CHL_t tempchannel, UINT8* ptrU8dataarray, UINT8 U8datalength){

    ptrtxdatabuffer[tempchannel] = ptrU8dataarray;
    txdatalength[tempchannel] = U8datalength;

}
void API_UART_RecieveData(enumUART_CHL_t tempchannel){
    
	HAL_UART_Receive_DMA(UART_channel[tempchannel], ptrrxdatabuffer[tempchannel], rxdatalength[tempchannel]);
	__HAL_DMA_DISABLE_IT(UART_DMA_Rxchannel[tempchannel], DMA_IT_TC);
	__HAL_DMA_DISABLE_IT(UART_DMA_Rxchannel[tempchannel], DMA_IT_HT); 
            
}

void API_UART_SendData(enumUART_CHL_t tempchannel){

	HAL_UART_AbortTransmit(UART_DMA_Txchannel[tempchannel]);
	HAL_UART_Transmit_DMA(UART_channel[tempchannel], ptrtxdatabuffer[tempchannel], txdatalength[tempchannel]);
	__HAL_DMA_DISABLE_IT(UART_DMA_Txchannel[tempchannel], DMA_IT_TC);
	__HAL_DMA_DISABLE_IT(UART_DMA_Txchannel[tempchannel], DMA_IT_HT);  
            
}
void API_UART_RxReset(enumUART_CHL_t tempchannel){

	HAL_UART_AbortReceive(UART_DMA_Rxchannel[tempchannel]);
	HAL_UART_Receive_DMA(UART_channel[tempchannel], ptrrxdatabuffer[tempchannel], rxdatalength[tempchannel]);
	__HAL_DMA_DISABLE_IT(UART_DMA_Rxchannel[tempchannel], DMA_IT_TC);
	__HAL_DMA_DISABLE_IT(UART_DMA_Rxchannel[tempchannel], DMA_IT_HT); 

}

