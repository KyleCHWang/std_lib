/*
 * ADC.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#include <MCAL_ADC.h>
#include "TypeDef.h"
//MCU configuration==========================================================
volatile UINT32 ADCresultarray[ADC_CHL_MAX];
UINT16 API_ADC_Get_rawdata(enumADC_CHL_t tempchannel){

    return (UINT16)(ADCresultarray[tempchannel]);
}
