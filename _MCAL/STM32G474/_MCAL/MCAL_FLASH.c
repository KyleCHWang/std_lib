/*
 * MCAL_FLASH.c
 *
 *  Created on: Mar 11, 2021
 *      Author: KYLE.CH.WANG
 */
#include "MCAL_FLASH.h"
#include "TypeDef.h"

// User Define function===================================================
#include "user_flash.h"
//=========================================================================
void API_FLASH_ErasePageWait(UINT32 Addr, UINT32 Npages){
	Flash_erase(Addr, Npages);
	delay_ms(10);
}
void API_FLASH_ReadWait(UINT32 Addr, UINT64* pData, UINT32 Size){
	Flash_read(Addr,pData,Size);
	delay_ms(2);
}
void API_FLASH_WriteWait(UINT32 Addr, UINT64* pData, UINT32 Size){
	Flash_write(Addr,pData,Size);
	delay_ms(2);
}
