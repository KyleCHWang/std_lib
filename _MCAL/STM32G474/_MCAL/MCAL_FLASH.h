/*
 * MCAL_FLASH.h
 *
 *  Created on: Mar 11, 2021
 *      Author: KYLE.CH.WANG
 */

#ifndef MCAL_FLASH_H
#define MCAL_FLASH_H
#include "TypeDef.h"

void API_FLASH_ErasePageWait(UINT32 Addr, UINT32 Npages);
void API_FLASH_ReadWait(UINT32 Addr, UINT64* pData, UINT32 Size);
void API_FLASH_WriteWait(UINT32 Addr, UINT64* pData, UINT32 Size);

#endif /* MCAL_FLASH_H_ */
