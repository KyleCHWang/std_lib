/*
 * Input capture.c
 *
 *  Created on: July 24, 2020
 *      Author: KYLE.CH.WANG
 */
#include "MCAL_IC.h"
#include "IQ_Math.h"
#include "TypeDef.h"
// user configuration=========================================================
#include "stm32g4xx_hal.h"
volatile UINT32* ICRegsCNT[IC_CHL_MAX] =
{
    &TIM2->CNT
};
volatile UINT32* ICRegsCompare[IC_CHL_MAX] =
{
    &TIM2->CCR2
};

//============================================================================
UINT32 API_IC_GET_COMPARE(enumIC_CHL_t ICChannel){
	
	return *ICRegsCompare[ICChannel];
}

void API_IC_RESET_CNT(enumIC_CHL_t ICChannel){
	
	*ICRegsCNT[ICChannel] = 0;
}

