/*
 * MCAL_UART.h
 *
 *  Created on: Jun 26, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef MCAL_MCAL_UART_H_
#define MCAL_MCAL_UART_H_
#include "TypeDef.h"



typedef enum
{
	UART_CHL_PRI,  // diagnostic
    UART_CHL_BLEnDIAG,  // diagnostic
    UART_CHL_MAX,

} enumUART_CHL_t;
extern void API_UART_Config_RxBuffer(enumUART_CHL_t tempchannel, UINT8* ptrU8dataarray, UINT8 U8datalength);
extern void API_UART_Config_TxBuffer(enumUART_CHL_t tempchannel, UINT8* ptrU8dataarray, UINT8 U8datalength);
extern void API_UART_RecieveData(enumUART_CHL_t tempchannel);
extern void API_UART_SendData(enumUART_CHL_t tempchannel);
extern void API_UART_RxReset(enumUART_CHL_t tempchannel);


#endif /* MCAL_MCAL_UART_H_ */
