/*
 * GPIO.h
 *
 *  Created on: Jun 26, 2019
 *      Author: KYLE.CH.WANG
 */
 
#ifndef MCAL_GPIO_H_
#define MCAL_GPIO_H_

#include "TypeDef.h"

//User define function=================================================
#include "stm32g4xx_hal.h"

#define UC2_VOUT_DET__Pin GPIO_PIN_0
#define UC2_VOUT_DET_GPIO_Port GPIOA
#define UC2_IOUT_DET_Pin GPIO_PIN_1
#define UC2_IOUT_DET_GPIO_Port GPIOA
#define NTC_EXT_Pin GPIO_PIN_2
#define NTC_EXT_GPIO_Port GPIOA
#define UC2_BAT_DET_Pin GPIO_PIN_3
#define UC2_BAT_DET_GPIO_Port GPIOA
#define UC2_OTP_LLC_Pin GPIO_PIN_4
#define UC2_OTP_LLC_GPIO_Port GPIOA
#define UC2_OTP_RELAY_Pin GPIO_PIN_5
#define UC2_OTP_RELAY_GPIO_Port GPIOA
#define HW_PROTECT_Pin GPIO_PIN_1
#define HW_PROTECT_GPIO_Port GPIOB
#define U2_RELAY_Pin GPIO_PIN_2
#define U2_RELAY_GPIO_Port GPIOB
#define LED6_Pin GPIO_PIN_11		//fault LED
#define LED6_GPIO_Port GPIOB
#define LED5_Pin GPIO_PIN_12
#define LED5_GPIO_Port GPIOB
#define LED4_Pin GPIO_PIN_13		//Charging LED
#define LED4_GPIO_Port GPIOB
#define LED3_Pin GPIO_PIN_14
#define LED3_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_15		// Complete LED
#define LED2_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOA
#define UC2_Tx_Pin GPIO_PIN_9
#define UC2_Tx_GPIO_Port GPIOA
#define UC2_Rx_Pin GPIO_PIN_10
#define UC2_Rx_GPIO_Port GPIOA
#define CAN_Rx_Pin GPIO_PIN_11
#define CAN_Rx_GPIO_Port GPIOA
#define CAN_Tx_Pin GPIO_PIN_12
#define CAN_Tx_GPIO_Port GPIOA
#define CV_ADJ_Pin GPIO_PIN_15
#define CV_ADJ_GPIO_Port GPIOA
#define BLE_TX_Pin GPIO_PIN_3
#define BLE_TX_GPIO_Port GPIOB
#define BLE_RX_Pin GPIO_PIN_4
#define BLE_RX_GPIO_Port GPIOB
#define EN_SR_Pin GPIO_PIN_5
#define EN_SR_GPIO_Port GPIOB
#define CC_ADJ_Pin GPIO_PIN_6
#define CC_ADJ_GPIO_Port GPIOB
#define EEPROM_WP_Pin GPIO_PIN_7
#define EEPROM_WP_GPIO_Port GPIOB
#define EEPROM_SCL_Pin GPIO_PIN_8
#define EEPROM_SCL_GPIO_Port GPIOB
#define EEPROM_SDA_Pin GPIO_PIN_9
#define EEPROM_SDA_GPIO_Port GPIOB

#define RESET_Pin GPIO_PIN_6
#define RESET_GPIO_Port GPIOA

//====================================================================


#define SET_GPIO_HW_PROTECT (HW_PROTECT_GPIO_Port->BSRR = HW_PROTECT_Pin)
#define CLR_GPIO_HW_PROTECT (HW_PROTECT_GPIO_Port->BRR = HW_PROTECT_Pin) 
#define DAT_GPIO_HW_PROTECT (HW_PROTECT_GPIO_Port->IDR & HW_PROTECT_Pin) ? 1 : 0

#define SET_GPIO_VBAT_ADJ (CV_ADJ_GPIO_Port->BSRR = CV_ADJ_Pin)
#define CLR_GPIO_VBAT_ADJ (CV_ADJ_GPIO_Port->BRR = CV_ADJ_Pin) 
#define DAT_GPIO_VBAT_ADJ (CV_ADJ_GPIO_Port->IDR & CV_ADJ_Pin) ? 1 : 0

#define SET_GPIO_IBAT_ADJ (CC_ADJ_GPIO_Port->BSRR = CC_ADJ_Pin)
#define CLR_GPIO_IBAT_ADJ (CC_ADJ_GPIO_Port->BRR = CC_ADJ_Pin) 
#define DAT_GPIO_IBAT_ADJ (CC_ADJ_GPIO_Port->IDR & CC_ADJ_Pin) ? 1 : 0

#define SET_GPIO_LED1 (LED1_GPIO_Port->BSRR = LED1_Pin)
#define CLR_GPIO_LED1 (LED1_GPIO_Port->BRR = LED1_Pin) 
#define DAT_GPIO_LED1 (LED1_GPIO_Port->IDR & LED1_Pin) ? 1 : 0

#define SET_GPIO_LED2 (LED2_GPIO_Port->BSRR = LED2_Pin)
#define CLR_GPIO_LED2 (LED2_GPIO_Port->BRR = LED2_Pin) 
#define DAT_GPIO_LED2 (LED2_GPIO_Port->IDR & LED2_Pin) ? 1 : 0

#define SET_GPIO_LED3 (LED3_GPIO_Port->BSRR = LED3_Pin)
#define CLR_GPIO_LED3 (LED3_GPIO_Port->BRR = LED3_Pin) 
#define DAT_GPIO_LED3 (LED3_GPIO_Port->IDR & LED3_Pin) ? 1 : 0

#define SET_GPIO_LED4 (LED4_GPIO_Port->BSRR = LED4_Pin)
#define CLR_GPIO_LED4 (LED4_GPIO_Port->BRR = LED4_Pin) 
#define DAT_GPIO_LED4 (LED4_GPIO_Port->IDR & LED4_Pin) ? 1 : 0

#define SET_GPIO_LED5 (LED5_GPIO_Port->BSRR = LED5_Pin)
#define CLR_GPIO_LED5 (LED5_GPIO_Port->BRR = LED5_Pin) 
#define DAT_GPIO_LED5 (LED5_GPIO_Port->IDR & LED5_Pin) ? 1 : 0

#define SET_GPIO_LED6 (LED6_GPIO_Port->BSRR = LED6_Pin)
#define CLR_GPIO_LED6 (LED6_GPIO_Port->BRR = LED6_Pin) 
#define DAT_GPIO_LED6 (LED6_GPIO_Port->IDR & LED6_Pin) ? 1 : 0

#define SET_GPIO_ORING (U2_RELAY_GPIO_Port->BSRR = U2_RELAY_Pin)
#define CLR_GPIO_ORING (U2_RELAY_GPIO_Port->BRR = U2_RELAY_Pin) 
#define DAT_GPIO_ORING (U2_RELAY_GPIO_Port->IDR & U2_RELAY_Pin) ? 1 : 0

//Primary I/O expander
#define SET_GPIO_LLC_EN PMIC.GPO.bits.EN_LLC = 1
#define CLR_GPIO_LLC_EN PMIC.GPO.bits.EN_LLC = 0
#define DAT_GPIO_LLC_EN PMIC.GPO.bits.EN_LLC

#define SET_GPIO_PFC_EN PMIC.GPO.bits.UC1_PFC_EN = 1
#define CLR_GPIO_PFC_EN PMIC.GPO.bits.UC1_PFC_EN = 0
#define DAT_GPIO_PFC_EN PMIC.GPO.bits.UC1_PFC_EN

#define SET_GPIO_PTC_RELAY PMIC.GPO.bits.UC1_RELAY = 1
#define CLR_GPIO_PTC_RELAY PMIC.GPO.bits.UC1_RELAY = 0
#define DAT_GPIO_PTC_RELAY PMIC.GPO.bits.UC1_RELAY

#define SET_GPIO_HWRESET (RESET_GPIO_Port->BSRR = RESET_Pin)
#define CLR_GPIO_HWRESET (RESET_GPIO_Port->BRR = RESET_Pin) 
#define DAT_GPIO_HWRESET (RESET_GPIO_Port->IDR & RESET_Pin) ? 1 : 0

#define SET_GPIO_VCCS2_EN (EN_SR_GPIO_Port->BSRR = EN_SR_Pin)
#define CLR_GPIO_VCCS2_EN (EN_SR_GPIO_Port->BRR = EN_SR_Pin) 
#define DAT_GPIO_VCCS2_EN (EN_SR_GPIO_Port->IDR & EN_SR_Pin) ? 1 : 0

#define SET_GPIO_BLETx (BLE_TX_GPIO_Port->BSRR = BLE_TX_Pin)
#define CLR_GPIO_BLETx (BLE_TX_GPIO_Port->BRR = BLE_TX_Pin) 
#define DAT_GPIO_BLETx (BLE_TX_GPIO_Port->IDR & BLE_TX_Pin) ? 1 : 0

#define SET_GPIO_BLERx (BLE_RX_GPIO_Port->BSRR = BLE_RX_Pin)
#define CLR_GPIO_BLERx (BLE_RX_GPIO_Port->BRR = BLE_RX_Pin) 
#define DAT_GPIO_BLERx (BLE_RX_GPIO_Port->IDR & BLE_RX_Pin) ? 1 : 0

//No function
//#define SET_GPIO_EEPVcc (uc_EEPVcc_GPIO_Port->BSRR = uc_EEPVcc_Pin)
//#define CLR_GPIO_EEPVcc (uc_EEPVcc_GPIO_Port->BRR = uc_EEPVcc_Pin) 
//#define DAT_GPIO_EEPVcc (uc_EEPVcc_GPIO_Port->IDR & uc_EEPVcc_Pin) ? 1 : 0

#define SET_GPIO_EEP_SCL (EEPROM_SCL_GPIO_Port->BSRR = EEPROM_SCL_Pin)
#define CLR_GPIO_EEP_SCL (EEPROM_SCL_GPIO_Port->BRR = EEPROM_SCL_Pin) 
#define DAT_GPIO_EEP_SCL (EEPROM_SCL_GPIO_Port->IDR & EEPROM_SCL_Pin) ? 1 : 0

#define SET_GPIO_EEP_SDA (EEPROM_SDA_GPIO_Port->BSRR = EEPROM_SDA_Pin)
#define CLR_GPIO_EEP_SDA (EEPROM_SDA_GPIO_Port->BRR = EEPROM_SDA_Pin) 
#define DAT_GPIO_EEP_SDA (EEPROM_SDA_GPIO_Port->IDR & EEPROM_SDA_Pin) ? 1 : 0

#define SET_GPIO_CAN_Rx (CAN_Rx_GPIO_Port->BSRR = CAN_Rx_Pin)
#define CLR_GPIO_CAN_Rx (CAN_Rx_GPIO_Port->BRR = CAN_Rx_Pin) 
#define DAT_GPIO_CAN_Rx (CAN_Rx_GPIO_Port->IDR & CAN_Rx_Pin) ? 1 : 0

#define SET_GPIO_CAN_Tx (CAN_Tx_GPIO_Port->BSRR = CAN_Tx_Pin)
#define CLR_GPIO_CAN_Tx (CAN_Tx_GPIO_Port->BRR = CAN_Tx_Pin) 
#define DAT_GPIO_CAN_Tx (CAN_Tx_GPIO_Port->IDR & CAN_Tx_Pin) ? 1 : 0


//No function
//#define SET_GPIO_CHARGERSTOP (uc_ChargerStop_GPIO_Port->BSRR = uc_ChargerStop_Pin)
//#define CLR_GPIO_CHARGERSTOP (uc_ChargerStop_GPIO_Port->BRR = uc_ChargerStop_Pin) 
//#define DAT_GPIO_CHARGERSTOP (uc_ChargerStop_GPIO_Port->IDR & uc_ChargerStop_Pin) ? 1 : 0

//APIs
//extern void API_GPIO_SetBit(enumGPIO_CHL_t tempdata,enumGPIO_ONOFF tempBool);
//extern enumGPIO_ONOFF API_GPIO_GetBit(enumGPIO_CHL_t tempdata);
#endif /* MCAL_GPIO_H_ */
