/*
 * PWM.h
 *
 *  Created on: Jun 25, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef MCAL_MCAL_PWM_H_
#define MCAL_MCAL_PWM_H_
#include "TypeDef.h"
#include "IQ_Math.h"

//User definition=================================
#include <math.h>
#define PicoS 1000000000000.0
#define BASE_FREQ 170000.0 
#define BASE_FREQ_PRD 170000.0 //PUSH-PULL MODE
#define TIME_STEP (PicoS/BASE_FREQ)
typedef enum
{
    PWM_CHL_IADJ,
    PWM_CHL_VADJ,
    PWM_CHL_MAX,

} enumPWM_CHL_t;
extern volatile UINT32* pwm_regs_PRD[PWM_CHL_MAX];
extern volatile UINT32* pwm_regs_DBL[PWM_CHL_MAX];
extern volatile UINT32* pwm_regs_DBF[PWM_CHL_MAX];
extern UINT16 pwm_regs_PRD_temp[PWM_CHL_MAX];
extern UINT16 pwm_regs_DBL_temp[PWM_CHL_MAX];
extern UINT16 pwm_regs_DBF_temp[PWM_CHL_MAX];
extern UINT8 pwm_regs_ONOFF_temp[PWM_CHL_MAX];
extern UINT8 PWMUpdate;
//================================================


typedef struct{

    //configuration
    UINT16 period_ticks_max;
    //UINT16 period_ticks_max_hr; //used in TI MCU
    UINT16 period_ticks_min;
    //UINT16 period_ticks_min_hr; //used in TI MCU
    UINT16 period_ticks_SR_max;
    //UINT16 SR period_ticks_max_hr; //used in TI MCU
    UINT16 deadtime_lead_ticks_max;
    //UINT8  deadtime_lead_ticks_max_hr;  //used in TI MCU
    UINT16 deadtime_lead_ticks_min;
    //UINT8  deadtime_lead_ticks_min_hr;  //used in TI MCU
    UINT16 deadtime_follow_ticks_max;
    //UINT8  deadtime_follow_ticks_max_hr;    //used in TI MCU
    UINT16 deadtime_follow_ticks_min;
    //UINT8  deadtime_follow_ticks_min_hr;    //used in TI MCU
    UINT16 dutycycle_ticks_min;
	UINT16 dutycycle_ticks_max;
    //instance


} MCAL_PWM_t;

extern volatile  MCAL_PWM_t pwm_settings[PWM_CHL_MAX];
extern void API_PWM_OutputEnalbe(UINT8 tempU8);
extern void API_PWM_OutputDisalbe(UINT8 tempU8);
extern void API_PWM_Config_period_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempfreq);
extern void API_PWM_Config_period_ticks_min(enumPWM_CHL_t tempchannel, UINT32 tempfreq);
extern void API_PWM_Config_SR_period_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempfreq);
extern void API_PWM_Config_deadtime_lead_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempps);
extern void API_PWM_Config_deadtime_lead_ticks_min(enumPWM_CHL_t tempchannel, UINT32 tempps);
extern void API_PWM_Config_deadtime_follow_ticks_max(enumPWM_CHL_t tempchannel, UINT32 tempps);
extern void API_PWM_Config_deadtime_follow_ticks_min(enumPWM_CHL_t tempchannel, UINT32 tempps);
extern void API_PWM_Config_dutycycle_ticks_min(enumPWM_CHL_t tempchannel, UINT32 ticks);
extern void API_PWM_Config_dutycycle_ticks_max(enumPWM_CHL_t tempchannel, UINT32 ticks);
extern void API_PWM_Update(enumPWM_CHL_t tempchannel,UINT32 tempperiod,UINT32 tempdeadtime_lead,UINT32 tempdeadtime_follow,UINT32 tempalpha,UINT32 tempgamma,INT32 temptheta);
extern void API_PWM_DutyMode_Update(enumPWM_CHL_t tempchannel,UINT32 tempperiod,UINT32 tempdeadtime_lead,UINT32 tempdeadtime_follow,UINT32 tempalpha,UINT32 tempgamma,INT32 temptheta);
extern UINT32 API_PWM_PeriodCalc(enumPWM_CHL_t tempchannel, UINT16 tempperiod);
extern void API_PWM_AllOFF(void);
extern void API_PWM_AllON(void);
extern void API_PWM_SRON(void);
extern void API_PWM_SROFF(void);
extern UINT32 API_PWM_DutyCalc(enumPWM_CHL_t tempchannel, UINT16 out);
extern void API_PWM_Set_Duty(enumPWM_CHL_t tempchannel, UINT32 ticks);


#else
#endif /* MCAL_MCAL_PWM_H_ */

