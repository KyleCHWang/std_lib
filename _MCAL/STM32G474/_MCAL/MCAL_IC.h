/*
 * PWM.h
 *
 *  Created on: Jun 25, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef MCAL_MCAL_IC_H_
#define MCAL_MCAL_IC_H_
#include "TypeDef.h"
#include "IQ_Math.h"

//User definition=================================
#include <math.h>
typedef enum
{
    IC_CHL_FAN,
    IC_CHL_MAX,

} enumIC_CHL_t;

#else
#endif /* MCAL_MCAL_IC_H_ */

