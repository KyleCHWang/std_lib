/*
 * Fan.h
 *
 *  Created on: Feb 7, 2020
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_FAN_H_
#define BSW_FAN_H_
#include "TypeDef.h"
#include "VoltageRegulator.h"
#include "PIDex.h"
#include "MCAL_IC.h"


typedef enum
{
    Fan_OFF,
    Fan_ON,
}FAN_ONOFF_t;
	
typedef struct{

    //Configuration
    VoltageRegulator_t* VR;
	UINT8 pulse;
	float clock;
    //Instance
    FAN_ONOFF_t  ONOFF;
	UINT8 FGState;
	UINT32 IC1;
	UINT32 IC2;
	UINT16 duty;
	float rpm;
	UINT8 fail;

} Fan_t;

extern void API_Fan_Config_VR(Fan_t* tempFan, VoltageRegulator_t* vr);
extern void API_Fan_Config_Clock(Fan_t* tempFan, float clock);
extern void API_Fan_Config_Pulse(Fan_t* tempFan, UINT8 pulse);
extern void API_Fan_Set_Duty(Fan_t* tempFan, UINT16 duty);
extern void API_Fan_SpeedCalc(Fan_t* tempFan);
extern void API_Fan_Fail(Fan_t* tempFan);
extern float API_Fan_Get_rpm(Fan_t* tempFan);
extern UINT8 API_Fan_Get_fail(Fan_t* tempFan);

#endif /* Fan_H_ */
