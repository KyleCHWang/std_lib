/*
 * EasyUART.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "EasyUART.h"
#include "CRC.h"
//User define function=========================================================
#include "Object.h"
void UDF_EZUART_TxFrameUpadate(EasyUART_t* tempinstance){
	UINT16 CRC16 = 0;
	tempinstance->txdatabuffer[0] = 0xFE;
	tempinstance->txdatabuffer[1] = 0xED;
	tempinstance->txdatabuffer[2] = PMIC.GPO.byte;
    tempinstance->txdatabuffer[3] = PMIC.Protect_EN.byte;
	tempinstance->txdatabuffer[4] = 0x00;
	tempinstance->txdatabuffer[5] = 0x00;
	tempinstance->txdatabuffer[6] = 0x00;
	tempinstance->txdatabuffer[7] = 0x00;
	tempinstance->txdatabuffer[8] = 0x00;
	tempinstance->txdatabuffer[9] = 0x00;	
	tempinstance->txdatabuffer[10] = 0x00;
	tempinstance->txdatabuffer[11] = 0x00;
	tempinstance->txdatabuffer[12] = 0x00;
	tempinstance->txdatabuffer[13] = 0x00;
	CRC16 = API_CRC16_0xFFFF(tempinstance->txdatabuffer, EZUART_TXBYTE_MAX-4);
	tempinstance->txdatabuffer[EZUART_TXBYTE_MAX-4] = (CRC16>>8);
	tempinstance->txdatabuffer[EZUART_TXBYTE_MAX-3] = (CRC16&0xFF);
	tempinstance->txdatabuffer[EZUART_TXBYTE_MAX-2] = 0xAB;
	tempinstance->txdatabuffer[EZUART_TXBYTE_MAX-1] = 0xAB;
}
void UDF_EZUART_RxDecode(EasyUART_t* tempinstance){
	PMIC.ADC_AC_POWER_DET_AVG = (tempinstance->rxdatabuffer[4]<<8) + tempinstance->rxdatabuffer[5];
	PMIC.ADC_VBoost_DET = (tempinstance->rxdatabuffer[6]<<8) + tempinstance->rxdatabuffer[7];
	PMIC.ADC_T_PFCMOS =  (tempinstance->rxdatabuffer[8]<<8) + tempinstance->rxdatabuffer[9];
	PMIC.ADC_T_LLCMOS =  (tempinstance->rxdatabuffer[10]<<8) + tempinstance->rxdatabuffer[11];
	PMIC.ADC_T_BRIDGE =  (tempinstance->rxdatabuffer[12]<<8) + tempinstance->rxdatabuffer[13];
	PMIC.Fault.byte = tempinstance->rxdatabuffer[3];
}
//=============================================================================
void RxBufferReset(EasyUART_t* tempinstance){
    UINT8 Cnt = 0;
    for(Cnt=0;Cnt<EZUART_RXBYTE_MAX;Cnt++){
        tempinstance->rxdatabuffer[Cnt] = 0;
    }
}
void API_EZUART_Config_UARTChannel(EasyUART_t* tempinstance, enumUART_CHL_t tempU8){

    tempinstance->UARTchannel = tempU8;
    API_UART_Config_RxBuffer(tempinstance->UARTchannel, tempinstance->rxdatabuffer, (UINT16)sizeof(tempinstance->rxdatabuffer));
    API_UART_Config_TxBuffer(tempinstance->UARTchannel, tempinstance->txdatabuffer, (UINT16)(sizeof(tempinstance->txdatabuffer)));
	API_UART_RecieveData(tempinstance->UARTchannel); //Start recieving
}

void API_EZUART_RxTask(EasyUART_t* tempinstance)
{
    UINT16 CRC16 = 0;
    UINT16 CRC16_H = 0,CRC16_L = 0;
	tempinstance->delaycnt[1]++;
	
	if ((tempinstance->rxdatabuffer[0] == 0xBE) && (tempinstance->rxdatabuffer[1] == 0xEF)&&(tempinstance->rxdatabuffer[EZUART_RXBYTE_MAX-1] == 0xAB)&&(tempinstance->rxdatabuffer[EZUART_RXBYTE_MAX-2] == 0xAB))
    {
    	CRC16 = API_CRC16_0xFFFF(tempinstance->rxdatabuffer , (EZUART_RXBYTE_MAX-4));
        CRC16_H = CRC16>>8;
        CRC16_L = CRC16&0xFF;
    	if((tempinstance->rxdatabuffer[EZUART_RXBYTE_MAX-3] == CRC16_L) && (tempinstance->rxdatabuffer[EZUART_RXBYTE_MAX-4] == CRC16_H)){
			UDF_EZUART_RxDecode(tempinstance);
			tempinstance->delaycnt[1] = 0;		
		}
		else{
			CRC16 = 0;
		}
		RxBufferReset(tempinstance);
    }
	
	if(tempinstance->delaycnt[1] >= RX_TIMEOUT){
		RxBufferReset(tempinstance);
		API_UART_RxReset(tempinstance->UARTchannel);
		tempinstance->delaycnt[1] = 0;
	}
   
}

void API_EZUART_TxTask(EasyUART_t* tempinstance){
	tempinstance->delaycnt[0]++;
	if(tempinstance->delaycnt[0] >= TX_PERIOD){
	    UDF_EZUART_TxFrameUpadate(tempinstance);
	    API_UART_SendData(tempinstance->UARTchannel);
		tempinstance->delaycnt[0] = 0;
    }
}
