#include "DigitalFilter.h"

#include "TypeDef.h"
/**
  * @brief  performs digital filter of a not filtered variable (unsigned16)
  * @param  hNoFilteredValue not filtered input variable
  * @param  *phLastFilteredValue pointer to last filtered variable
  * @param  bDigitShift order of filter (n in the following formula)
  * @retval hFilteredValue  filtered value of input varible
  *
  * The digital filter applies the formula: yk = y(k-1) - (y(k-1)>>n) + (xk>>n)
*/

UINT16 uSimpleDigitalLowPassFilter(UINT16 hNoFilteredValue, UINT16 *phLastFilteredValue, UINT8 bDigitShift)
{
  UINT16 hFilteredValue = *phLastFilteredValue;
  UINT32 wAux = 0;

  /* yk = y(k-1) - (y(k-1)>>n) + (xk>>n) */
//  filteredValue = filteredValue - (filteredValue >> digitShift) + (noFilteredValue >> digitShift);
  wAux = (hFilteredValue << bDigitShift) - hFilteredValue + hNoFilteredValue;
  wAux = (wAux >> bDigitShift);
  hFilteredValue = (UINT16)wAux;
//  *phLastFilteredValue = hFilteredValue;           // no update: the new value is returned by function

  return hFilteredValue;
}

/**
  * @brief  performs digital filter of a not filtered variable (signed16)
  * @param  hNoFilteredValue not filtered input variable
  * @param  *phLastFilteredValue pointer to last filtered variable
  * @param  bDigitShift order of filter (n in the following formula)
  * @retval hFilteredValue  filtered value of input varible
  *
  * The digital filter applies the formula: yk = y(k-1) - (y(k-1)>>n) + (xk>>n)
*/

INT16 sSimpleDigitalLowPassFilter(INT16 hNoFilteredValue, INT16 *phLastFilteredValue, INT8 bDigitShift)
{
  INT16 hFilteredValue = *phLastFilteredValue;
  INT32 wAux = 0;

  /* yk = y(k-1) - (y(k-1)>>n) + (xk>>n) */
//  filteredValue = filteredValue - (filteredValue >> digitShift) + (noFilteredValue >> digitShift);
//  wAux = (hFilteredValue << bDigitShift) - hFilteredValue + hNoFilteredValue;

  wAux = (hFilteredValue << bDigitShift);
  wAux -= hFilteredValue;
  wAux += hNoFilteredValue;
  wAux = (wAux >> bDigitShift);
  hFilteredValue = (INT16)wAux;

  return hFilteredValue;

//  wAux = (hFilteredValue << bDigitShift) - hFilteredValue + hNoFilteredValue;
//  wAux = (wAux >> bDigitShift);
//  return (int16_t)wAux;
}
