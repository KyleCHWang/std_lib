/*
 * TempSensor.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#include "TypeDef.h"
#include "MCAL_ADC.h"
#include "DigitalFilter.h"
#include "TempSensor.h"

void API_TS_Config_ADCchanel(TemperatureSensor_t* tempinstance,enumADC_CHL_t tempdata){

    if(tempdata<ADC_CHL_MAX){
        tempinstance->ADCchanel = tempdata;
    }
}
void API_TS_Config_Tmax(TemperatureSensor_t* tempinstance,float max){

	tempinstance->Tmax = max;
	
}
void API_TS_Config_Tmin(TemperatureSensor_t* tempinstance,float min){

	tempinstance->Tmin = min;

}
void API_TS_Config_T0index(TemperatureSensor_t* tempinstance,UINT8 index){

	tempinstance->T0index = index;

}
void API_TS_Config_Ttable(TemperatureSensor_t* tempinstance,const UINT16* table){

	tempinstance->Ttable = table;

}
void API_TS_Config_Ttablesize(TemperatureSensor_t* tempinstance,UINT8 size){

	tempinstance->Ttablesize = size;

}
void API_TS_Update(TemperatureSensor_t* tempinstance){
	UINT8 index = 0;
	float tempfloat = 0;

	for(index=0;index<tempinstance->Ttablesize;index++){
		if(tempinstance->Ttable[index] >= tempinstance->valueQ12_filtered){
			break;
		}
	}
	
	if(tempinstance->Ttable[index]!=tempinstance->Ttable[index-1]){

		tempfloat = (float)(tempinstance->valueQ12_filtered-tempinstance->Ttable[index-1])/(tempinstance->Ttable[index]-tempinstance->Ttable[index-1]);
	}
	tempfloat = ((index-1) + tempfloat) - tempinstance->T0index;
	if(tempfloat>=tempinstance->Tmax){
		tempfloat = tempinstance->Tmax;
	}
	else if(tempfloat<=tempinstance->Tmin){
		tempfloat = tempinstance->Tmin;
	}
	tempinstance->Cels = tempfloat;
	tempinstance->Fahr = (tempinstance->Cels * 1.8f) + 32.0f;
}
void API_TS_Update_N(TemperatureSensor_t* tempinstance){	//Negative update
	UINT8 index = 0;
	float tempfloat = 0;

	for(index=0;index<tempinstance->Ttablesize;index++){
		if(tempinstance->Ttable[index] <= tempinstance->valueQ12_filtered){
			break;
		}
	}
	
	if(tempinstance->Ttable[index]!=tempinstance->Ttable[index-1]){

		tempfloat = (float)(tempinstance->Ttable[index-1]-tempinstance->valueQ12_filtered)/(tempinstance->Ttable[index-1]-tempinstance->Ttable[index]);
	}
	tempfloat = ((index-1) + tempfloat) - (tempinstance->T0index-1);
	if(tempfloat>=tempinstance->Tmax){
		tempfloat = tempinstance->Tmax;
	}
	else if(tempfloat<=tempinstance->Tmin){
		tempfloat = tempinstance->Tmin;
	}
	tempinstance->Cels = tempfloat;
	tempinstance->Fahr = (tempinstance->Cels * 1.8f) + 32.0f;
}


float API_TS_Get_Cels(TemperatureSensor_t* tempinstance){

	return tempinstance->Cels;

}

