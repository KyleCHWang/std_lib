/*
 * PIDex.c
 *
 *  Created on: Jul 16, 2019
 *      Author: KYLE.CH.WANG
 */

#include "TypeDef.h"
#include "PIDex.h"

#define U8_MAX     ((UINT8)255)
#define S8_MAX     ((INT8)127)
#define S8_MIN     ((INT8)-127)
#define U16_MAX    ((UINT16)65535u)
#define S16_MAX    ((INT16)32767)
#define S16_MIN    ((INT16)-32767)
#define U32_MAX    ((UINT32)4294967295uL)
#define S32_MAX    ((INT32)2147483647)
#define S32_MIN    ((INT32)-2147483647)

void API_PID_Reset(PID_Struct_test_t *PID_Struct){

    PID_Struct->wError = 0;
    PID_Struct->wIntegral = 0;
    PID_Struct->wPreviousError = 0;
}
INT32 API_PID_Regulator_ex(INT16 hReference, INT16 hPresentFeedback, PID_Struct_test_t *PID_Struct)
 {
    INT32 wError, wProportional_Term, wIntegral_Term, wOutput_32, wIntegral_sum_temp;
    INT32 wDischarge = 0;
    INT32 wUpperOutputLimit = PID_Struct->wUpper_Limit_Output;
    INT32 wLowerOutputLimit = PID_Struct->wLower_Limit_Output;

 #ifdef DIFFERENTIAL_TERM_ENABLED_EX
    INT32 wDifferential_Term;
 #endif
   // error computation
   wError = (INT32)(hReference - hPresentFeedback);
   PID_Struct->wError = wError;
   // Proportional term computation
   wProportional_Term = PID_Struct->hKp_Gain * wError;

   // Integral term computation
   if (PID_Struct->hKi_Gain == 0)
   {
     PID_Struct->wIntegral = 0;
   }
   else
   {
     wIntegral_Term = PID_Struct->hKi_Gain * wError;
     wIntegral_sum_temp = PID_Struct->wIntegral + wIntegral_Term;

if (wIntegral_sum_temp < 0)
     {
      if (PID_Struct->wIntegral > 0)
      {
        if (wIntegral_Term > 0)
        {
          wIntegral_sum_temp = S32_MAX;
        }
      }
    }
    else
    {
      if (PID_Struct->wIntegral < 0)
      {
        if (wIntegral_Term < 0)
        {
          wIntegral_sum_temp = -S32_MAX;
        }
      }
    }

     if (wIntegral_sum_temp > PID_Struct->wUpper_Limit_Integral)
     {
       PID_Struct->wIntegral = PID_Struct->wUpper_Limit_Integral;
     }
     else if (wIntegral_sum_temp < PID_Struct->wLower_Limit_Integral)
     {
       PID_Struct->wIntegral = PID_Struct->wLower_Limit_Integral;
     }
     else
     {
      PID_Struct->wIntegral = wIntegral_sum_temp;
     }

   }
  // Differential term computation
 #ifdef DIFFERENTIAL_TERM_ENABLED_EX
   {
   INT32 wtemp;

   wtemp = wError - PID_Struct->wPreviousError;
   wDifferential_Term = PID_Struct->hKd_Gain * wtemp;
   PID_Struct->wPreviousError = wError;    // store value
   }
   wOutput_32 = (wProportional_Term/PID_Struct->hKp_Divisor+
                 PID_Struct->wIntegral/PID_Struct->hKi_Divisor +
                 wDifferential_Term/PID_Struct->hKd_Divisor);

 #else
   wOutput_32 = (wProportional_Term/PID_Struct->hKp_Divisor+
                 PID_Struct->wIntegral/PID_Struct->hKi_Divisor);
 #endif

  if (wOutput_32 > wUpperOutputLimit)
  {
    wDischarge = wUpperOutputLimit - wOutput_32;
    wOutput_32 = wUpperOutputLimit;
  }
  else if (wOutput_32 < wLowerOutputLimit)
  {
    wDischarge = wLowerOutputLimit - wOutput_32;
    wOutput_32 = wLowerOutputLimit;
  }
  else
  {}

  PID_Struct->wIntegral += wDischarge;
  return wOutput_32;
 }
