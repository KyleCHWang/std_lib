/*
 * Oring.h
 *
 *  Created on: Aug 16, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_COMPONENT_ORING_H_
#define BSW_COMPONENT_ORING_H_
#include "TypeDef.h"
typedef enum
{
    Oring_OFF,
    Oring_ON,
    Oring_Precharge,
    Oring_Precharge_OK,
}OringStatus_t;
typedef struct{

    //Configuration
    float lowthreshold;
    float upthreshold;
    float uplimit;
    UINT16 dutymax;
    //Instance
    UINT16 cnt;
    UINT16 duty;
    OringStatus_t status;

} Oring_t;

extern void API_Oring_Config_dutymax(Oring_t* tempOring, UINT16 dutymax);
extern void API_Oring_Config_uplimit(Oring_t* tempOring, float uplimit);
extern void API_Oring_Config_upthreshold(Oring_t* tempOring, float upthreshold);
extern void API_Oring_Config_lowthreshold(Oring_t* tempOring, float lowthreshold);
extern void API_Oring_Control(Oring_t* tempOring, float input, float target);
extern void API_Oring_Control_Q12(Oring_t* tempOring, UINT16 input, UINT16 target);
extern void API_Oring_ON(Oring_t* tempOring);
extern void API_Oring_OFF(Oring_t* tempOring);
extern UINT8 API_Oring_Get_status(Oring_t* tempOring);
extern void API_Oring_Precharge(Oring_t* tempOring);
extern void API_Oring_Set_duty(Oring_t* tempOring, UINT16 dutycycle);
extern void API_Oring_PrechargeControl(Oring_t* tempOring, UINT16 input, UINT16 target);
extern void API_Oring_Reset(Oring_t* tempOring);



#endif /* BSW_COMPONENT_ORING_H_ */
