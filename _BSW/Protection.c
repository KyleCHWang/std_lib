/*
 * Protection.c
 *
 *  Created on: Aug 15, 2019
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "IQ_Math.h"
#include "Protection.h"


void API_Protect_Configuation(Protection_t* tempProtect,FunctionalState enable,ProtectionMode_t mode, UINT16 debounce,UINT16 debounce_R, float protectvalue, float up, float low){

    tempProtect->enable = enable;
    tempProtect->mode = mode;
	tempProtect->debounce = debounce;
	tempProtect->debounceRecovery = debounce_R;
    tempProtect->value_protect = protectvalue;
    tempProtect->uplimit = up;
    tempProtect->lowlimit = low;
    tempProtect->value_lowlimit = protectvalue + low;
    tempProtect->value_uplimit = protectvalue + up;

}
void API_Protect_ONOFF(Protection_t* tempProtect,FunctionalState enable){
	tempProtect->enable = enable;
	if(enable == DISABLE){
        tempProtect->tick = 0;
		tempProtect->fault = 0;
	}
}

UINT8 API_Protect_Get_fault(Protection_t* tempProtect){
	return tempProtect->fault;
}

inline
UINT8 API_Protect_upHysteresis(Protection_t* tempProtect, float Input){
    if(tempProtect->enable == 0){
        tempProtect->fault = 0;
        return tempProtect->fault;
    }

    if(tempProtect->fault && tempProtect->mode == Protection_Latch){
        return tempProtect->fault;
    }
        
    if(Input >= tempProtect->value_uplimit){
		if(tempProtect->tick>=tempProtect->debounce){
			tempProtect->tick = 0;
			tempProtect->fault = 1;
		}
		else{
			if(!tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
    else if(Input <= tempProtect->value_lowlimit && tempProtect->mode != Protection_Latch){
		if(tempProtect->tick>=tempProtect->debounceRecovery){
			tempProtect->tick = 0;
			tempProtect->fault = 0;
		}
		else{
			if(tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
	else{
		tempProtect->tick = 0;
	}
    return tempProtect->fault;
}

inline
UINT8 API_Protect_lowHysteresis(Protection_t* tempProtect, float Input){
    if(tempProtect->enable == 0){
        tempProtect->fault = 0;
        return tempProtect->fault;
    }

    if(tempProtect->fault && tempProtect->mode == Protection_Latch){
        return tempProtect->fault;
    }
    
    if(Input <= tempProtect->value_lowlimit){
       if(tempProtect->tick>=tempProtect->debounce){
			tempProtect->tick = 0;
			tempProtect->fault = 1;
		}
		else{
			if(!tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
    else if(Input >= tempProtect->value_uplimit && tempProtect->mode != Protection_Latch){
       	if(tempProtect->tick>=tempProtect->debounceRecovery){
			tempProtect->tick = 0;
			tempProtect->fault = 0;
		}
		else{
			if(tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
	else{
		tempProtect->tick = 0;
	}
    return tempProtect->fault;
}

inline
UINT8 API_Protect_setFlag(Protection_t* tempProtect, UINT8 flag){

    if(tempProtect->enable == 0){
        tempProtect->fault = 0;
        return tempProtect->fault;
    }
    if(flag == SET){
       if(tempProtect->tick>=tempProtect->debounce){
			tempProtect->tick = 0;
			tempProtect->fault = 1;
		}
		else{
			if(!tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
    else{
        if(tempProtect->mode == Protection_Latch){
            return tempProtect->fault;
        }
    
        if(tempProtect->tick>=tempProtect->debounceRecovery){
			tempProtect->tick = 0;
			tempProtect->fault = 0;
		}
		else{
			if(tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
    return tempProtect->fault;
}

inline
UINT8 API_Protect_resetFlag(Protection_t* tempProtect, UINT8 flag){

    if(tempProtect->enable == 0){
        tempProtect->fault = 0;
        return tempProtect->fault;
    }
    if(flag == RESET){
       	if(tempProtect->tick>=tempProtect->debounce){
			tempProtect->tick = 0;
			tempProtect->fault = 1;
		}
		else{
			if(!tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
    else{
        if(tempProtect->mode == Protection_Latch){
            return tempProtect->fault;
        }
    
        if(tempProtect->tick>=tempProtect->debounceRecovery){
			tempProtect->tick = 0;
			tempProtect->fault = 0;
		}
		else{
			if(tempProtect->fault){
				tempProtect->tick++;
			}
		}
    }
    return tempProtect->fault;
}
