/*
 * ChargeProfile.h
 *
 *  Created on: Jul 18, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_COMPONENT_CHARGEPROFILE_H_
#define BSW_COMPONENT_CHARGEPROFILE_H_

#include "TypeDef.h"

//User define function==============================================================================
typedef enum{

    Note_Charge_SoftStart,
    Note_Charge_MAX,

} NoteCharge_t;
typedef enum{

    Note_Discharge_SoftStart,
    Note_Discharge_MAX,

} NoteDischarge_t;
typedef enum{

    Note_UPS_SoftStart,
    Note_UPS_MAX,

} NoteUPS_t;

//==================================================================================================
typedef enum{

    Note_Status_Playing,
    Note_Status_TimeReach,
    Note_Status_Error,

} NoteStatus_t;

typedef struct
{
    //Configuration
    UINT16 time;
    UINT16 time_div;
    UINT16 vslope;
    UINT16 islope;
    UINT16 vstart;
    UINT16 istart;
    UINT16 vCV;
    UINT16 iCC;
} ChargeNotes_t;

typedef struct
{
    //Configuration
    ChargeNotes_t* noteAddr;
    UINT8  Numofnote;
    //Instance
    UINT16 note;
    UINT16 time;
    UINT16 time_tick;
    UINT16 vref;
    UINT16 iref;

} ChargeProfile_t;

extern const ChargeNotes_t Charge_Notes[];
extern const ChargeNotes_t Discharge_Notes[];
extern const ChargeNotes_t UPS_Notes[];

extern void API_CP_Config_noteAddr(ChargeProfile_t* tempCP,ChargeNotes_t* addr);
extern void API_CP_Config_Numofnote(ChargeProfile_t* tempCP,UINT8 number);
extern UINT16 API_CP_Get_vref(ChargeProfile_t* tempCP);
extern UINT16 API_CP_Get_iref(ChargeProfile_t* tempCP);
extern NoteStatus_t API_CP_Tick(ChargeProfile_t* tempCP, UINT8 note);
extern void API_CP_Follow(ChargeProfile_t* tempCP,UINT16* ptrref,UINT16 slope, UINT16 prescaler, UINT16 target);
extern void API_CP_Reset(ChargeProfile_t* tempCP);
extern void API_CP_Follow_float(ChargeProfile_t* tempCP,float* ptrref, float slope, UINT16 prescaler, float target);


#endif /* BSW_COMPONENT_CHARGEPROFILE_H_ */

