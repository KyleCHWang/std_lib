#ifndef Calibration_H
#define Calibration_H

#include "TypeDef.h"
typedef enum{
	Calibration_item_0,
	Calibration_item_1,
	Calibration_item_2,
	Calibration_item_3,
	Calibration_item_MAX,
}Calibration_item_t;
//Module definition======================================	
typedef struct{
	//Configuration
	float coeA_Theory;
	float coeB_Theory;
	float coeA_Tolerance;
	float coeB_Tolerance;
	//Instance
	float coeA;
	float coeB;
	float Value[2];
	float MCUValue[2];
	UINT8 fault:1;
} CalibrationData_t;	

typedef struct{              
	CalibrationData_t CalibrationData[Calibration_item_MAX];
	//instance
	UINT32 delaycnt[10];
} Calibration_t;

#endif
