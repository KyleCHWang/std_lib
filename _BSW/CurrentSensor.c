/*
 * CurrentSensor.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "MCAL_ADC.h"
#include "DigitalFilter.h"
#include "CurrentSensor.h"

void API_CS_Config_ADCchanel(CurrentSensor_t* tempinstance,enumADC_CHL_t tempdata){

    if(tempdata<ADC_CHL_MAX){
        tempinstance->ADCchanel = tempdata;
    }
}

void API_CS_Config_Imax(CurrentSensor_t* tempinstance,float tempdata){

	tempinstance->Imax = tempdata;
}

void API_CS_Config_Imin(CurrentSensor_t* tempinstance,float tempdata){
	
	tempinstance->Imin = tempdata;
}

void API_CS_Config_coeA(CurrentSensor_t* tempinstance,float tempdata){

    tempinstance->coeA = tempdata;
}

void API_CS_Config_coeB(CurrentSensor_t* tempinstance,float tempdata){

    tempinstance->coeB = tempdata;
}

UINT16 API_CS_Get_integer(CurrentSensor_t* tempinstance){
    return tempinstance->integer;
}
UINT16 API_CS_Get_point(CurrentSensor_t* tempinstance){
    return tempinstance->point;
}
float API_CS_Get_Ampere(CurrentSensor_t* tempinstance){
    return tempinstance->Amp;
}
	
void API_CS_Update(CurrentSensor_t* tempinstance){

	tempinstance->valueQ12_raw = API_ADC_Get_rawdata(tempinstance->ADCchanel);
}

void API_CS_AmpereUpdate(CurrentSensor_t* tempinstance){
    float tempfloat = 0;
    UINT32 u32Temp = 0;
    tempfloat = (tempinstance->valueQ12_filtered - tempinstance->coeB)/(tempinstance->coeA);//Coe_V_LV_A
    if(tempfloat > tempinstance->Imax){
		tempfloat = tempinstance->Imax;
		tempinstance->fault = CS_OC;
	}
	else if(tempfloat < tempinstance->Imin){
		tempfloat = tempinstance->Imin;
		tempinstance->fault = CS_OK;
	}
	else{
		tempinstance->fault = CS_OK;
	}
	tempinstance->Amp = tempfloat;
    tempinstance->integer = (UINT16)tempfloat;
    u32Temp = (UINT32)(tempfloat * 1000);
    u32Temp = (u32Temp%1000);
    tempinstance->point = u32Temp;
}

UINT16 API_CS_AmpToQ12(CurrentSensor_t* tempinstance, float amp){
	UINT16 Q12 = 0;
	Q12 = (UINT16)(((amp)*(tempinstance->coeA))+tempinstance->coeB);
	
	return Q12;
}

