/*
 * ChargeProfile.c
 *
 *  Created on: Jul 18, 2019
 *      Author: KYLE.CH.WANG
 */

#include "ChargeProfile.h"
#include "TypeDef.h"

#define _Previous 0xFFFF
#define _iup 0xFFFF
#define _idown 0xFFFE
#define _vup 0xFFFD
#define _vdown 0xFFFC
#define _min 60000
#define _s 1000
#define _ms 1

//User Define function==============================================================================================================
const ChargeNotes_t Charge_Notes[] =
{
     //time     //time_div        //vslope    //islope       //vstart        //istart           //vCV        //iCC
     {2000,     2*_ms,               1,          1,             0,              0,                 1691,           0},   //soft-start
     //{60,      _min,               1,          1,             _Previous,      _Previous,         3449,        1500},
     //{1000,      _s,               1,          1,             _Previous,      _Previous,         3449,        1500}
};
const ChargeNotes_t Discharge_Notes[] =
{
     //time     //time_div        //vslope    //islope       //vstart        //istart           //vCV        //iCC
     {2000,     2*_ms,               2,          1,             0,              1500,              3172,        1500},   //soft-start
     //{1000,     _s,                1,          1,             _Previous,      _Previous,         3449,        1500}
};
const ChargeNotes_t UPS_Notes[] =
{
     //time     //time_div        //vslope    //islope       //vstart        //istart           //vCV        //iCC
     {2000,     2*_ms,               2,          1,             0,              1500,              2922,        1500},   //soft-start
     //{1000,     _s,                1,          1,             _Previous,      _Previous,         3449,        1500}
};
//===================================================================================================================================

void API_CP_Config_noteAddr(ChargeProfile_t* tempCP,ChargeNotes_t* addr){

    tempCP->noteAddr = addr;
}

void API_CP_Config_Numofnote(ChargeProfile_t* tempCP,UINT8 number){

    tempCP->Numofnote = number;
}

UINT16 API_CP_Get_vref(ChargeProfile_t* tempCP){

    return tempCP -> vref;
}
UINT16 API_CP_Get_iref(ChargeProfile_t* tempCP){

    return tempCP -> iref;
}

void API_CP_Reset(ChargeProfile_t* tempCP){

    tempCP->note = 0;
    tempCP->time = 0;
    tempCP->time_tick = 0;
    tempCP->vref = 0;
    tempCP->iref = 0;
}

void API_CP_Follow_float(ChargeProfile_t* tempCP,float* ptrref, float slope, UINT16 prescaler, float target){ //1ms loop

    tempCP->time_tick++;
    if(tempCP->time_tick >= prescaler){
        tempCP->time_tick = 0;
        if(target<slope){
            *ptrref = 0;
        }
        else if((*ptrref >= (target-slope)) && (*ptrref <= (target+slope))){
            *ptrref = target;
        }
        else{
            if(*ptrref > target){
                *ptrref -= slope;
            }
            else if(*ptrref < target){
                *ptrref += slope;
            }
        }
    }

}


void API_CP_Follow(ChargeProfile_t* tempCP,UINT16* ptrref,UINT16 slope, UINT16 prescaler, UINT16 target){ //1ms loop

    tempCP->time_tick++;
    if(tempCP->time_tick >= prescaler){
        tempCP->time_tick = 0;
        if(target<slope){
            *ptrref = 0;
        }
        else if((*ptrref >= (target-slope)) && (*ptrref <= (target+slope))){
            *ptrref = target;
        }
        else{
            if(*ptrref > target){
                *ptrref -= slope;
            }
            else if(*ptrref < target){
                *ptrref += slope;
            }
        }
    }

}

NoteStatus_t API_CP_Tick(ChargeProfile_t* tempCP, UINT8 note){ //1ms loop

    if(note >= tempCP->Numofnote){
        return Note_Status_Error;
    }
    if(tempCP->time == tempCP->noteAddr[tempCP->note].time){
        return Note_Status_TimeReach;
    }
    tempCP->time_tick++;
    if(tempCP->time_tick >= tempCP->noteAddr[tempCP->note].time_div){
        tempCP->time_tick = 0;
        tempCP->time++;
        tempCP->vref += tempCP->noteAddr[tempCP->note].vslope;
        tempCP->iref += tempCP->noteAddr[tempCP->note].islope;
        if(tempCP->vref > tempCP->noteAddr[tempCP->note].vCV){
            tempCP->vref = tempCP->noteAddr[tempCP->note].vCV;
        }
        if(tempCP->iref > tempCP->noteAddr[tempCP->note].iCC){
            tempCP->iref = tempCP->noteAddr[tempCP->note].iCC;
        }
    }
    return Note_Status_Playing;
}
