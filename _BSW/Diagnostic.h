/*
 * Diagnostic.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef COMPONENT_DIAGNOSTIC_H_
#define COMPONENT_DIAGNOSTIC_H_

#include "TypeDef.h"
#include "MCAL_UART.h"

//BSW USER configuration=============================================
#define DIAG_RXBYTE_MAX 6
#define DIAG_CMD_MAX 20

typedef struct{
    UINT32 hdr;
    UINT32 databyte[32];
    UINT32 flag_hdr;
    BF_U32 databit[2];
    UINT32 edr;
} Diagnostic_TxFrame_t;

//==============================================================

typedef struct
 {
   //Configuration
   enumUART_CHL_t UARTchannel;
   void (*cmdarray[DIAG_CMD_MAX])(UINT16 tempU8);
   //Instance
   UINT8 checkdelay_ticks;
   UINT8 rxdatabuffer[DIAG_RXBYTE_MAX];
   Diagnostic_TxFrame_t txframe;
 } Diagnostic_t;

extern void UDF_Diag_SefTest(UINT16 tempU8);
extern void UDF_Diag_AddVADJ(UINT16 tempU8);
extern void UDF_Diag_MinusVADJ(UINT16 tempU8);
extern void UDF_Diag_AddIADJ(UINT16 tempU8);
extern void UDF_Diag_MinusIADJ(UINT16 tempU8);
extern void UDF_Diag_SetONOFF(UINT16 tempU8);
extern void UDF_Diag_SetMode(UINT16 tempU8);


extern void API_Diag_Config_CMDRegister(Diagnostic_t* tempinstance, void (*cmd)(UINT16 tempU8), UINT8 tempU8);
extern void API_Diag_Config_UARTChannel(Diagnostic_t* tempinstance, enumUART_CHL_t tempU8);
extern void API_Diag_CMDCheck(Diagnostic_t* tempinstance);

#endif /* COMPONENT_DIAGNOSTIC_H_ */
