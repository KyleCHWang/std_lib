/*
 * CRC.h
 *
 *  Created on: Mar 31, 2020
 *      Author: KYLE.CH.WANG
 */

#ifndef CRC_H_
#define CRC_H_

#include "TypeDef.h"
//==============================================================
extern UINT16 API_CRC16_0xFFFF(UINT8 *Block, UINT16 len);


#endif /* COMPONENT_CRC_H_ */
