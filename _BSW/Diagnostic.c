/*
 * Diagnostic.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "IQ_Math.h"
#include "Diagnostic.h"
//User define function=========================================================
#include "Object.h"
UINT8 DIAG_testdata = 93;
BF_U8 faultbuffer[2];


void UDF_Diag_Set_TxFrameUpadate(Diagnostic_t* tempinstance){


    tempinstance->txframe.hdr = 0xABABABAB;
    tempinstance->txframe.databyte[0] = VR_VADJ.out;
    tempinstance->txframe.databyte[1] = VR_IADJ.out;
	tempinstance->txframe.databyte[2] = System.ONOFF;
	tempinstance->txframe.databyte[3] = System.mode;
	tempinstance->txframe.databyte[4] = ChargerSM.state;
    tempinstance->txframe.databyte[5] = DIAG_testdata;
    tempinstance->txframe.databyte[6] = 6;
    tempinstance->txframe.databyte[7] = 0;
    tempinstance->txframe.databyte[8] = 0;
    tempinstance->txframe.databyte[9] = 0;
    tempinstance->txframe.databyte[10] = 0;
    tempinstance->txframe.databyte[11] = 0;
    tempinstance->txframe.databyte[12] = 0;
    tempinstance->txframe.databyte[13] = 0;
    tempinstance->txframe.databyte[14] = 0;
    tempinstance->txframe.databyte[15] = 0;
    tempinstance->txframe.databyte[16] = 0;
    tempinstance->txframe.databyte[17] = 0;
    tempinstance->txframe.databyte[18] = 0;
    tempinstance->txframe.databyte[19] = 0;
    tempinstance->txframe.databyte[20] = VS_vLV_DCBUS.integer;
    tempinstance->txframe.databyte[21] = VS_vLV_DCBUS.point;
    tempinstance->txframe.databyte[22] = VS_vLV_BAT.integer;
    tempinstance->txframe.databyte[23] = VS_vLV_BAT.point;
    tempinstance->txframe.databyte[24] = CS_iLV_Charge.integer;
    tempinstance->txframe.databyte[25] = CS_iLV_Charge.point;
    tempinstance->txframe.databyte[26] = VS_vLV_DCBUS.valueQ12_raw;
    tempinstance->txframe.databyte[27] = VS_vLV_DCBUS.valueQ12_filtered;
    tempinstance->txframe.databyte[28] = VS_vLV_BAT.valueQ12_raw;
    tempinstance->txframe.databyte[29] = VS_vLV_BAT.valueQ12_filtered;
    tempinstance->txframe.databyte[30] = CS_iLV_Charge.valueQ12_raw;
    tempinstance->txframe.databyte[31] = CS_iLV_Charge.valueQ12_filtered;

    tempinstance->txframe.flag_hdr = 0xCFCFCFCF;
    tempinstance->txframe.databit[0].bit.b0 = 0;
    tempinstance->txframe.databit[0].bit.b1 = 1;
    tempinstance->txframe.databit[0].bit.b2 = 0;
    tempinstance->txframe.databit[0].bit.b16 = 0;
    tempinstance->txframe.databit[0].bit.b17 = 0;
    tempinstance->txframe.databit[0].bit.b18 = 0;
    tempinstance->txframe.databit[0].bit.b19 = 0;
	

	tempinstance->txframe.databit[1].bit.b20 = faultbuffer[0].bit.b7; //Vin_VF.fault;
	tempinstance->txframe.databit[1].bit.b21 = Vin_OFP.fault;
	tempinstance->txframe.databit[1].bit.b22 = Vin_UFP.fault;
    tempinstance->txframe.databit[1].bit.b23 = faultbuffer[0].bit.b5; //PFC_F.fault;
    tempinstance->txframe.databit[1].bit.b24 = faultbuffer[0].bit.b4; //HW_OCP_LV.fault;
    tempinstance->txframe.databit[1].bit.b25 = faultbuffer[0].bit.b3; //HW_OVP_LV.fault;
    tempinstance->txframe.databit[1].bit.b26 = faultbuffer[1].bit.b7; //BAT_Reverse.fault;
    tempinstance->txframe.databit[1].bit.b27 = faultbuffer[1].bit.b6; //SW_OVP_LV.fault;
    tempinstance->txframe.databit[1].bit.b28 = faultbuffer[1].bit.b5; //SW_UVP_LV.fault;
    tempinstance->txframe.databit[1].bit.b29 = faultbuffer[1].bit.b4; //SW_OCP_LV.fault;
    tempinstance->txframe.databit[1].bit.b30 = faultbuffer[1].bit.b3; //OTP.fault;
    tempinstance->txframe.databit[1].bit.b31 = ChargerSTOP.fault;

    tempinstance->txframe.edr = 0xEDEDEDED;

}
void UDF_Diag_SefTest(UINT16 tempU8){
    DIAG_testdata = tempU8;
}

void UDF_Diag_AddVADJ(UINT16 tempU8){

	INT32 tempout = 0;

	tempout = API_VR_Get_out(&VR_VADJ);
	tempout += tempU8;
	if(tempout>=65535){
		tempout	= 65535;
	}
	API_VR_Set_out(&VR_VADJ, (UINT16)tempout);
}
void UDF_Diag_MinusVADJ(UINT16 tempU8){

	INT32 tempout = 0;

	tempout = API_VR_Get_out(&VR_VADJ);
	tempout -= tempU8;
	if(tempout<=0){
		tempout	= 0;
	}
	API_VR_Set_out(&VR_VADJ, (UINT16)tempout);
}
void UDF_Diag_AddIADJ(UINT16 tempU8){

	INT32 tempout = 0;

	tempout = API_VR_Get_out(&VR_IADJ);
	tempout += tempU8;
	if(tempout>=65535){
		tempout	= 65535;
	}
	API_VR_Set_out(&VR_IADJ, (UINT16)tempout);
}
void UDF_Diag_MinusIADJ(UINT16 tempU8){

	INT32 tempout = 0;

	tempout = API_VR_Get_out(&VR_IADJ);
	tempout -= tempU8;
	if(tempout<=0){
		tempout	= 0;
	}
	API_VR_Set_out(&VR_IADJ, (UINT16)tempout);
}
void UDF_Diag_SetONOFF(UINT16 tempU8){

	if(tempU8 < System_ONOFF_MAX){
		System.ONOFF = tempU8;
	}
	else{
		System.ONOFF = System_OFF;
	}
}
void UDF_Diag_SetMode(UINT16 tempU8){

	if(tempU8 < Mode_MAX){
		System.mode = tempU8;
	}
	else{
		System.mode = Mode_Charge;
	}
}

/*
void UD_PID_PlusPeriod(UINT8 tempdata){

    INT32 period_set;
    openloop = 1;
    if(GC_HV1.period > (65535-300)){
        period_set = 65535;
    }
    else{
        period_set = GC_HV1.period+50;
    }
    API_GC_Set_period(&GC_HV1,period_set);
    API_GC_Set_period(&GC_HV2,period_set);
    API_GC_Set_period(&GC_LV3B,period_set);
    API_GC_Set_period(&GC_LV4B,period_set);

}
void UDF_PID_MinusPeriod(UINT8 tempdata){

    INT32 period_set;
    if(GC_HV1.period < (300)){
        period_set = 0;
    }
    else{
        period_set = GC_HV1.period-50;
    }
    openloop = 1;
    API_GC_Set_period(&GC_HV1,period_set);
    API_GC_Set_period(&GC_HV2,period_set);
    API_GC_Set_period(&GC_LV3B,period_set);
    API_GC_Set_period(&GC_LV4B,period_set);

}
void UDF_PID_Period_0(UINT8 tempdata){

    openloop = 1;
    API_GC_Set_period(&GC_HV1,0);
    API_GC_Set_period(&GC_HV2,0);
    API_GC_Set_period(&GC_LV3B,0);
    API_GC_Set_period(&GC_LV4B,0);
}
void UDF_PID_Period_max(UINT8 tempdata){

    openloop = 1;
    API_GC_Set_period(&GC_HV1,65535);
    API_GC_Set_period(&GC_HV2,65535);
    API_GC_Set_period(&GC_LV3B,65535);
    API_GC_Set_period(&GC_LV4B,65535);

}

void UDF_PID_Closeloop(UINT8 tempdata){

    openloop = 0;
    API_PID_Reset(&vLV_Controller);
    API_PID_Reset(&iLVCharge_Controller);

}
void UDF_PID_PlusAlpha(UINT8 tempdata){

    if(LVtoHV==0){
        API_GC_Set_alpha(&GC_LV4B,(GC_LV4B.alpha+300));
    }
    else if(LVtoHV==1){
        API_GC_Set_alpha(&GC_HV1,(GC_HV1.alpha+300));
    }


}
void UDF_PID_MinusAlpha(UINT8 tempdata){
    if(LVtoHV==0){
        if((INT32)GC_LV4B.alpha-100 < 0){

            API_GC_Set_alpha(&GC_LV4B,0);
        }
        else{
            API_GC_Set_alpha(&GC_LV4B,(GC_LV4B.alpha-100));
        }
    }
    else if(LVtoHV==1){
        if((INT32)GC_HV1.alpha-100 < 0){
            API_GC_Set_alpha(&GC_HV1,0);
        }
        else{
            API_GC_Set_alpha(&GC_HV1,(GC_HV1.alpha-100));
        }
    }
}
void UDF_PID_PlusGamma(UINT8 tempdata){

    if(LVtoHV==0){
        API_GC_Set_gamma(&GC_LV3B,(GC_LV3B.gamma+100));
        API_GC_Set_gamma(&GC_LV4B,(GC_LV4B.gamma+100));
    }
    else if(LVtoHV==1){
        API_GC_Set_gamma(&GC_HV1,(GC_HV1.gamma+100));
        API_GC_Set_gamma(&GC_HV2,(GC_HV2.gamma+100));
    }

}
void UDF_PID_MinusGamma(UINT8 tempdata){
    if(LVtoHV==0){
        if((INT32)GC_LV3B.gamma-100 < 0){
            API_GC_Set_gamma(&GC_LV3B,0);
            API_GC_Set_gamma(&GC_LV4B,0);
        }
        else{
            API_GC_Set_gamma(&GC_HV1,(GC_HV1.gamma-100));
            API_GC_Set_gamma(&GC_HV2,(GC_HV2.gamma-100));
        }
    }
    else if(LVtoHV==1){
        if((INT32)GC_HV1.gamma-100 < 0){
            API_GC_Set_gamma(&GC_HV1,0);
            API_GC_Set_gamma(&GC_HV2,0);
        }
        else{
            API_GC_Set_gamma(&GC_HV1,(GC_HV1.gamma-100));
            API_GC_Set_gamma(&GC_HV2,(GC_HV2.gamma-100));
        }
    }

}
void UDF_PID_PlusTheta(UINT8 tempdata){

    if(LVtoHV==0){
        API_GC_Set_theta(&GC_LV3B,(GC_LV3B.theta+100));
    }
    else if(LVtoHV==1){
        API_GC_Set_theta(&GC_HV2,(GC_HV2.theta+100));
    }

}
void UDF_PID_MinusTheta(UINT8 tempdata){
    if(LVtoHV==0){
        API_GC_Set_theta(&GC_LV3B,(GC_LV3B.theta-100));
    }
    else if(LVtoHV==1){
        API_GC_Set_theta(&GC_HV2,(GC_HV2.theta-100));
    }

}
void UDF_PID_PlusVref(UINT8 tempdata){

    if(System.mode == Mode_Charge){
        if((HKC_Converter.rxdata.Vcmd_LV+50) <= 4000){

            HKC_Converter.rxdata.Vcmd_LV += 50;
        }
        else{

            HKC_Converter.rxdata.Vcmd_LV = 4000;
        }
    }
    else if(System.mode == Mode_Discharge||System.mode == Mode_UPS){
        if((HKC_Converter.rxdata.Vcmd_HV+50) <= 4000){

            HKC_Converter.rxdata.Vcmd_HV += 50;
        }
        else{

            HKC_Converter.rxdata.Vcmd_HV = 4000;
        }
    }
}
void UDF_PID_MinusVref(UINT8 tempdata){

    if(System.mode == Mode_Charge){
        if(HKC_Converter.rxdata.Vcmd_LV<50){
            HKC_Converter.rxdata.Vcmd_LV = 0;
        }
        else{
            HKC_Converter.rxdata.Vcmd_LV -=50;
        }
    }
    else if(System.mode == Mode_Discharge||System.mode == Mode_UPS){
        if(HKC_Converter.rxdata.Vcmd_HV<50){
            HKC_Converter.rxdata.Vcmd_HV = 0;
        }
        else{
            HKC_Converter.rxdata.Vcmd_HV -=50;
        }
    }
}
void UDF_PID_PlusIref(UINT8 tempdata){

    if(System.mode == Mode_Charge){
        if((HKC_Converter.rxdata.Icmd_LV_Charge+50) <= 3822){

            HKC_Converter.rxdata.Icmd_LV_Charge += 100;
        }
        else{

            HKC_Converter.rxdata.Icmd_LV_Charge = 3822;
        }
    }
    else if(System.mode == Mode_Discharge||System.mode == Mode_UPS){
        if((HKC_Converter.rxdata.Icmd_LV_Discharge+50) <= 2750){

            HKC_Converter.rxdata.Icmd_LV_Discharge += 100;
        }
        else{

            HKC_Converter.rxdata.Icmd_LV_Discharge = 2750;
        }

    }
}
void UDF_PID_MinusIref(UINT8 tempdata){
    if(System.mode == Mode_Charge){
        if(HKC_Converter.rxdata.Icmd_LV_Charge<50){
            HKC_Converter.rxdata.Icmd_LV_Charge = 0;
        }
        else{
            HKC_Converter.rxdata.Icmd_LV_Charge -= 100;
        }

    }
    else if(System.mode == Mode_Discharge||System.mode == Mode_UPS){
        if(HKC_Converter.rxdata.Icmd_LV_Discharge<50){
            HKC_Converter.rxdata.Icmd_LV_Discharge = 0;
        }
        else{
            HKC_Converter.rxdata.Icmd_LV_Discharge -= 100;
        }

    }

}

void UDF_PID_FaultCheck(UINT8 tempdata){

    ChargerSM.faultcheck = 1;


}
void UDF_System_SetMode(UINT8 tempdata){
    if(tempdata < Mode_MAX){
        System.mode = (ChargerSystemMode_t)tempdata;
    }
}
void UDF_System_SetONOFF(UINT8 tempdata){
    if(tempdata < System_ONOFF_MAX){
        System.ONOFF = tempdata;
    }
}
*/
//================================================================================

void RxBufferReset(Diagnostic_t* tempinstance){
    UINT8 Cnt = 0;
    for(Cnt=0;Cnt<DIAG_RXBYTE_MAX;Cnt++){
        tempinstance->rxdatabuffer[Cnt] = 0;
    }
}
void API_Diag_Config_UARTChannel(Diagnostic_t* tempinstance, enumUART_CHL_t tempU8){

    tempinstance->UARTchannel = tempU8;
    API_UART_Config_RxBuffer(tempinstance->UARTchannel, tempinstance->rxdatabuffer, (UINT16)sizeof(tempinstance->rxdatabuffer));
    API_UART_Config_TxBuffer(tempinstance->UARTchannel, (UINT8*)(&tempinstance->txframe), (UINT16)(sizeof(tempinstance->txframe)));
	API_UART_RecieveData(tempinstance->UARTchannel); //Start recieving
}
void API_Diag_Config_CMDRegister(Diagnostic_t* tempinstance, void (*cmd)(UINT16 tempU8), UINT8 tempU8){

    tempinstance->cmdarray[tempU8] = cmd;
}
void API_Diag_CMDCheck(Diagnostic_t* tempinstance)
{
    UINT8   cmd_code = 0;
    UINT16  cmd_data = 0;

    UDF_Diag_Set_TxFrameUpadate(tempinstance);
    tempinstance->checkdelay_ticks++;
    if (tempinstance->checkdelay_ticks < 100){ //100 * 1 = 100ms
        return;
    }
    tempinstance->checkdelay_ticks = 0;

    //------------------------------rx
    if ((tempinstance->rxdatabuffer[0] == 0xD1) && (tempinstance->rxdatabuffer[1] == 0xD8) && (tempinstance->rxdatabuffer[5] == 0xED))
    {
        cmd_code = tempinstance->rxdatabuffer[2];
        cmd_data = tempinstance->rxdatabuffer[3];
        cmd_data <<= 8;
        cmd_data |= tempinstance->rxdatabuffer[4];

        tempinstance->cmdarray[cmd_code](cmd_data);

    }
    RxBufferReset(tempinstance);
	API_UART_RxReset(tempinstance->UARTchannel);
    //-------------------------tx
    UDF_Diag_Set_TxFrameUpadate(tempinstance);
    API_UART_SendData(tempinstance->UARTchannel);

}



