/*
 * VoltageSensor.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef COMPONENT_VOLTAGESENSOR_H_
#define COMPONENT_VOLTAGESENSOR_H_

#include "TypeDef.h"
#include "IQ_Math.h"
#include "MCAL_ADC.h"
#include "DigitalFilter.h"

//CFG
typedef enum{
    VS_OK,
    VS_OV,
    VS_UV,
}VS_FAULT_t;


typedef struct
 {
   //Configuration
   enumADC_CHL_t  ADCchanel;
   float Vmax;
   float Vmin;
   float coeA;
   float coeB;
   float Resistance;
   //Instance
   UINT16 valueQ12_raw;
   UINT16 valueQ12_filtered;
   UINT16 integer;
   UINT16 point;
   float Volt;
   UINT8 fault;
 } VoltageSensor_t;

 //API
extern void API_VS_Config_ADCchanel(VoltageSensor_t* tempinstance,enumADC_CHL_t tempdata);
extern void API_VS_Config_coeA(VoltageSensor_t* tempinstance,float tempdata);
extern void API_VS_Config_coeB(VoltageSensor_t* tempinstance,float tempdata);
extern void API_VS_Config_Resistance(VoltageSensor_t* tempinstance,float tempdata);


extern UINT16 API_VS_Get_valueQ12_filtered(VoltageSensor_t* tempinstance);
extern UINT16 API_VS_Get_integer(VoltageSensor_t* tempinstance);
extern UINT16 API_VS_Get_point(VoltageSensor_t* tempinstance);
extern float API_VS_Get_Volt(VoltageSensor_t* tempinstance);


extern void API_VS_Update(VoltageSensor_t* tempinstance);
extern void API_VS_VoltUpdate(VoltageSensor_t* tempinstance);
extern void API_VS_VoltUpdateCompensation(VoltageSensor_t* tempinstance,float current);
extern UINT16 API_VS_VoltToQ12(VoltageSensor_t* tempinstance, float volt);

extern void API_VS_Config_Vmax(VoltageSensor_t* tempinstance,float tempdata);
extern void API_VS_Config_Vmin(VoltageSensor_t* tempinstance,float tempdata);




#endif /* COMPONENT_VOLTAGESENSOR_H_ */
