#include "TypeDef.h"
#include "MCAL_PWM.h"
#include "Object.h"
#include "Calibration.h"
#include "IQ_Math.h"

static float coe_r[(Calibration_item_MAX*2)] = {0.0f};
static float coe_w[(Calibration_item_MAX*2)] = {0.0f};

static void COE_Judge(Calibration_t* inst, Calibration_item_t item){
	float tempCOE_A, tempCOE_B;
	tempCOE_A = inst->CalibrationData[item].coeA - inst->CalibrationData[item].coeA_Theory;
	tempCOE_B = inst->CalibrationData[item].coeB - inst->CalibrationData[item].coeB_Theory;
	tempCOE_A = ABS(tempCOE_A);
	tempCOE_B = ABS(tempCOE_B);
	if(tempCOE_A > inst->CalibrationData[item].coeA_Tolerance){
		inst->CalibrationData[item].fault = 1;
	}
	else if(tempCOE_B > inst->CalibrationData[item].coeB_Tolerance){
		inst->CalibrationData[item].fault = 1;
	}
	else{
		inst->CalibrationData[item].fault = 0;
	}
}

void API_Calibrate_Config_coeA_Theory(Calibration_t* inst, Calibration_item_t item, float coe){
	inst->CalibrationData[item].coeA_Theory = coe;
}
void API_Calibrate_Config_coeB_Theory(Calibration_t* inst, Calibration_item_t item, float coe){
	inst->CalibrationData[item].coeB_Theory = coe;
}
void API_Calibrate_Config_coeA_Tolerance(Calibration_t* inst, Calibration_item_t item, float coe){
	inst->CalibrationData[item].coeA_Tolerance = coe;
}
void API_Calibrate_Config_coeB_Tolerance(Calibration_t* inst, Calibration_item_t item, float coe){
	inst->CalibrationData[item].coeB_Tolerance = coe;
}
float API_Calibrate_Get_coeA(Calibration_t* inst, Calibration_item_t item){
	return inst->CalibrationData[item].coeA;
}
float API_Calibrate_Get_coeB(Calibration_t* inst, Calibration_item_t item){
	return inst->CalibrationData[item].coeB;
}
void API_Calibrate_SetPoint(Calibration_t* inst, Calibration_item_t item, UINT8 index, float Value, float MCUValue){
	inst->CalibrationData[item].MCUValue[index] = MCUValue;
	inst->CalibrationData[item].Value[index] = Value;
}
void  API_Calibrate_Proccess(Calibration_t* inst, Calibration_item_t item){
	
	inst->CalibrationData[item].coeA = (float)((inst->CalibrationData[item].MCUValue[0] - inst->CalibrationData[item].MCUValue[1])/(inst->CalibrationData[item].Value[0] - inst->CalibrationData[item].Value[1]));
	inst->CalibrationData[item].coeB = (float)((inst->CalibrationData[item].MCUValue[0] - inst->CalibrationData[item].coeA*(inst->CalibrationData[item].Value[0])));
	COE_Judge(inst,item);
	
}
void  API_Calibrate_SaveData(Calibration_t* inst, UINT32 Addr){
	UINT8 cnt=0;
	for(cnt=0;cnt<Calibration_item_MAX;cnt++){
		coe_w[cnt*2] = inst->CalibrationData[cnt].coeA;
		coe_w[(cnt*2)+1] = inst->CalibrationData[cnt].coeB;
	}
	API_FLASH_WriteWait(Addr, (UINT64*)coe_w, Calibration_item_MAX);
}
void API_Calibrate_ReadData(Calibration_t* inst, UINT32 Addr){
	UINT8 cnt=0;
	float tempCOE_A, tempCOE_B;
	API_FLASH_ReadWait(Addr, (UINT64*)coe_r, Calibration_item_MAX);
	for(cnt=0;cnt<Calibration_item_MAX;cnt++){
		inst->CalibrationData[cnt].coeA = coe_r[cnt*2] ;
		inst->CalibrationData[cnt].coeB = coe_r[(cnt*2)+1];
		COE_Judge(inst,cnt);
	}
	
}

UINT8 API_Calibrate_CheckFault(Calibration_t* inst){
	UINT8 cnt=0;
	for(cnt=0;cnt<Calibration_item_MAX;cnt++){
		if(inst->CalibrationData[cnt].fault == 1){
			return 1;
		}
	}
	return 0;
}

