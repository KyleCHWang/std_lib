/*
 * EasyUART.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef EASY_UART_H_
#define EASY_UART_H_

#include "TypeDef.h"
#include "MCAL_UART.h"

//BSW USER configuration=============================================
#define RX_TIMEOUT 300
#define TX_PERIOD 100
#define EZUART_RXBYTE_MAX 40
#define EZUART_TXBYTE_MAX 40
//==============================================================

typedef struct
 {
   //Configuration
   enumUART_CHL_t UARTchannel;
   //Instance
   UINT8 rxdatabuffer[EZUART_RXBYTE_MAX];
   UINT8 txdatabuffer[EZUART_TXBYTE_MAX];
   UINT16 delaycnt[3];
 } EasyUART_t;

extern void API_EZUART_Config_UARTChannel(EasyUART_t* tempinstance, enumUART_CHL_t tempU8);
extern void API_EZUART_RxTask(EasyUART_t* tempinstance);
extern void API_EZUART_TxTask(EasyUART_t* tempinstance);

#endif /* COMPONENT_DIAGNOSTIC_H_ */
