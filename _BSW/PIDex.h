/*
 * PIDex.h
 *
 *  Created on: Jul 16, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_DIGITALCONTROL_PIDEX_H_
#define BSW_DIGITALCONTROL_PIDEX_H_

typedef struct
 {
   INT16 hKp_Gain;
   UINT16 hKp_Divisor;
   INT16 hKi_Gain;
   UINT16 hKi_Divisor;
   INT32 wLower_Limit_Output;     //Lower Limit for Output limitation
   INT32 wUpper_Limit_Output;     //Lower Limit for Output limitation
   INT32 wLower_Limit_Integral;   //Lower Limit for Integral term limitation
   INT32 wUpper_Limit_Integral;   //Lower Limit for Integral term limitation
   INT32 wIntegral;
   INT32 wError;
   // Actually used only if DIFFERENTIAL_TERM_ENABLED is enabled
   INT16 hKd_Gain;
   UINT16 hKd_Divisor;
   INT32 wPreviousError;
 } PID_Struct_test_t;

 extern void API_PID_Reset(PID_Struct_test_t *PID_Struct);
 extern INT32 API_PID_Regulator_ex(INT16 hReference, INT16 hPresentFeedback, PID_Struct_test_t *PID_Struct);

#endif /* BSW_DIGITALCONTROL_PIDEX_H_ */
