/*
 * VoltageRegulator.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef COMPONENT_VOLTAGEREGULATOR_H_
#define COMPONENT_VOLTAGEREGULATOR_H_

#include "TypeDef.h"
#include "IQ_Math.h"
#include "MCAL_PWM.h"

//CFG

typedef struct
 {
   //Configuration
   enumPWM_CHL_t  PWMchanel;
   float coeA;
   float coeB;
   //Instance
   UINT16 out;
   float output;
 } VoltageRegulator_t;

 //API
extern void API_VR_Config_coeA(VoltageRegulator_t* tempinstance, float coe);
extern void API_VR_Config_coeB(VoltageRegulator_t* tempinstance, float coe);
extern void API_VR_Config_PWMchanel(VoltageRegulator_t* tempinstance, enumPWM_CHL_t tempdata);
extern UINT16 API_VR_Get_out(VoltageRegulator_t* tempinstance);
extern void API_VR_Set_out(VoltageRegulator_t* tempinstance, UINT16 out);
extern void API_VR_Output(VoltageRegulator_t* tempinstance, float output);



#endif /* COMPONENT_VOLTAGEREGULATOR_H_ */
