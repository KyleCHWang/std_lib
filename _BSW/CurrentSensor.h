/*
 * CurrentSensor.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef COMPONENT_CURRENTSENSOR_H_
#define COMPONENT_CURRENTSENSOR_H_
#include "TypeDef.h"
#include "IQ_Math.h"
#include "MCAL_ADC.h"
#include "DigitalFilter.h"

//CFG

typedef enum{
    CS_OK,
    CS_OC,

}CS_FAULT_t;


typedef struct
 {
   //Configuration
   enumADC_CHL_t  ADCchanel;
   float Imax;
   float Imin;
   float coeA;
   float coeB;
   UINT32 calibration; //Q16
   //Instance
   UINT16 valueQ12_raw;
   UINT16 valueQ12_filtered;
   UINT16 integer;
   UINT16 point;
   float Amp;
   UINT8 fault;
 } CurrentSensor_t;

 //API
 extern void API_CS_Config_ADCchanel(CurrentSensor_t* tempinstance,enumADC_CHL_t tempdata);
 extern void API_CS_Config_coeA(CurrentSensor_t* tempinstance,float tempdata);
 extern void API_CS_Config_coeB(CurrentSensor_t* tempinstance,float tempdata);
 extern void API_CS_Config_Imax(CurrentSensor_t* tempinstance,float tempdata);
 extern void API_CS_Config_Imin(CurrentSensor_t* tempinstance,float tempdata);
 
 extern UINT16 API_CS_Get_integer(CurrentSensor_t* tempinstance);
 extern UINT16 API_CS_Get_point(CurrentSensor_t* tempinstance); 
 extern float API_CS_Get_Ampere(CurrentSensor_t* tempinstance);

 extern void API_CS_Update(CurrentSensor_t* tempinstance);
 extern void API_CS_AmpereUpdate(CurrentSensor_t* tempinstance);
 extern  UINT16 API_CS_AmpToQ12(CurrentSensor_t* tempinstance, float amp);

#endif /* COMPONENT_CURRENTSENSOR_H_ */
