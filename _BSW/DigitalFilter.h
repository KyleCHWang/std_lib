/*
 * DigitalFilter.h
 *
 *  Created on: Jun 21, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef DIGITALCONTROL_DIGITALFILTER_H_
#define DIGITALCONTROL_DIGITALFILTER_H_

typedef enum
{
    DF_SINGEDSIMPLE,
    DF_UNSINGEDSIMPLE,
    DF_MAX,

} enumDigitalFilter_Type_t;



#endif /* DIGITALCONTROL_DIGITALFILTER_H_ */
