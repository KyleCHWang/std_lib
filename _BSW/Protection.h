/*
 * Protection.h
 *
 *  Created on: Aug 15, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_COMPONENT_PROTECTION_H_
#define BSW_COMPONENT_PROTECTION_H_
#include "TypeDef.h"
#include "MCAL_GPIO.h"

typedef enum{
    Protection_Hiccup,
    Protection_Latch,
}ProtectionMode_t;

typedef struct{

    //Configuration
    UINT8 enable;
    ProtectionMode_t mode;
    float value_protect;
    float uplimit;
    float lowlimit;
	UINT16 debounce;
	UINT16 debounceRecovery;
    //instance
    float value_uplimit;
    float value_lowlimit;
    UINT8 fault;
	UINT16 tick;
	

} Protection_t;

extern void API_Protect_Configuation(Protection_t* tempProtect,FunctionalState enable,ProtectionMode_t mode,UINT16 debounce,UINT16 debounce_R, float protectvalue, float up, float low);
extern void API_Protect_ONOFF(Protection_t* tempProtect,FunctionalState enable) ;
extern UINT8 API_Protect_Get_fault(Protection_t* tempProtect);
extern UINT8 API_Protect_upHysteresis(Protection_t* tempProtect, float Input);
extern UINT8 API_Protect_lowHysteresis(Protection_t* tempProtect, float Input);
extern UINT8 API_Protect_setFlag(Protection_t* tempProtect, UINT8 flag);
extern UINT8 API_Protect_resetFlag(Protection_t* tempProtect, UINT8 flag);


#endif /* BSW_COMPONENT_PROTECTION_H_ */
