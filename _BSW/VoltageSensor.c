/*
 * VoltageSensor.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#include "TypeDef.h"
#include "MCAL_ADC.h"
#include "DigitalFilter.h"
#include "VoltageSensor.h"

void API_VS_Config_ADCchanel(VoltageSensor_t* tempinstance,enumADC_CHL_t tempdata){

    if(tempdata<ADC_CHL_MAX){
        tempinstance->ADCchanel = tempdata;
    }
}
void API_VS_Config_Vmax(VoltageSensor_t* tempinstance,float tempdata){

	tempinstance->Vmax = tempdata;
}
void API_VS_Config_Vmin(VoltageSensor_t* tempinstance,float tempdata){

	tempinstance->Vmin = tempdata;
}

void API_VS_Config_coeA(VoltageSensor_t* tempinstance,float tempdata){

    tempinstance->coeA = tempdata;
}

void API_VS_Config_coeB(VoltageSensor_t* tempinstance,float tempdata){

    tempinstance->coeB = tempdata;
}
void API_VS_Config_Resistance(VoltageSensor_t* tempinstance,float tempdata){

	tempinstance->Resistance = tempdata;
}

UINT16 API_VS_Get_valueQ12_filtered(VoltageSensor_t* tempinstance){
    return tempinstance->valueQ12_filtered;
}
UINT16 API_VS_Get_integer(VoltageSensor_t* tempinstance){
    return tempinstance->integer;
}
UINT16 API_VS_Get_point(VoltageSensor_t* tempinstance){
    return tempinstance->point;
}
float API_VS_Get_Volt(VoltageSensor_t* tempinstance){
    return tempinstance->Volt;
}

void API_VS_VoltUpdate(VoltageSensor_t* tempinstance){
    float tempfloat = 0;
    UINT32 u32Temp = 0;
	tempfloat = (tempinstance->valueQ12_filtered - tempinstance->coeB)/(tempinstance->coeA);
	 if(tempfloat > tempinstance->Vmax){
		tempfloat = tempinstance->Vmax;
		tempinstance->fault = VS_OV;
	}
	else if(tempfloat < tempinstance->Vmin){
		tempfloat = tempinstance->Vmin;
		tempinstance->fault = VS_UV;
	}
	else{
		tempinstance->fault = VS_OK;
	}    
    tempinstance->Volt = tempfloat;
    tempinstance->integer = (UINT16)tempfloat;
    u32Temp = (UINT32)(tempfloat * 1000);
    u32Temp = (u32Temp%1000);
    tempinstance->point = u32Temp;
}
void API_VS_VoltUpdateCompensation(VoltageSensor_t* tempinstance,float current){
	API_VS_VoltUpdate(tempinstance);
	tempinstance->Volt -= (current*(tempinstance->Resistance));
	
}
void API_VS_Update(VoltageSensor_t* tempinstance){

    tempinstance->valueQ12_raw = API_ADC_Get_rawdata(tempinstance->ADCchanel);
}

UINT16 API_VS_VoltToQ12(VoltageSensor_t* tempinstance, float volt){
	UINT16 Q12 = 0;
	Q12 = (UINT16)(((volt)*(tempinstance->coeA))+tempinstance->coeB);
	
	return Q12;
}


