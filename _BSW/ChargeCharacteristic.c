/*
 * ChargeProfile.c
 *
 *  Created on: Jul 18, 2019
 *      Author: KYLE.CH.WANG
 */

#include "ChargeCharacteristic.h"
#include "TypeDef.h"
#include "Object.h"

//IUIaWET12h_Para====================================================================================================
_st_IUIaWET12h IUIaWET12h_Para;
void API_CC_IUIaWET12h_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IUIaWET12h_Para.CurrnetLevel = IUIaWET12h_CL_0;	
    IUIaWET12h_Para.PhaseOld = PHASE_MAX;
    IUIaWET12h_Para.Phase = PHASE_INIT;
	IUIaWET12h_Para.Proccess = PROC_STOP;
	IUIaWET12h_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IUIaWET12h_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_IUIaWET12h_getProccess(void){
	return IUIaWET12h_Para.Proccess;
}
enum_CC_Phase API_CC_IUIaWET12h_getPhase(void){
	return IUIaWET12h_Para.Phase;
}

void API_CC_IUIaWET12h_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	BF_U32 tempbf;
	tempbf.all = 0;
	UINT8 reset = bf.bit.b0;
	IUIaWET12h_Para.CurrnetLevel = (enum_IUIaWET12h_CurrentLevel)para0;
	switch(IUIaWET12h_Para.Phase){
		case PHASE_INIT:
			if(IUIaWET12h_Para.Phase != IUIaWET12h_Para.PhaseOld){
                IUIaWET12h_Para.PhaseOld = IUIaWET12h_Para.Phase;  
				API_CC_IUIaWET12h_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IUIaWET12h_Para.Phase = PHASE_1;
			};
			IUIaWET12h_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IUIaWET12h_Para.Phase != IUIaWET12h_Para.PhaseOld){
                IUIaWET12h_Para.PhaseOld = IUIaWET12h_Para.Phase;
				Control.vref_LV = IUIaWET12h_U2;
				Control.iref_LV_Charge = IUIaWET12h_I1[IUIaWET12h_Para.CurrnetLevel];
				IUIaWET12h_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIaWET12h_U2*CV_tolerance)){
				IUIaWET12h_Para.Phase = PHASE_2;
			}
			if(IUIaWET12h_Para.T[1] >= IUIaWET12h_T1_MAX){
				IUIaWET12h_Para.Phase = PHASE_STOP;
			}
			if(IUIaWET12h_Para.T[1] <= IUIaWET12h_T1_MAX){
				IUIaWET12h_Para.T[1]++;
			}
			IUIaWET12h_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IUIaWET12h_Para.Phase != IUIaWET12h_Para.PhaseOld){
                IUIaWET12h_Para.PhaseOld = IUIaWET12h_Para.Phase;
				Control.vref_LV = IUIaWET12h_U2;
				Control.iref_LV_Charge = IUIaWET12h_I1[IUIaWET12h_Para.CurrnetLevel];
				IUIaWET12h_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IUIaWET12h_I3[IUIaWET12h_Para.CurrnetLevel]){
				IUIaWET12h_Para.Phase = PHASE_3;
			}
			if(IUIaWET12h_Para.T[2] >= IUIaWET12h_T2_MAX){
				IUIaWET12h_Para.Phase = PHASE_STOP;
			}
			if(IUIaWET12h_Para.T[2] <= IUIaWET12h_T2_MAX){
				IUIaWET12h_Para.T[2]++;
			}
			IUIaWET12h_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IUIaWET12h_Para.Phase != IUIaWET12h_Para.PhaseOld){
                IUIaWET12h_Para.PhaseOld = IUIaWET12h_Para.Phase;
				Control.iref_LV_Charge = IUIaWET12h_I3[IUIaWET12h_Para.CurrnetLevel];
				IUIaWET12h_Para.T3 = IUIaWET12h_Para.T[1] + IUIaWET12h_Para.T[2];
				if(IUIaWET12h_Para.T3 <= 1*HOUR){
					IUIaWET12h_Para.T3 = 1*HOUR;
				}
				else if(IUIaWET12h_Para.T3 >= 3*HOUR){
					IUIaWET12h_Para.T3 = 3*HOUR;
				}
				IUIaWET12h_Para.T[3] = 0;
				IUIaWET12h_Para.T[9] = 0;
			}
			if(IUIaWET12h_Para.T[9]==CVtoFC_Delay){
				Control.vref_LV = IUIaWET12h_U3;
			}
			if(IUIaWET12h_Para.T[3] >= IUIaWET12h_Para.T3){
				IUIaWET12h_Para.Phase = PHASE_4;
			}
			if(IUIaWET12h_Para.T[3] <= IUIaWET12h_Para.T3){
				IUIaWET12h_Para.T[3]++;
			}
			if(IUIaWET12h_Para.T[9]<CVtoFC_Delay){
				IUIaWET12h_Para.T[9]++;
			}
			IUIaWET12h_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IUIaWET12h_Para.Phase != IUIaWET12h_Para.PhaseOld){
                IUIaWET12h_Para.PhaseOld = IUIaWET12h_Para.Phase;
				Control.vref_LV = 0.0f;
				Control.iref_LV_Charge = 0.0f;
			}
			IUIaWET12h_Para.Proccess = PROC_FINISH;
		break;
		case PHASE_STOP:
			if(IUIaWET12h_Para.Phase != IUIaWET12h_Para.PhaseOld){
				 IUIaWET12h_Para.PhaseOld = IUIaWET12h_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IUIaWET12h_Para.Proccess = PROC_ABORT;
		break;	
		default:
		break;

	}
        
}
//=======================================================================================================================

//IUIaWET8h_Para====================================================================================================
_st_IUIaWET8h IUIaWET8h_Para;
void API_CC_IUIaWET8h_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IUIaWET8h_Para.CurrnetLevel = IUIaWET8h_CL_0;	
    IUIaWET8h_Para.PhaseOld = PHASE_MAX;
    IUIaWET8h_Para.Phase = PHASE_INIT;
	IUIaWET8h_Para.Proccess = PROC_STOP;
	IUIaWET8h_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IUIaWET8h_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_IUIaWET8h_getProccess(void){
	return IUIaWET8h_Para.Proccess;
}
enum_CC_Phase API_CC_IUIaWET8h_getPhase(void){
	return IUIaWET8h_Para.Phase;
}
void API_CC_IUIaWET8h_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	IUIaWET8h_Para.CurrnetLevel = (enum_IUIaWET8h_CurrentLevel)para0;
	BF_U32 tempbf;
	tempbf.all = 0;
	switch(IUIaWET8h_Para.Phase){
		case PHASE_INIT:
			if(IUIaWET8h_Para.Phase != IUIaWET8h_Para.PhaseOld){
                IUIaWET8h_Para.PhaseOld = IUIaWET8h_Para.Phase;
				API_CC_IUIaWET8h_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IUIaWET8h_Para.Phase = PHASE_1;
			}
			IUIaWET8h_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IUIaWET8h_Para.Phase != IUIaWET8h_Para.PhaseOld){
                IUIaWET8h_Para.PhaseOld = IUIaWET8h_Para.Phase;
				Control.vref_LV = IUIaWET8h_U2;
				Control.iref_LV_Charge = IUIaWET8h_I1[IUIaWET8h_Para.CurrnetLevel];
				IUIaWET8h_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIaWET8h_U2*CV_tolerance)){
				IUIaWET8h_Para.Phase = PHASE_2;
			}
			if(IUIaWET8h_Para.T[1] >= IUIaWET8h_T1_MAX){
				IUIaWET8h_Para.Phase = PHASE_STOP;
			}
			if(IUIaWET8h_Para.T[1] <= IUIaWET8h_T1_MAX){
				IUIaWET8h_Para.T[1]++;
			}
			IUIaWET8h_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IUIaWET8h_Para.Phase != IUIaWET8h_Para.PhaseOld){
                IUIaWET8h_Para.PhaseOld = IUIaWET8h_Para.Phase;
				Control.vref_LV = IUIaWET8h_U2;
				Control.iref_LV_Charge = IUIaWET8h_I1[IUIaWET8h_Para.CurrnetLevel];
				IUIaWET8h_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IUIaWET8h_I3[IUIaWET8h_Para.CurrnetLevel]){
				IUIaWET8h_Para.Phase = PHASE_3;
			}
			if(IUIaWET8h_Para.T[2] >= IUIaWET8h_T2_MAX){
				IUIaWET8h_Para.Phase = PHASE_STOP;
			}
			if(IUIaWET8h_Para.T[2] <= IUIaWET8h_T2_MAX){
				IUIaWET8h_Para.T[2]++;
			}
			IUIaWET8h_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IUIaWET8h_Para.Phase != IUIaWET8h_Para.PhaseOld){
                IUIaWET8h_Para.PhaseOld = IUIaWET8h_Para.Phase;
				Control.iref_LV_Charge = IUIaWET8h_I3[IUIaWET8h_Para.CurrnetLevel];
				IUIaWET8h_Para.T3 = IUIaWET8h_Para.T[1] + IUIaWET8h_Para.T[2];
				IUIaWET8h_Para.T[3] = 0;
				IUIaWET8h_Para.T[9] = 0;
				if(IUIaWET8h_Para.T3 <= 1*HOUR){
					IUIaWET8h_Para.T3 = 1*HOUR;
				}
				else if(IUIaWET8h_Para.T3 >= 3*HOUR){
					IUIaWET8h_Para.T3 = 3*HOUR;
				}
			}
			if(IUIaWET8h_Para.T[9]==CVtoFC_Delay){
				Control.vref_LV = IUIaWET8h_U3;
			}
			if(IUIaWET8h_Para.T[3] >= IUIaWET8h_Para.T3){
				IUIaWET8h_Para.Phase = PHASE_4;
			}
			if(IUIaWET8h_Para.T[3] <= IUIaWET8h_Para.T3){
				IUIaWET8h_Para.T[3]++;
			}
			if(IUIaWET8h_Para.T[9]<CVtoFC_Delay){
				IUIaWET8h_Para.T[9]++;
			}
			IUIaWET8h_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IUIaWET8h_Para.Phase != IUIaWET8h_Para.PhaseOld){
                IUIaWET8h_Para.PhaseOld = IUIaWET8h_Para.Phase;
				Control.vref_LV = 0.0f;
				Control.iref_LV_Charge = 0.0f;
			}
			IUIaWET8h_Para.Proccess = PROC_FINISH;
		break;	
		case PHASE_STOP:
			if(IUIaWET8h_Para.Phase != IUIaWET8h_Para.PhaseOld){
				 IUIaWET8h_Para.PhaseOld = IUIaWET8h_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IUIaWET8h_Para.Proccess = PROC_ABORT;
		break;	
		default:
		break;

	}
}
//=======================================================================================================================
//IUIU0AGMDiscover_Para====================================================================================================
_st_IUIU0AGMDiscover IUIU0AGMDiscover_Para;
void API_CC_IUIU0AGMDiscover_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IUIU0AGMDiscover_Para.CurrnetLevel = IUIU0AGMDiscover_CL_0;	
    IUIU0AGMDiscover_Para.PhaseOld = PHASE_MAX;
    IUIU0AGMDiscover_Para.Phase = PHASE_INIT;
	IUIU0AGMDiscover_Para.Proccess = PROC_STOP;
	IUIU0AGMDiscover_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IUIU0AGMDiscover_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_IUIU0AGMDiscover_getProccess(void){
	return IUIU0AGMDiscover_Para.Proccess;
}
enum_CC_Phase API_CC_IUIU0AGMDiscover_getPhase(void){
	return IUIU0AGMDiscover_Para.Phase;
}

void API_CC_IUIU0AGMDiscover_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	IUIU0AGMDiscover_Para.CurrnetLevel = (enum_IUIU0AGMDiscover_CurrentLevel)para0;
	switch(IUIU0AGMDiscover_Para.Phase){
		case PHASE_INIT:
			if(IUIU0AGMDiscover_Para.Phase != IUIU0AGMDiscover_Para.PhaseOld){
                IUIU0AGMDiscover_Para.PhaseOld = IUIU0AGMDiscover_Para.Phase;
				API_CC_IUIU0AGMDiscover_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IUIU0AGMDiscover_Para.Phase = PHASE_1;
			}
			IUIU0AGMDiscover_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IUIU0AGMDiscover_Para.Phase != IUIU0AGMDiscover_Para.PhaseOld){
                IUIU0AGMDiscover_Para.PhaseOld = IUIU0AGMDiscover_Para.Phase;
				Control.vref_LV = IUIU0AGMDiscover_U2;
				Control.iref_LV_Charge = IUIU0AGMDiscover_I1[IUIU0AGMDiscover_Para.CurrnetLevel];
				IUIU0AGMDiscover_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIU0AGMDiscover_U2*CV_tolerance)){
				IUIU0AGMDiscover_Para.Phase = PHASE_2;
			}
			if(IUIU0AGMDiscover_Para.T[1] >= IUIU0AGMDiscover_T1_MAX){
				IUIU0AGMDiscover_Para.Phase = PHASE_STOP;
			}
			if(IUIU0AGMDiscover_Para.T[1] <= IUIU0AGMDiscover_T1_MAX){
				IUIU0AGMDiscover_Para.T[1]++;
			}
			IUIU0AGMDiscover_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IUIU0AGMDiscover_Para.Phase != IUIU0AGMDiscover_Para.PhaseOld){
                IUIU0AGMDiscover_Para.PhaseOld = IUIU0AGMDiscover_Para.Phase;
				Control.vref_LV = IUIU0AGMDiscover_U2;
				Control.iref_LV_Charge = IUIU0AGMDiscover_I1[IUIU0AGMDiscover_Para.CurrnetLevel];
				IUIU0AGMDiscover_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IUIU0AGMDiscover_I3[IUIU0AGMDiscover_Para.CurrnetLevel]){
				IUIU0AGMDiscover_Para.Phase = PHASE_3;
			}
			if(IUIU0AGMDiscover_Para.T[2] >= IUIU0AGMDiscover_T2_MAX){
				IUIU0AGMDiscover_Para.Phase = PHASE_STOP;
			}
			if(IUIU0AGMDiscover_Para.T[2] <= IUIU0AGMDiscover_T2_MAX){
				IUIU0AGMDiscover_Para.T[2]++;
			}
			IUIU0AGMDiscover_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IUIU0AGMDiscover_Para.Phase != IUIU0AGMDiscover_Para.PhaseOld){
                IUIU0AGMDiscover_Para.PhaseOld = IUIU0AGMDiscover_Para.Phase;
				Control.iref_LV_Charge = IUIU0AGMDiscover_I3[IUIU0AGMDiscover_Para.CurrnetLevel];
				IUIU0AGMDiscover_Para.T3 = IUIU0AGMDiscover_Para.T[1] + IUIU0AGMDiscover_Para.T[2];
				IUIU0AGMDiscover_Para.T[3] = 0;
				IUIU0AGMDiscover_Para.T[9] = 0;
				if(IUIU0AGMDiscover_Para.T3 <= IUIU0AGMDiscover_T3_MIN){
					IUIU0AGMDiscover_Para.T3 = IUIU0AGMDiscover_T3_MIN;
				}
				else if(IUIU0AGMDiscover_Para.T3 >= IUIU0AGMDiscover_T3_MAX){
					IUIU0AGMDiscover_Para.T3 = IUIU0AGMDiscover_T3_MAX;
				}
			}
			if(IUIU0AGMDiscover_Para.T[9]==CVtoFC_Delay){
				Control.vref_LV = IUIU0AGMDiscover_U3;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIU0AGMDiscover_U3*CV_tolerance)){
				IUIU0AGMDiscover_Para.Phase = PHASE_4;
			}
			if(IUIU0AGMDiscover_Para.T[3] >= IUIU0AGMDiscover_Para.T3){
				IUIU0AGMDiscover_Para.Phase = PHASE_4;
			}
			if(IUIU0AGMDiscover_Para.T[3] <= IUIU0AGMDiscover_Para.T3){
				IUIU0AGMDiscover_Para.T[3]++;
			}
			if(IUIU0AGMDiscover_Para.T[9]<CVtoFC_Delay){
				IUIU0AGMDiscover_Para.T[9]++;
			}
			IUIU0AGMDiscover_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IUIU0AGMDiscover_Para.Phase != IUIU0AGMDiscover_Para.PhaseOld){
                IUIU0AGMDiscover_Para.PhaseOld = IUIU0AGMDiscover_Para.Phase;
				Control.vref_LV = IUIU0AGMDiscover_U4;
				Control.iref_LV_Charge = IUIU0AGMDiscover_I3[IUIU0AGMDiscover_Para.CurrnetLevel];
			}
			IUIU0AGMDiscover_Para.Proccess = PROC_RUN;// float charge no finish
		break;	

		case PHASE_STOP:
			if(IUIU0AGMDiscover_Para.Phase != IUIU0AGMDiscover_Para.PhaseOld){
				 IUIU0AGMDiscover_Para.PhaseOld = IUIU0AGMDiscover_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IUIU0AGMDiscover_Para.Proccess = PROC_ABORT;
		break;	
		default:
			IUIU0AGMDiscover_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================

//IU0UAGMGeneric_Para====================================================================================================
_st_IU0UAGMGeneric IU0UAGMGeneric_Para;
void API_CC_IU0UAGMGeneric_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IU0UAGMGeneric_Para.CurrnetLevel = IU0UAGMGeneric_CL_0;	
    IU0UAGMGeneric_Para.PhaseOld = PHASE_MAX;
    IU0UAGMGeneric_Para.Phase = PHASE_INIT;
	IU0UAGMGeneric_Para.Proccess = PROC_STOP;
	IU0UAGMGeneric_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IU0UAGMGeneric_Para.T[cnt] = 0;
	}
}

enum_CC_Proccess API_CC_IU0UAGMGeneric_getProccess(void){
	return IU0UAGMGeneric_Para.Proccess;
}
enum_CC_Phase API_CC_IU0UAGMGeneric_getPhase(void){
	return IU0UAGMGeneric_Para.Phase;
}

void API_CC_IU0UAGMGeneric_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	IU0UAGMGeneric_Para.CurrnetLevel = (enum_IU0UAGMGeneric_CurrentLevel)para0;
	switch(IU0UAGMGeneric_Para.Phase){
		case PHASE_INIT:
			if(IU0UAGMGeneric_Para.Phase != IU0UAGMGeneric_Para.PhaseOld){
                IU0UAGMGeneric_Para.PhaseOld = IU0UAGMGeneric_Para.Phase;
				API_CC_IU0UAGMGeneric_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IU0UAGMGeneric_Para.Phase = PHASE_1;
			}
			IU0UAGMGeneric_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IU0UAGMGeneric_Para.Phase != IU0UAGMGeneric_Para.PhaseOld){
                IU0UAGMGeneric_Para.PhaseOld = IU0UAGMGeneric_Para.Phase;
				Control.vref_LV = IU0UAGMGeneric_U2;
				Control.iref_LV_Charge = IU0UAGMGeneric_I1[IU0UAGMGeneric_Para.CurrnetLevel];
				IU0UAGMGeneric_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IU0UAGMGeneric_U2*CV_tolerance)){
				IU0UAGMGeneric_Para.Phase = PHASE_2;
			}
			if(IU0UAGMGeneric_Para.T[1] >= IU0UAGMGeneric_T1_MAX){
				IU0UAGMGeneric_Para.Phase = PHASE_STOP;
			}
			if(IU0UAGMGeneric_Para.T[1] <= IU0UAGMGeneric_T1_MAX){
				IU0UAGMGeneric_Para.T[1]++;
			}
			IU0UAGMGeneric_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IU0UAGMGeneric_Para.Phase != IU0UAGMGeneric_Para.PhaseOld){
                IU0UAGMGeneric_Para.PhaseOld = IU0UAGMGeneric_Para.Phase;
				Control.vref_LV = IU0UAGMGeneric_U2;
				Control.iref_LV_Charge = IU0UAGMGeneric_I1[IU0UAGMGeneric_Para.CurrnetLevel];
				IU0UAGMGeneric_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IU0UAGMGeneric_I2[IU0UAGMGeneric_Para.CurrnetLevel]){
				IU0UAGMGeneric_Para.Phase = PHASE_3;
			}
			if(IU0UAGMGeneric_Para.T[2] >= IU0UAGMGeneric_T2_MAX){
				IU0UAGMGeneric_Para.Phase = PHASE_STOP;
			}
			if(IU0UAGMGeneric_Para.T[2] <= IU0UAGMGeneric_T2_MAX){
				IU0UAGMGeneric_Para.T[2]++;
			}
			IU0UAGMGeneric_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IU0UAGMGeneric_Para.Phase != IU0UAGMGeneric_Para.PhaseOld){
                IU0UAGMGeneric_Para.PhaseOld = IU0UAGMGeneric_Para.Phase;
				Control.vref_LV = IU0UAGMGeneric_U3;
				Control.iref_LV_Charge = IU0UAGMGeneric_I2[IU0UAGMGeneric_Para.CurrnetLevel];
				IU0UAGMGeneric_Para.T3 = IU0UAGMGeneric_T3;
				IU0UAGMGeneric_Para.T[3] = 0;
			}
			
			if(IU0UAGMGeneric_Para.T[3] >= IU0UAGMGeneric_Para.T3){
				IU0UAGMGeneric_Para.Phase = PHASE_4;
			}
			if(IU0UAGMGeneric_Para.T[3] <= IU0UAGMGeneric_Para.T3){
				IU0UAGMGeneric_Para.T[3]++;
			}
			IU0UAGMGeneric_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IU0UAGMGeneric_Para.Phase != IU0UAGMGeneric_Para.PhaseOld){
                IU0UAGMGeneric_Para.PhaseOld = IU0UAGMGeneric_Para.Phase;
				Control.vref_LV = IU0UAGMGeneric_U3;
				Control.iref_LV_Charge = IU0UAGMGeneric_I2[IU0UAGMGeneric_Para.CurrnetLevel];
			}
			IU0UAGMGeneric_Para.Proccess = PROC_RUN;// float charge no finish
		break;	

		case PHASE_STOP:
			if(IU0UAGMGeneric_Para.Phase != IU0UAGMGeneric_Para.PhaseOld){
				 IU0UAGMGeneric_Para.PhaseOld = IU0UAGMGeneric_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IU0UAGMGeneric_Para.Proccess = PROC_ABORT;
		break;	
		default:
			IU0UAGMGeneric_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================
//IUU0AGMFULLRIVER_Para====================================================================================================
_st_IUU0AGMFULLRIVER IUU0AGMFULLRIVER_Para;
void API_CC_IUU0AGMFULLRIVER_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IUU0AGMFULLRIVER_Para.CurrnetLevel = IUU0AGMFULLRIVER_CL_0;	
    IUU0AGMFULLRIVER_Para.PhaseOld = PHASE_MAX;
    IUU0AGMFULLRIVER_Para.Phase = PHASE_INIT;
	IUU0AGMFULLRIVER_Para.Proccess = PROC_STOP;
	IUU0AGMFULLRIVER_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IUU0AGMFULLRIVER_Para.T[cnt] = 0;
	}
}

enum_CC_Proccess API_CC_IUU0AGMFULLRIVER_getProccess(void){
	return IUU0AGMFULLRIVER_Para.Proccess;
}

enum_CC_Phase API_CC_IUU0AGMFULLRIVER_getPhase(void){
	return IUU0AGMFULLRIVER_Para.Phase;
}

void API_CC_IUU0AGMFULLRIVER_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	IUU0AGMFULLRIVER_Para.CurrnetLevel = (enum_IUU0AGMFULLRIVER_CurrentLevel)para0;
	switch(IUU0AGMFULLRIVER_Para.Phase){
		case PHASE_INIT:
			if(IUU0AGMFULLRIVER_Para.Phase != IUU0AGMFULLRIVER_Para.PhaseOld){
                IUU0AGMFULLRIVER_Para.PhaseOld = IUU0AGMFULLRIVER_Para.Phase;
				API_CC_IUU0AGMFULLRIVER_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IUU0AGMFULLRIVER_Para.Phase = PHASE_1;
			}
			IUU0AGMFULLRIVER_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IUU0AGMFULLRIVER_Para.Phase != IUU0AGMFULLRIVER_Para.PhaseOld){
                IUU0AGMFULLRIVER_Para.PhaseOld = IUU0AGMFULLRIVER_Para.Phase;
				Control.vref_LV = IUU0AGMFULLRIVER_U2;
				Control.iref_LV_Charge = IUU0AGMFULLRIVER_I1[IUU0AGMFULLRIVER_Para.CurrnetLevel];
				IUU0AGMFULLRIVER_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUU0AGMFULLRIVER_U2*CV_tolerance)){
				IUU0AGMFULLRIVER_Para.Phase = PHASE_2;
			}
			if(IUU0AGMFULLRIVER_Para.T[1] >= IUU0AGMFULLRIVER_T1_MAX){
				IUU0AGMFULLRIVER_Para.Phase = PHASE_STOP;
			}
			if(IUU0AGMFULLRIVER_Para.T[1] <= IUU0AGMFULLRIVER_T1_MAX){
				IUU0AGMFULLRIVER_Para.T[1]++;
			}
			IUU0AGMFULLRIVER_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IUU0AGMFULLRIVER_Para.Phase != IUU0AGMFULLRIVER_Para.PhaseOld){
                IUU0AGMFULLRIVER_Para.PhaseOld = IUU0AGMFULLRIVER_Para.Phase;
				Control.vref_LV = IUU0AGMFULLRIVER_U2;
				Control.iref_LV_Charge = IUU0AGMFULLRIVER_I1[IUU0AGMFULLRIVER_Para.CurrnetLevel];
				IUU0AGMFULLRIVER_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IUU0AGMFULLRIVER_I2[IUU0AGMFULLRIVER_Para.CurrnetLevel]){
				IUU0AGMFULLRIVER_Para.Phase = PHASE_3;
			}
			if(IUU0AGMFULLRIVER_Para.T[2] >= IUU0AGMFULLRIVER_T2_MAX){
				IUU0AGMFULLRIVER_Para.Phase = PHASE_STOP;
			}
			if(IUU0AGMFULLRIVER_Para.T[2] <= IUU0AGMFULLRIVER_T2_MAX){
				IUU0AGMFULLRIVER_Para.T[2]++;
			}
			IUU0AGMFULLRIVER_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IUU0AGMFULLRIVER_Para.Phase != IUU0AGMFULLRIVER_Para.PhaseOld){
                IUU0AGMFULLRIVER_Para.PhaseOld = IUU0AGMFULLRIVER_Para.Phase;
				Control.vref_LV = IUU0AGMFULLRIVER_U3;
				Control.iref_LV_Charge = IUU0AGMFULLRIVER_I2[IUU0AGMFULLRIVER_Para.CurrnetLevel];
				IUU0AGMFULLRIVER_Para.T3 = IUU0AGMFULLRIVER_T3;
				IUU0AGMFULLRIVER_Para.T[3] = 0;
			}
			
			if(IUU0AGMFULLRIVER_Para.T[3] >= IUU0AGMFULLRIVER_Para.T3){
				IUU0AGMFULLRIVER_Para.Phase = PHASE_4;
			}
			if(IUU0AGMFULLRIVER_Para.T[3] <= IUU0AGMFULLRIVER_Para.T3){
				IUU0AGMFULLRIVER_Para.T[3]++;
			}
			IUU0AGMFULLRIVER_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IUU0AGMFULLRIVER_Para.Phase != IUU0AGMFULLRIVER_Para.PhaseOld){
                IUU0AGMFULLRIVER_Para.PhaseOld = IUU0AGMFULLRIVER_Para.Phase;
				Control.vref_LV = IUU0AGMFULLRIVER_U3;
				Control.iref_LV_Charge = IUU0AGMFULLRIVER_I2[IUU0AGMFULLRIVER_Para.CurrnetLevel];
			}
			IUU0AGMFULLRIVER_Para.Proccess = PROC_RUN;// float charge no finish
		break;	

		case PHASE_STOP:
			if(IUU0AGMFULLRIVER_Para.Phase != IUU0AGMFULLRIVER_Para.PhaseOld){
				 IUU0AGMFULLRIVER_Para.PhaseOld = IUU0AGMFULLRIVER_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IUU0AGMFULLRIVER_Para.Proccess = PROC_ABORT;
		break;	
		default:
			IUU0AGMFULLRIVER_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================
//IUIaGEL_Para====================================================================================================
_st_IUIaGEL IUIaGEL_Para;
void API_CC_IUIaGEL_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IUIaGEL_Para.CurrnetLevel = IUIaGEL_CL_0;	
    IUIaGEL_Para.PhaseOld = PHASE_MAX;
    IUIaGEL_Para.Phase = PHASE_INIT;
	IUIaGEL_Para.Proccess = PROC_STOP;
	IUIaGEL_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IUIaGEL_Para.T[cnt] = 0;
	}
}

enum_CC_Proccess API_CC_IUIaGEL_getProccess(void){
	return IUIaGEL_Para.Proccess;
}
enum_CC_Phase API_CC_IUIaGEL_getPhase(void){
	return IUIaGEL_Para.Phase;
}

void API_CC_IUIaGEL_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	IUIaGEL_Para.CurrnetLevel = (enum_IUIaGEL_CurrentLevel)para0;
	switch(IUIaGEL_Para.Phase){
		case PHASE_INIT:
			if(IUIaGEL_Para.Phase != IUIaGEL_Para.PhaseOld){
                IUIaGEL_Para.PhaseOld = IUIaGEL_Para.Phase;
				API_CC_IUIaGEL_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IUIaGEL_Para.Phase = PHASE_1;
			}
			IUIaGEL_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IUIaGEL_Para.Phase != IUIaGEL_Para.PhaseOld){
                IUIaGEL_Para.PhaseOld = IUIaGEL_Para.Phase;
				Control.vref_LV = IUIaGEL_U2;
				Control.iref_LV_Charge = IUIaGEL_I1[IUIaGEL_Para.CurrnetLevel];
				IUIaGEL_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIaGEL_U2*CV_tolerance)){
				IUIaGEL_Para.Phase = PHASE_2;
			}
			if(IUIaGEL_Para.T[1] >= IUIaGEL_T1_MAX){
				IUIaGEL_Para.Phase = PHASE_STOP;
			}
			if(IUIaGEL_Para.T[1] <= IUIaGEL_T1_MAX){
				IUIaGEL_Para.T[1]++;
			}
			IUIaGEL_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IUIaGEL_Para.Phase != IUIaGEL_Para.PhaseOld){
                IUIaGEL_Para.PhaseOld = IUIaGEL_Para.Phase;
				Control.vref_LV = IUIaGEL_U2;
				Control.iref_LV_Charge = IUIaGEL_I1[IUIaGEL_Para.CurrnetLevel];
				IUIaGEL_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IUIaGEL_I3[IUIaGEL_Para.CurrnetLevel]){
				IUIaGEL_Para.Phase = PHASE_3;
			}
			if(IUIaGEL_Para.T[2] >= IUIaGEL_T1plusT2_MAX){
				IUIaGEL_Para.Phase = PHASE_STOP;
			}
			if(IUIaGEL_Para.T[2] <= IUIaGEL_T1plusT2_MAX){
				IUIaGEL_Para.T[2]++;
			}
			IUIaGEL_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IUIaGEL_Para.Phase != IUIaGEL_Para.PhaseOld){
                IUIaGEL_Para.PhaseOld = IUIaGEL_Para.Phase;
				Control.iref_LV_Charge = IUIaGEL_I3[IUIaGEL_Para.CurrnetLevel];
				IUIaGEL_Para.T3 = IUIaGEL_Para.T[1] + IUIaGEL_Para.T[2];
				IUIaGEL_Para.T[3] = 0;
				IUIaGEL_Para.T[9] = 0;
				if(IUIaGEL_Para.T3 <= IUIaGEL_T3_MIN){
					IUIaGEL_Para.T3 = IUIaGEL_T3_MIN;
				}
				else if(IUIaGEL_Para.T3 >= IUIaGEL_T3_MAX){
					IUIaGEL_Para.T3 = IUIaGEL_T3_MAX;
				}
				IUIaGEL_Para.T[3] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIaGEL_U3*CV_tolerance)){
				IUIaGEL_Para.Phase = PHASE_4;
			}
			if(IUIaGEL_Para.T[9] == CVtoFC_Delay){
				Control.vref_LV = IUIaGEL_U3;
			}
			if(IUIaGEL_Para.T[3] >= IUIaGEL_Para.T3){
				IUIaGEL_Para.Phase = PHASE_4;
			}
			if(IUIaGEL_Para.T[3] <= IUIaGEL_Para.T3){
				IUIaGEL_Para.T[3]++;
			}
			if(IUIaGEL_Para.T[9] < CVtoFC_Delay){
				IUIaGEL_Para.T[9]++;
			}	
			IUIaGEL_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IUIaGEL_Para.Phase != IUIaGEL_Para.PhaseOld){
                IUIaGEL_Para.PhaseOld = IUIaGEL_Para.Phase;
				Control.vref_LV = IUIaGEL_U4;
				Control.iref_LV_Charge = IUIaGEL_I3[IUIaGEL_Para.CurrnetLevel];
			}
			IUIaGEL_Para.Proccess = PROC_RUN;// float charge no finish
		break;	

		case PHASE_STOP:
			if(IUIaGEL_Para.Phase != IUIaGEL_Para.PhaseOld){
				 IUIaGEL_Para.PhaseOld = IUIaGEL_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IUIaGEL_Para.Proccess = PROC_ABORT;
		break;	
		default:
			IUIaGEL_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================
//IU0UZenith_Para====================================================================================================
_st_IU0UZenith IU0UZenith_Para;
void  API_CC_IU0UZenith_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IU0UZenith_Para.CurrnetLevel = IU0UZenith_CL_0;	
    IU0UZenith_Para.PhaseOld = PHASE_MAX;
    IU0UZenith_Para.Phase = PHASE_INIT;
	IU0UZenith_Para.Proccess = PROC_STOP;
	IU0UZenith_Para.T2 = 0;
	for(cnt=0;cnt<10;cnt++){
		IU0UZenith_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_IU0UZenith_getProccess(void){
	return IU0UZenith_Para.Proccess;
}
enum_CC_Phase API_CC_IU0UZenith_getPhase(void){
	return IU0UZenith_Para.Phase;
}

void API_CC_IU0UZenith_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	IU0UZenith_Para.CurrnetLevel = (enum_IU0UZenith_CurrentLevel)para0;
	switch(IU0UZenith_Para.Phase){
		case PHASE_INIT:
			if(IU0UZenith_Para.Phase != IU0UZenith_Para.PhaseOld){
                IU0UZenith_Para.PhaseOld = IU0UZenith_Para.Phase;
				API_CC_IU0UZenith_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IU0UZenith_Para.Phase = PHASE_1;
			}
			IU0UZenith_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IU0UZenith_Para.Phase != IU0UZenith_Para.PhaseOld){
                IU0UZenith_Para.PhaseOld = IU0UZenith_Para.Phase;
				Control.vref_LV = IU0UZenith_U2;
				Control.iref_LV_Charge = IU0UZenith_I1[IU0UZenith_Para.CurrnetLevel];
				IU0UZenith_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IU0UZenith_U2*CV_tolerance)){
				IU0UZenith_Para.Phase = PHASE_2;
			}
			if(IU0UZenith_Para.T[1] >= IU0UZenith_T1_MAX){
				IU0UZenith_Para.Phase = PHASE_STOP;
			}
			if(IU0UZenith_Para.T[1] <= IU0UZenith_T1_MAX){
				IU0UZenith_Para.T[1]++;
			}
			IU0UZenith_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IU0UZenith_Para.Phase != IU0UZenith_Para.PhaseOld){
                IU0UZenith_Para.PhaseOld = IU0UZenith_Para.Phase;
				Control.vref_LV = IU0UZenith_U2;
				Control.iref_LV_Charge = IU0UZenith_I1[IU0UZenith_Para.CurrnetLevel];
				IU0UZenith_Para.T2 = IU0UZenith_Para.T[1];
				IU0UZenith_Para.T[2] = 0;
				if(IU0UZenith_Para.T2 <= IU0UZenith_T2_MIN){
					IU0UZenith_Para.T2 = IU0UZenith_T2_MIN;
				}
				else if(IU0UZenith_Para.T2 >= IU0UZenith_T2_MAX){
					IU0UZenith_Para.T2 = IU0UZenith_T2_MAX;
				}
			}
			if(IU0UZenith_Para.T[2] >= IU0UZenith_Para.T2){
				IU0UZenith_Para.Phase = PHASE_3;
			}
			if(IU0UZenith_Para.T[2] <= IU0UZenith_T2_MAX){
				IU0UZenith_Para.T[2]++;
			}
			IU0UZenith_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IU0UZenith_Para.Phase != IU0UZenith_Para.PhaseOld){
                IU0UZenith_Para.PhaseOld = IU0UZenith_Para.Phase;
				Control.vref_LV = IU0UZenith_U3;
				Control.iref_LV_Charge = IU0UZenith_I1[IU0UZenith_Para.CurrnetLevel];
			}
			IU0UZenith_Para.Proccess = PROC_RUN;
		break;
		case PHASE_STOP:
			if(IU0UZenith_Para.Phase != IU0UZenith_Para.PhaseOld){
				 IU0UZenith_Para.PhaseOld = IU0UZenith_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IU0UZenith_Para.Proccess = PROC_ABORT;
		break;	
		default:
			IU0UZenith_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================
//LithiumRelion====================================================================================================
_st_LithiumRelion LithiumRelion_Para;
void  API_CC_LithiumRelion_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	LithiumRelion_Para.CurrnetLevel = LithiumRelion_CL_0;	
    LithiumRelion_Para.PhaseOld = PHASE_MAX;
    LithiumRelion_Para.Phase = PHASE_INIT;
	LithiumRelion_Para.Proccess = PROC_STOP;
	for(cnt=0;cnt<10;cnt++){
		LithiumRelion_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_LithiumRelion_getProccess(void){
	return LithiumRelion_Para.Proccess;
}
enum_CC_Phase API_CC_LithiumRelion_getPhase(void){
	return LithiumRelion_Para.Phase;
}

void API_CC_LithiumRelion_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	LithiumRelion_Para.CurrnetLevel = (enum_LithiumRelion_CurrentLevel)para0;
	switch(LithiumRelion_Para.Phase){
		case PHASE_INIT:
			if(LithiumRelion_Para.Phase != LithiumRelion_Para.PhaseOld){
                LithiumRelion_Para.PhaseOld = LithiumRelion_Para.Phase;
				API_CC_LithiumRelion_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				LithiumRelion_Para.Phase = PHASE_1;
			}
			LithiumRelion_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(LithiumRelion_Para.Phase != LithiumRelion_Para.PhaseOld){
                LithiumRelion_Para.PhaseOld = LithiumRelion_Para.Phase;
				Control.vref_LV = LithiumRelion_U2;
				Control.iref_LV_Charge = LithiumRelion_I1[LithiumRelion_Para.CurrnetLevel];
				LithiumRelion_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(LithiumRelion_U2*CV_tolerance)){
				LithiumRelion_Para.Phase = PHASE_2;
			}
			if(LithiumRelion_Para.T[1] >= LithiumRelion_T1_MAX){
				LithiumRelion_Para.Phase = PHASE_STOP;
			}
			if(LithiumRelion_Para.T[1] <= LithiumRelion_T1_MAX){
				LithiumRelion_Para.T[1]++;
			}
			LithiumRelion_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(LithiumRelion_Para.Phase != LithiumRelion_Para.PhaseOld){
                LithiumRelion_Para.PhaseOld = LithiumRelion_Para.Phase;
				Control.vref_LV = LithiumRelion_U2;
				Control.iref_LV_Charge = LithiumRelion_I1[LithiumRelion_Para.CurrnetLevel];
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=LithiumRelion_I2[LithiumRelion_Para.CurrnetLevel]){
				LithiumRelion_Para.Phase = PHASE_3;
			}
			if(LithiumRelion_Para.T[2] >= LithiumRelion_T2_MAX){
				LithiumRelion_Para.Phase = PHASE_STOP;
			}
			if(LithiumRelion_Para.T[2] <= LithiumRelion_T2_MAX){
				LithiumRelion_Para.T[2]++;
			}
			LithiumRelion_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(LithiumRelion_Para.Phase != LithiumRelion_Para.PhaseOld){
                LithiumRelion_Para.PhaseOld = LithiumRelion_Para.Phase;
				Control.vref_LV = 0.0f;
				Control.iref_LV_Charge = 0.0f;
			}
			LithiumRelion_Para.Proccess = PROC_FINISH;
		break;
		case PHASE_STOP:
			if(LithiumRelion_Para.Phase != LithiumRelion_Para.PhaseOld){
				 LithiumRelion_Para.PhaseOld = LithiumRelion_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			LithiumRelion_Para.Proccess = PROC_ABORT;
		break;	
		default:
			LithiumRelion_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================

//IUIaAGMTrojan_Para====================================================================================================
_st_IUIaAGMTrojan IUIaAGMTrojan_Para;
void API_CC_IUIaAGMTrojan_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	IUIaAGMTrojan_Para.CurrnetLevel = IUIaAGMTrojan_CL_0;	
    IUIaAGMTrojan_Para.PhaseOld = PHASE_MAX;
    IUIaAGMTrojan_Para.Phase = PHASE_INIT;
	IUIaAGMTrojan_Para.Proccess = PROC_STOP;
	IUIaAGMTrojan_Para.T3 = 0;
	for(cnt=0;cnt<10;cnt++){
		IUIaAGMTrojan_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_IUIaAGMTrojan_getProccess(void){
	return IUIaAGMTrojan_Para.Proccess;
}
enum_CC_Phase API_CC_IUIaAGMTrojan_getPhase(void){
	return IUIaAGMTrojan_Para.Phase;
}
void API_CC_IUIaAGMTrojan_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	BF_U32 tempbf;
	tempbf.all = 0;
	UINT8 reset = bf.bit.b0;
	IUIaAGMTrojan_Para.CurrnetLevel = (enum_IUIaAGMTrojan_CurrentLevel)para0;
	switch(IUIaAGMTrojan_Para.Phase){
		case PHASE_INIT:
			if(IUIaAGMTrojan_Para.Phase != IUIaAGMTrojan_Para.PhaseOld){
                IUIaAGMTrojan_Para.PhaseOld = IUIaAGMTrojan_Para.Phase;  
				API_CC_IUIaAGMTrojan_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				IUIaAGMTrojan_Para.Phase = PHASE_1;
			};
			IUIaAGMTrojan_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(IUIaAGMTrojan_Para.Phase != IUIaAGMTrojan_Para.PhaseOld){
                IUIaAGMTrojan_Para.PhaseOld = IUIaAGMTrojan_Para.Phase;
				Control.vref_LV = IUIaAGMTrojan_U2;
				Control.iref_LV_Charge = IUIaAGMTrojan_I1[IUIaAGMTrojan_Para.CurrnetLevel];
				IUIaAGMTrojan_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(IUIaAGMTrojan_U2*CV_tolerance)){
				IUIaAGMTrojan_Para.Phase = PHASE_2;
			}
			if(IUIaAGMTrojan_Para.T[1] >= IUIaAGMTrojan_T1_MAX){
				IUIaAGMTrojan_Para.Phase = PHASE_STOP;
			}
			if(IUIaAGMTrojan_Para.T[1] <= IUIaAGMTrojan_T1_MAX){
				IUIaAGMTrojan_Para.T[1]++;
			}
			if((IUIaAGMTrojan_Para.T[1]+IUIaAGMTrojan_Para.T[2]+IUIaAGMTrojan_Para.T[3])>=IUIaAGMTrojan_T1T2T3_MAX){
				IUIaAGMTrojan_Para.Phase = PHASE_STOP;
			}
			IUIaAGMTrojan_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(IUIaAGMTrojan_Para.Phase != IUIaAGMTrojan_Para.PhaseOld){
                IUIaAGMTrojan_Para.PhaseOld = IUIaAGMTrojan_Para.Phase;
				Control.vref_LV = IUIaAGMTrojan_U2;
				Control.iref_LV_Charge = IUIaAGMTrojan_I1[IUIaAGMTrojan_Para.CurrnetLevel];
				IUIaAGMTrojan_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=IUIaAGMTrojan_I3[IUIaAGMTrojan_Para.CurrnetLevel]){
				IUIaAGMTrojan_Para.Phase = PHASE_3;
			}
			if(IUIaAGMTrojan_Para.T[2] >= IUIaAGMTrojan_T2_MAX){
				IUIaAGMTrojan_Para.Phase = PHASE_STOP;
			}
			if(IUIaAGMTrojan_Para.T[2] <= IUIaAGMTrojan_T2_MAX){
				IUIaAGMTrojan_Para.T[2]++;
			}
			if((IUIaAGMTrojan_Para.T[1]+IUIaAGMTrojan_Para.T[2]+IUIaAGMTrojan_Para.T[3])>=IUIaAGMTrojan_T1T2T3_MAX){
				IUIaAGMTrojan_Para.Phase = PHASE_STOP;
			}
			IUIaAGMTrojan_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(IUIaAGMTrojan_Para.Phase != IUIaAGMTrojan_Para.PhaseOld){
                IUIaAGMTrojan_Para.PhaseOld = IUIaAGMTrojan_Para.Phase;
				Control.iref_LV_Charge = IUIaAGMTrojan_I3[IUIaAGMTrojan_Para.CurrnetLevel];
				IUIaAGMTrojan_Para.T3 = IUIaAGMTrojan_Para.T[1]>>1;
				if(IUIaAGMTrojan_Para.T3 <= IUIaAGMTrojan_T3_MIN){
					IUIaAGMTrojan_Para.T3 = IUIaAGMTrojan_T3_MIN;
				}
				else if(IUIaAGMTrojan_Para.T3 >= IUIaAGMTrojan_T3_MAX){
					IUIaAGMTrojan_Para.T3 = IUIaAGMTrojan_T3_MAX;
				}
				IUIaAGMTrojan_Para.T[3] = 0;
				IUIaAGMTrojan_Para.T[9] = 0;
				IUIaAGMTrojan_Para.T[8] = 0;
			}
			IUIaAGMTrojan_Para.T[9]++;
			if(IUIaAGMTrojan_Para.T[8]==CVtoFC_Delay){
				Control.vref_LV = IUIaAGMTrojan_U3;
			}
			if(IUIaAGMTrojan_Para.T[9] >= IUIaAGMTrojan_T3_SlopeCheck){
				IUIaAGMTrojan_Para.T[9] = 0;
				IUIaAGMTrojan_Para.Vout_T3 = API_VS_Get_Volt(&VS_vLV_BAT);
				if((IUIaAGMTrojan_Para.Vout_T3 - IUIaAGMTrojan_Para.Vout_T3_old)<=IUIaAGMTrojan_Slope){
					IUIaAGMTrojan_Para.Phase = PHASE_4;
				}
				 IUIaAGMTrojan_Para.Vout_T3_old = IUIaAGMTrojan_Para.Vout_T3;
			}
			if(IUIaAGMTrojan_Para.T[3] >= IUIaAGMTrojan_Para.T3){
				IUIaAGMTrojan_Para.Phase = PHASE_4;
			}
			if(IUIaAGMTrojan_Para.T[3] <= IUIaAGMTrojan_Para.T3){
				IUIaAGMTrojan_Para.T[3]++;
			}
			if(IUIaAGMTrojan_Para.T[8]<CVtoFC_Delay){
				IUIaAGMTrojan_Para.T[8]++;
			}
			if((IUIaAGMTrojan_Para.T[1]+IUIaAGMTrojan_Para.T[2]+IUIaAGMTrojan_Para.T[3])>=IUIaAGMTrojan_T1T2T3_MAX){
				IUIaAGMTrojan_Para.Phase = PHASE_STOP;
			}
			IUIaAGMTrojan_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(IUIaAGMTrojan_Para.Phase != IUIaAGMTrojan_Para.PhaseOld){
                IUIaAGMTrojan_Para.PhaseOld = IUIaAGMTrojan_Para.Phase;
				Control.vref_LV = 0.0f;
				Control.iref_LV_Charge = 0.0f;
			}
			IUIaAGMTrojan_Para.Proccess = PROC_FINISH;
		break;	

		case PHASE_STOP:
			if(IUIaAGMTrojan_Para.Phase != IUIaAGMTrojan_Para.PhaseOld){
				 IUIaAGMTrojan_Para.PhaseOld = IUIaAGMTrojan_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			IUIaAGMTrojan_Para.Proccess = PROC_ABORT;
		break;	
		default:
		break;

	}       
}
//=======================================================================================================================
//TrojanTrillium_Para====================================================================================================
_st_TrojanTrillium TrojanTrillium_Para;
void  API_CC_TrojanTrillium_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	TrojanTrillium_Para.CurrnetLevel = TrojanTrillium_CL_0;	
    TrojanTrillium_Para.PhaseOld = PHASE_MAX;
    TrojanTrillium_Para.Phase = PHASE_INIT;
	TrojanTrillium_Para.Proccess = PROC_STOP;
	for(cnt=0;cnt<10;cnt++){
		TrojanTrillium_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_TrojanTrillium_getProccess(void){
	return TrojanTrillium_Para.Proccess;
}
enum_CC_Phase API_CC_TrojanTrillium_getPhase(void){
	return TrojanTrillium_Para.Phase;
}

void API_CC_TrojanTrillium_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	UINT8 reset = bf.bit.b0;
	BF_U32 tempbf;
	tempbf.all = 0;
	TrojanTrillium_Para.CurrnetLevel = (enum_TrojanTrillium_CurrentLevel)para0;
	switch(TrojanTrillium_Para.Phase){
		case PHASE_INIT:
			if(TrojanTrillium_Para.Phase != TrojanTrillium_Para.PhaseOld){
                TrojanTrillium_Para.PhaseOld = TrojanTrillium_Para.Phase;
				API_CC_TrojanTrillium_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				TrojanTrillium_Para.Phase = PHASE_1;
			}
			TrojanTrillium_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(TrojanTrillium_Para.Phase != TrojanTrillium_Para.PhaseOld){
                TrojanTrillium_Para.PhaseOld = TrojanTrillium_Para.Phase;
				Control.vref_LV = TrojanTrillium_U2;
				Control.iref_LV_Charge = TrojanTrillium_I1[TrojanTrillium_Para.CurrnetLevel];
				TrojanTrillium_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(TrojanTrillium_U2*CV_tolerance)){
				TrojanTrillium_Para.Phase = PHASE_2;
			}
			if(TrojanTrillium_Para.T[1] >= TrojanTrillium_T1_MAX){
				TrojanTrillium_Para.Phase = PHASE_STOP;
			}
			if(TrojanTrillium_Para.T[1] <= TrojanTrillium_T1_MAX){
				TrojanTrillium_Para.T[1]++;
			}
			TrojanTrillium_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(TrojanTrillium_Para.Phase != TrojanTrillium_Para.PhaseOld){
                TrojanTrillium_Para.PhaseOld = TrojanTrillium_Para.Phase;
				Control.vref_LV = TrojanTrillium_U2;
				Control.iref_LV_Charge = TrojanTrillium_I1[TrojanTrillium_Para.CurrnetLevel];
				TrojanTrillium_Para.T[2] = 0;
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge) <= TrojanTrillium_I2[TrojanTrillium_Para.CurrnetLevel]){
				TrojanTrillium_Para.Phase = PHASE_3;
			}
			if(TrojanTrillium_Para.T[2] >= TrojanTrillium_T2_MAX){
				TrojanTrillium_Para.Phase = PHASE_3;
			}
			if(TrojanTrillium_Para.T[2] <= TrojanTrillium_T2_MAX){
				TrojanTrillium_Para.T[2]++;
			}
			TrojanTrillium_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(TrojanTrillium_Para.Phase != TrojanTrillium_Para.PhaseOld){
                TrojanTrillium_Para.PhaseOld = TrojanTrillium_Para.Phase;
				Control.vref_LV = 0.0f;
				Control.iref_LV_Charge = 0.0f;
			}
			TrojanTrillium_Para.Proccess = PROC_FINISH;
		break;
		case PHASE_STOP:
			if(TrojanTrillium_Para.Phase != TrojanTrillium_Para.PhaseOld){
				 TrojanTrillium_Para.PhaseOld = TrojanTrillium_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			TrojanTrillium_Para.Proccess = PROC_ABORT;
		break;	
		default:
			TrojanTrillium_Para.Proccess = PROC_ABORT;
		break;

	}
}
//=======================================================================================================================
//AGMHoppecke_Para====================================================================================================
_st_AGMHoppecke AGMHoppecke_Para;
void API_CC_AGMHoppecke_Init(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2){
	UINT8 cnt = 0;
	AGMHoppecke_Para.CurrnetLevel = AGMHoppecke_CL_0;	
    AGMHoppecke_Para.PhaseOld = PHASE_MAX;
    AGMHoppecke_Para.Phase = PHASE_INIT;
	AGMHoppecke_Para.Proccess = PROC_STOP;
	AGMHoppecke_Para.T4 = 0;
	for(cnt=0;cnt<10;cnt++){
		AGMHoppecke_Para.T[cnt] = 0;
	}
}
enum_CC_Proccess API_CC_AGMHoppecke_getProccess(void){
	return AGMHoppecke_Para.Proccess;
}
enum_CC_Phase API_CC_AGMHoppecke_getPhase(void){
	return AGMHoppecke_Para.Phase;
}

void API_CC_AGMHoppecke_Run(BF_U32 bf,           UINT8 para0, UINT64 para1, UINT64 para2){ //in 1s loop
	BF_U32 tempbf;
	tempbf.all = 0;
	UINT8 reset = bf.bit.b0;
	AGMHoppecke_Para.CurrnetLevel = (enum_AGMHoppecke_CurrentLevel)para0;
	switch(AGMHoppecke_Para.Phase){
		case PHASE_INIT:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
                AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;  
				API_CC_AGMHoppecke_Init(tempbf,0,0,0);
			}	
			if(reset != 1){
				if(API_VS_Get_Volt(&VS_vLV_BAT)<=AGMHoppecke_UT0){
					AGMHoppecke_Para.Phase = PHASE_STOP;
				}
				else{
					AGMHoppecke_Para.Phase = PHASE_1;
				}
			};
			AGMHoppecke_Para.Proccess = PROC_STOP;
		break;
		case PHASE_1:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
                AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;
				Control.vref_LV = AGMHoppecke_UT1;
				Control.iref_LV_Charge = AGMHoppecke_Ids[AGMHoppecke_Para.CurrnetLevel];
				AGMHoppecke_Para.T[1] = 0;
			}
			if(API_VS_Get_Volt(&VS_vLV_BAT)>=(AGMHoppecke_UT1*CV_tolerance)){
				AGMHoppecke_Para.Phase = PHASE_2;
			}
			if(AGMHoppecke_Para.T[1] >= AGMHoppecke_T1_MAX){
				AGMHoppecke_Para.Phase = PHASE_STOP;
			}
			if(AGMHoppecke_Para.T[1] <= AGMHoppecke_T1_MAX){
				AGMHoppecke_Para.T[1]++;
			}
			AGMHoppecke_Para.Proccess = PROC_RUN;
		break;
		case PHASE_2:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
                AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;
				Control.vref_LV = AGMHoppecke_UT3;
				Control.iref_LV_Charge = AGMHoppecke_I1[AGMHoppecke_Para.CurrnetLevel];
				AGMHoppecke_Para.T[2] = 0;
			}
			if((API_VS_Get_Volt(&VS_vLV_BAT)>=AGMHoppecke_UT3*CV_tolerance)){
				AGMHoppecke_Para.Phase = PHASE_3;
			}
			if(AGMHoppecke_Para.T[2] >= AGMHoppecke_T2T3_MAX){
				AGMHoppecke_Para.Phase = PHASE_STOP;
			}
			if(AGMHoppecke_Para.T[2] <= AGMHoppecke_T2T3_MAX){
				AGMHoppecke_Para.T[2]++;
			}
			AGMHoppecke_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_3:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
                AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;
				Control.vref_LV = AGMHoppecke_UT3;
				Control.iref_LV_Charge = AGMHoppecke_I1[AGMHoppecke_Para.CurrnetLevel];
				AGMHoppecke_Para.T[3] = 0; 
			}
			if(API_CS_Get_Ampere(&CS_iLV_Charge)<=AGMHoppecke_I2[AGMHoppecke_Para.CurrnetLevel]){
				AGMHoppecke_Para.Phase = PHASE_4;
			}
			if(AGMHoppecke_Para.T[2] + AGMHoppecke_Para.T[3] >= AGMHoppecke_T2T3_MAX){
				AGMHoppecke_Para.Phase = PHASE_STOP;
			}
			if(AGMHoppecke_Para.T[3] <= AGMHoppecke_T2T3_MAX){
				AGMHoppecke_Para.T[3]++;
			}
			AGMHoppecke_Para.Proccess = PROC_RUN;
		break;
		case PHASE_4:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
                AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;
				Control.vref_LV = AGMHoppecke_UT4;
				Control.iref_LV_Charge = AGMHoppecke_I2[AGMHoppecke_Para.CurrnetLevel];
				AGMHoppecke_Para.T[4] = 0;
				AGMHoppecke_Para.T4 = (AGMHoppecke_Para.T[2]+AGMHoppecke_Para.T[3])>>1;
			}
			if(AGMHoppecke_Para.T[4] >= AGMHoppecke_Para.T4){
				AGMHoppecke_Para.Phase = PHASE_5;
			}
			if((AGMHoppecke_Para.T[2]+AGMHoppecke_Para.T[3]+AGMHoppecke_Para.T[4]) <= AGMHoppecke_T2T3T4_MAX){
				AGMHoppecke_Para.T[4]++;
			}
			AGMHoppecke_Para.Proccess = PROC_RUN;
		break;	
		case PHASE_5:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
                AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;
				Control.vref_LV = AGMHoppecke_UT5;
				Control.iref_LV_Charge = AGMHoppecke_I2[AGMHoppecke_Para.CurrnetLevel];
			}
			AGMHoppecke_Para.Proccess = PROC_RUN;
		break;
		case PHASE_STOP:
			if(AGMHoppecke_Para.Phase != AGMHoppecke_Para.PhaseOld){
				 AGMHoppecke_Para.PhaseOld = AGMHoppecke_Para.Phase;
				 Control.vref_LV = 0.0f;
				 Control.iref_LV_Charge = 0.0f;
			}
			AGMHoppecke_Para.Proccess = PROC_ABORT;
		break;	
		default:
		break;

	}
        
}
//=======================================================================================================================

void (*API_CC_Battery_Run[BP_MAX])(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2) = {
	API_CC_IUIaWET12h_Run,
	API_CC_IUIaWET8h_Run,	
	API_CC_IUIU0AGMDiscover_Run,	
	API_CC_IU0UAGMGeneric_Run,
	API_CC_IUU0AGMFULLRIVER_Run,
	API_CC_IUIaGEL_Run,
	API_CC_IU0UZenith_Run,
	API_CC_LithiumRelion_Run,
	API_CC_IUIaAGMTrojan_Run,
	API_CC_TrojanTrillium_Run,
	API_CC_AGMHoppecke_Run,
};	
void (*API_CC_Battery_Init[BP_MAX])(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2) = {
	API_CC_IUIaWET12h_Init,
	API_CC_IUIaWET8h_Init,	
	API_CC_IUIU0AGMDiscover_Init,	
	API_CC_IU0UAGMGeneric_Init,
	API_CC_IUU0AGMFULLRIVER_Init,
	API_CC_IUIaGEL_Init,
	API_CC_IU0UZenith_Init,
	API_CC_LithiumRelion_Init,
	API_CC_IUIaAGMTrojan_Init,
	API_CC_TrojanTrillium_Init,
	API_CC_AGMHoppecke_Init,
};	
enum_CC_Proccess (*API_CC_Battery_getProccess[BP_MAX])(void) = {
	API_CC_IUIaWET12h_getProccess,
	API_CC_IUIaWET8h_getProccess,	
	API_CC_IUIU0AGMDiscover_getProccess,	
	API_CC_IU0UAGMGeneric_getProccess,
	API_CC_IUU0AGMFULLRIVER_getProccess,
	API_CC_IUIaGEL_getProccess,
	API_CC_IU0UZenith_getProccess,
	API_CC_LithiumRelion_getProccess,
	API_CC_IUIaAGMTrojan_getProccess,
	API_CC_TrojanTrillium_getProccess,
	API_CC_AGMHoppecke_getProccess,
};	
	
enum_CC_Phase (*API_CC_Battery_getPhase[BP_MAX])(void) = {
	API_CC_IUIaWET12h_getPhase,
	API_CC_IUIaWET8h_getPhase,	
	API_CC_IUIU0AGMDiscover_getPhase,	
	API_CC_IU0UAGMGeneric_getPhase,
	API_CC_IUU0AGMFULLRIVER_getPhase,
	API_CC_IUIaGEL_getPhase,
	API_CC_IU0UZenith_getPhase,
	API_CC_LithiumRelion_getPhase,
	API_CC_IUIaAGMTrojan_getPhase,
	API_CC_TrojanTrillium_getPhase,
	API_CC_AGMHoppecke_getPhase,
};	

