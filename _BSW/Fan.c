/*
 * Fan.c
 *
 *  Created on: Feb 6, 2020
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "Fan.h"

void API_Fan_Config_VR(Fan_t* tempFan, VoltageRegulator_t* vr){

	tempFan->VR = vr;
	
}
void API_Fan_Config_Clock(Fan_t* tempFan, float clock){

	tempFan->clock = clock;	
	
}
void API_Fan_Config_Pulse(Fan_t* tempFan, UINT8 pulse){

	tempFan->pulse = pulse;	
	
}

float API_Fan_Get_rpm(Fan_t* tempFan){

	return tempFan->rpm;
}

UINT8 API_Fan_Get_fail(Fan_t* tempFan){

	return tempFan->fail;
}

void API_Fan_Set_Duty(Fan_t* tempFan, UINT16 duty){

	tempFan->duty = duty;
	if(duty == 0){
		tempFan->ONOFF = 0;
		tempFan->fail = 0;
		tempFan->rpm = 0;
	}
	else{
		tempFan->ONOFF = 1;
	}
	API_VR_Set_out(tempFan->VR,tempFan->duty);
	
}


void API_Fan_SpeedCalc(Fan_t* tempFan){
	UINT32 ICDiff = 0;
	if(tempFan->FGState == 0){
    	// Get the 1st Input Capture value 
    	tempFan->IC1 = API_IC_GET_COMPARE(IC_CHL_FAN);
   		tempFan->FGState = 1;
  	}
  	else if(tempFan->FGState == 1){
	    // Get the 2nd Input Capture value 
	   tempFan->IC2 = API_IC_GET_COMPARE(IC_CHL_FAN);
	    // Capture computation 
	    if (tempFan->IC2 > tempFan->IC1){
	      ICDiff = (tempFan->IC2 - tempFan->IC1); 
	    }
	    else if (tempFan->IC2 < tempFan->IC1){
	      // 0xFFFF is max TIM1_CCRx value 
	      ICDiff = ((0xFFFF - tempFan->IC1) + tempFan->IC2) + 1;
	    }
	    else{
	      // If capture values are equal, we have reached the limit of frequency measures
	      Error_Handler();
	    }
		tempFan->rpm = ((tempFan->clock / ICDiff)/tempFan->pulse)*60;
		tempFan->FGState = 0;
	    API_IC_RESET_CNT(IC_CHL_FAN);
		tempFan->fail = 0;
    } 
}
void API_Fan_Fail(Fan_t* tempFan){

	if(tempFan->ONOFF==1){
		tempFan->IC1 = 0;
		tempFan->IC2 = 0;
	  	tempFan->FGState = 0;
		tempFan->rpm = 0;
		tempFan->fail = 1;
	}
}



