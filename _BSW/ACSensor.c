/*
 * ACSensor.c
 *
 *  Created on: Feb 19, 2020
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "ACSensor.h"
void API_ACS_Config_Hysteresis(ACSensor_t* Inst, UINT16 hysteresis){
	Inst->Hysteresis = hysteresis;
}
void API_ACS_Config_Threshold(ACSensor_t* Inst, UINT16 Threshold){
	Inst->Threshold = Threshold;
}
void API_ACS_Config_AVG_num(ACSensor_t* Inst, UINT16 AVG_num){
	Inst->AVG_num = AVG_num;
}
void API_ACS_Config_AVG_num_div(ACSensor_t* Inst, UINT16 AVG_num_div){
	Inst->AVG_num_div = AVG_num_div;
}
UINT16 API_ACS_Get_ValuePeakAVG(ACSensor_t* Inst){
	return Inst->ValuePeakAVG;
}
void API_ACS_Peak(ACSensor_t* Inst,UINT16 Data){
	switch(Inst->State){
		case ACSensor_Idle:
			if(Inst->State!=Inst->StateOld){
				Inst->StateOld = Inst->State;
				//AVG algorithm
				Inst->ValuePeakSUM += Inst->ValuePeak;
				if(Inst->ValuePeakSUM > Inst->ValuePeakAVG_BUF[Inst->Index]){//prevent overflow
					Inst->ValuePeakSUM -= Inst->ValuePeakAVG_BUF[Inst->Index];
				}
				else{
					Inst->ValuePeakSUM = 0;
				}
				Inst->ValuePeakAVG_BUF[Inst->Index] = Inst->ValuePeak;
				Inst->ValuePeakAVG = Inst->ValuePeakSUM >> Inst->AVG_num_div;
				//Circular Buffer
				if(++Inst->Index >= Inst->AVG_num){
					Inst->Index = 0;
				}
			}
			if(Data>Inst->Threshold){
				Inst->State = ACSensor_Peak;
			}
			break;
	
		case ACSensor_Peak:
			if(Inst->State != Inst->StateOld){
				Inst->StateOld = Inst->State;
				Inst->ValuePeak = 0;
			}
			if(Data > Inst->ValuePeak){
				Inst->ValuePeak = Data;
			}
			if(Data <= (Inst->Threshold-Inst->Hysteresis)){
				Inst->State = ACSensor_Idle;
			}
			break;
	}
}
