/*
 * ACSensor.h
 *
 *  Created on: Feb 19, 2020
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_COMPONENT_ACSENSOR_H_
#define BSW_COMPONENT_ACSENSOR_H_
#include "TypeDef.h"

typedef enum
{
	ACSensor_Idle,
	ACSensor_Peak,         		

} ACSensor_State_t;

typedef struct{
    //Configuration
    UINT16 AVG_num_div; //平均右移位元數
    UINT16 AVG_num;		//平均數量
    UINT16 Threshold;
	UINT16 Hysteresis;
    //instance
    ACSensor_State_t State;
	ACSensor_State_t StateOld;
	UINT16 ValuePeak;
	UINT16 ValuePeakAVG_BUF[64];
	UINT16 ValuePeakAVG;
	UINT32 ValuePeakSUM;
	UINT8 Index;
} ACSensor_t;

extern void API_ACS_Config_Threshold(ACSensor_t* Inst, UINT16 Threshold);
extern void API_ACS_Config_AVG_num(ACSensor_t* Inst, UINT16 AVG_num);
extern void API_ACS_Config_AVG_num_div(ACSensor_t* Inst, UINT16 AVG_num_div);
extern void API_ACS_Peak(ACSensor_t* Inst,UINT16 Data);

#endif /* BSW_COMPONENT_ACSENSOR_H_ */
