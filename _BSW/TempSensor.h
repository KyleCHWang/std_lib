/*
 * TempSensor.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef COMPONENT_TEMPSENSOR_H_
#define COMPONENT_TEMPSENSOR_H_

#include "TypeDef.h"
#include "IQ_Math.h"
#include "MCAL_ADC.h"
//API

//CFG
typedef struct
 {
   //Configuration
   enumADC_CHL_t  ADCchanel;
   float Tmax;
   float Tmin;
   UINT8 T0index;
   UINT8 Ttablesize;
   const UINT16* Ttable;
   //Instance
   UINT16 valueQ12_raw;
   UINT16 valueQ12_filtered;
   INT16 integer;
   INT16 point;
   float Cels;
   float Fahr;
   UINT8 fault;
 } TemperatureSensor_t;
 
extern void API_TS_Config_ADCchanel(TemperatureSensor_t* tempinstance,enumADC_CHL_t tempdata);
extern void API_TS_Config_Tmax(TemperatureSensor_t* tempinstance,float max);
extern void API_TS_Config_Tmin(TemperatureSensor_t* tempinstance,float min);
extern void API_TS_Config_T0index(TemperatureSensor_t* tempinstance,UINT8 index);
extern void API_TS_Config_Ttable(TemperatureSensor_t* tempinstance,const UINT16* table);
extern void API_TS_Config_Ttablesize(TemperatureSensor_t* tempinstance,UINT8 size);
extern void API_TS_Update(TemperatureSensor_t* tempinstance);
extern float API_TS_Get_Cels(TemperatureSensor_t* tempinstance);


#endif /* COMPONENT_TEMPSENSOR_H_ */
