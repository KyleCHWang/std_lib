/*
 * VoltageRegulator.c
 *
 *  Created on: Dec 24, 2019
 *      Author: KYLE.CH.WANG
 */

#include "TypeDef.h"
#include "MCAL_PWM.h"
#include "VoltageRegulator.h"

void API_VR_Config_PWMchanel(VoltageRegulator_t* tempinstance, enumPWM_CHL_t tempdata){

    if(tempdata<PWM_CHL_MAX){
        tempinstance->PWMchanel = tempdata;
    }
}

void API_VR_Config_coeA(VoltageRegulator_t* tempinstance, float coe){

	tempinstance->coeA = coe;
}

void API_VR_Config_coeB(VoltageRegulator_t* tempinstance, float coe){

	tempinstance->coeB = coe;
}


UINT16 API_VR_Get_out(VoltageRegulator_t* tempinstance){

	return tempinstance->out;

}

void API_VR_Set_out(VoltageRegulator_t* tempinstance, UINT16 out){
	
	UINT32 ticks;
	tempinstance->out = out;
	ticks = API_PWM_DutyCalc(tempinstance->PWMchanel,out);
	API_PWM_Set_Duty(tempinstance->PWMchanel, ticks);
}

void API_VR_Output(VoltageRegulator_t* tempinstance, float output){

	UINT32 out;
	UINT32 ticks; 
	tempinstance->output = output;
	out= (UINT32)((output * tempinstance->coeA) + tempinstance->coeB);
	tempinstance->out = out;
	if(out >= 65535){
		out = 65535;
	}
	tempinstance->out = out;
	ticks = API_PWM_DutyCalc(tempinstance->PWMchanel,out);
	API_PWM_Set_Duty(tempinstance->PWMchanel, ticks);
	
}

