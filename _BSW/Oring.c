/*
 * Oring.c
 *
 *  Created on: Aug 16, 2019
 *      Author: KYLE.CH.WANG
 */

#include "Oring.h"
#include "TypeDef.h"
#include "IQ_Math.h"
#include "MCAL_GPIO.h"
#include "Object.h"


void API_Oring_Config_uplimit(Oring_t* tempOring, float uplimit){
    tempOring->uplimit = uplimit;
}

void API_Oring_Config_upthreshold(Oring_t* tempOring, float upthreshold){
    tempOring->upthreshold = upthreshold;
}

void API_Oring_Config_lowthreshold(Oring_t* tempOring, float lowthreshold){
    tempOring->lowthreshold = lowthreshold;
}

void API_Oring_Config_dutymax(Oring_t* tempOring, UINT16 dutymax){
    tempOring->dutymax = dutymax;
}
void API_Oring_Set_duty(Oring_t* tempOring, UINT16 dutycycle){
    if(dutycycle > tempOring->dutymax){
        dutycycle =  tempOring->dutymax;
    }
    tempOring->duty = dutycycle;
}
void API_Oring_PrechargeControl(Oring_t* tempOring, UINT16 input, UINT16 target){
    if(tempOring->status!=Oring_Precharge){
        return ;
    }
    if((tempOring->cnt < tempOring->duty)&&(input<target)){
        SET_GPIO_ORING;
    }
    else{
        CLR_GPIO_ORING;
    }
    tempOring->cnt++;
    if(tempOring->cnt >= tempOring->dutymax){
        tempOring->cnt = 0;
    }
    if((input >= (target-200)) && (input <= (target+200))){
        tempOring->status = Oring_Precharge_OK;
    }
    else{
        tempOring->status = Oring_Precharge;
    }

}

void API_Oring_Control(Oring_t* tempOring, float input, float target){

    float target_f_uplimit = 0;
    float target_f_lowlimit = 0;
    float target_f_upthreshold= 0;

    if(tempOring->status == Oring_OFF){
        target_f_uplimit = target + tempOring->uplimit;
    }
    else if(tempOring->status == Oring_ON){
        target_f_uplimit = target + tempOring->uplimit*2;
    }
    target_f_lowlimit = target + tempOring->lowthreshold;
    target_f_upthreshold = target + tempOring->upthreshold;
	
    float value_uplimit = target_f_uplimit;
    float value_upthreshold = target_f_upthreshold;
    float value_lowthreshold = target_f_lowlimit;

    if(input >=value_upthreshold && input <= value_uplimit){
        SET_GPIO_ORING;
        tempOring->status = Oring_ON;
    }
    else if(input > value_uplimit){

        tempOring->status = Oring_OFF;

    }

    else if(input <= value_lowthreshold){
        CLR_GPIO_ORING;
        tempOring->status = Oring_OFF;
    }


}

void API_Oring_ON(Oring_t* tempOring){

    SET_GPIO_ORING;
    tempOring->status = Oring_ON;

}

void API_Oring_OFF(Oring_t* tempOring){

    CLR_GPIO_ORING;
    tempOring->status = Oring_OFF;
}

void API_Oring_Precharge(Oring_t* tempOring){

    CLR_GPIO_ORING;
    tempOring->status = Oring_Precharge;
}

UINT8 API_Oring_Get_status(Oring_t* tempOring){

    return tempOring->status;
}
void API_Oring_Reset(Oring_t* tempOring){
	tempOring->cnt = 0;
    tempOring->duty = 0;
    tempOring->status = Oring_OFF;
}


