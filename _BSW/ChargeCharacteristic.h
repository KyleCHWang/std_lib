/*
 * ChargeProfile.h
 *
 *  Created on: Jul 18, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef BSW_COMPONENT_CHARGECHARACTERISTIC_H_
#define BSW_COMPONENT_CHARGECHARACTERISTIC_H_
#include "TypeDef.h"

static const UINT32 HOUR = 3600;
static const UINT32 MINUTE = 60;
static const UINT32 SEC = 1;
static const float CV_tolerance = 0.96f;
static const float CC_tolerance = 1.04f;
static const UINT32 CVtoFC_Delay = 5;	//Delaytime from CV mode to Float Charge



typedef enum{
	BP_IUIaWET12h,
	BP_IUIaWET8h,
	BP_IUIU0AGMDiscover,
	BP_IU0UAGMGeneric,
	BP_IUU0AGMFULLRIVER,
	BP_IUIaGEL,
	BP_IU0UZenith,
	BP_LithiumRelion,
	BP_IUIaAGMTrojan,
	BP_TrojanTrillium,
	BP_AGMHoppecke,
	BP_MAX,
} enum_BatteryProfile;


typedef enum{
	PROC_STOP,
	PROC_RUN,
	PROC_ABORT,
	PROC_FINISH,
} enum_CC_Proccess;

typedef enum{
	PHASE_INIT,
	PHASE_1,
	PHASE_2,
	PHASE_3,
	PHASE_4,
	PHASE_5,	
	PHASE_STOP,
	PHASE_MAX,
} enum_CC_Phase;
	
//IUIaWET12h========================================================================================
typedef enum{
	IUIaWET12h_CL_0,
	IUIaWET12h_CL_1,	
	IUIaWET12h_CL_2,
	IUIaWET12h_CL_3,
	IUIaWET12h_CL_MAX,
} enum_IUIaWET12h_CurrentLevel;	
static const float IUIaWET12h_I1[IUIaWET12h_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IUIaWET12h_I3[IUIaWET12h_CL_MAX] =  {5.5f,7.0f,7.5f,10.0f};
static const float IUIaWET12h_U2 = 28.8f;
static const float IUIaWET12h_U3 = 31.8f;
static const UINT32 IUIaWET12h_T1_MAX  = 15*3600; //15h
static const UINT32 IUIaWET12h_T2_MAX = 10*3600; //15h
typedef struct{
    //Configuration
	enum_IUIaWET12h_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
    enum_CC_Proccess Proccess;
	UINT32 T3;
	UINT32 T[10];
} _st_IUIaWET12h;
//==================================================================================================
//IUIaWET8h========================================================================================
typedef enum{
	IUIaWET8h_CL_0,
	IUIaWET8h_CL_1,	
	IUIaWET8h_CL_2,
	IUIaWET8h_CL_3,
	IUIaWET8h_CL_MAX,
} enum_IUIaWET8h_CurrentLevel;	
static const float IUIaWET8h_I1[IUIaWET8h_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IUIaWET8h_I3[IUIaWET8h_CL_MAX] =  {3.8f,5.0f,6.3f,7.5f};
static const float IUIaWET8h_U2 = 28.8f;
static const float IUIaWET8h_U3 = 31.8f;
static const UINT32 IUIaWET8h_T1_MAX  = 15*3600; //15h
static const UINT32 IUIaWET8h_T2_MAX = 10*3600; //15h
typedef struct{
    //Configuration
	enum_IUIaWET8h_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
    enum_CC_Proccess Proccess;
	UINT32 T3;
	UINT32 T[10];
} _st_IUIaWET8h;
//==================================================================================================

//IUIU0AGMDiscover========================================================================================
typedef enum{
	IUIU0AGMDiscover_CL_0,
	IUIU0AGMDiscover_CL_1,	
	IUIU0AGMDiscover_CL_2,
	IUIU0AGMDiscover_CL_3,
	IUIU0AGMDiscover_CL_MAX,
} enum_IUIU0AGMDiscover_CurrentLevel;	
static const float IUIU0AGMDiscover_I1[IUIU0AGMDiscover_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IUIU0AGMDiscover_I3[IUIU0AGMDiscover_CL_MAX] =  {2.4f,3.2f,4.0f,4.8f};
static const float IUIU0AGMDiscover_U2 = 29.4f;
static const float IUIU0AGMDiscover_U3 = 31.8f;
static const float IUIU0AGMDiscover_U4 = 27.2f;

static const UINT32 IUIU0AGMDiscover_T1_MAX  = 16*3600; //16h
static const UINT32 IUIU0AGMDiscover_T2_MAX = 8*3600; //8h
static const UINT32 IUIU0AGMDiscover_T3_MAX = 4*3600; //4h
static const UINT32 IUIU0AGMDiscover_T3_MIN = 1*3600; //4h


typedef struct{
    //Configuration
	enum_IUIU0AGMDiscover_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T3;
	UINT32 T[10];
} _st_IUIU0AGMDiscover;
//==================================================================================================
//IU0UAGMGeneric========================================================================================
typedef enum{
	IU0UAGMGeneric_CL_0,
	IU0UAGMGeneric_CL_1,	
	IU0UAGMGeneric_CL_2,
	IU0UAGMGeneric_CL_3,
	IU0UAGMGeneric_CL_MAX,
} enum_IU0UAGMGeneric_CurrentLevel;	
static const float IU0UAGMGeneric_I1[IU0UAGMGeneric_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IU0UAGMGeneric_I2[IU0UAGMGeneric_CL_MAX] =  {2.3f,3.2f,3.6f,5.0f};
static const float IU0UAGMGeneric_U2 = 29.4f;
static const float IU0UAGMGeneric_U3 = 27.6f;

static const UINT32 IU0UAGMGeneric_T1_MAX  = 10*3600; //10h
static const UINT32 IU0UAGMGeneric_T2_MAX = 10*3600; //10h
static const UINT32 IU0UAGMGeneric_T3 = 4*3600; //10h


typedef struct{
    //Configuration
	enum_IU0UAGMGeneric_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T3;
	UINT32 T[10];
} _st_IU0UAGMGeneric;
//==================================================================================================

//IUU0AGMFULLRIVER========================================================================================
typedef enum{
	IUU0AGMFULLRIVER_CL_0,
	IUU0AGMFULLRIVER_CL_1,	
	IUU0AGMFULLRIVER_CL_2,
	IUU0AGMFULLRIVER_CL_3,
	IUU0AGMFULLRIVER_CL_MAX,
} enum_IUU0AGMFULLRIVER_CurrentLevel;	
static const float IUU0AGMFULLRIVER_I1[IUU0AGMFULLRIVER_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IUU0AGMFULLRIVER_I2[IUU0AGMFULLRIVER_CL_MAX] =  {2.4f,3.2f,4.0f,4.8f};
static const float IUU0AGMFULLRIVER_U2 = 29.4f;
static const float IUU0AGMFULLRIVER_U3 = 27.6f;

static const UINT32 IUU0AGMFULLRIVER_T1_MAX  = 10*3600; //10h
static const UINT32 IUU0AGMFULLRIVER_T2_MAX = 10*3600; //10h
static const UINT32 IUU0AGMFULLRIVER_T3 = 4*3600; //10h


typedef struct{
    //Configuration
	enum_IUU0AGMFULLRIVER_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T3;
	UINT32 T[10];
} _st_IUU0AGMFULLRIVER;
//==================================================================================================
//IUUaGEL========================================================================================
typedef enum{
	IUIaGEL_CL_0,
	IUIaGEL_CL_1,	
	IUIaGEL_CL_2,
	IUIaGEL_CL_3,
	IUIaGEL_CL_MAX,
} enum_IUIaGEL_CurrentLevel;	
static const float IUIaGEL_I1[IUIaGEL_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IUIaGEL_I3[IUIaGEL_CL_MAX] =  {1.2f,1.5f,2.0f,2.5f};
static const float IUIaGEL_U2 = 28.2f;
static const float IUIaGEL_U3 = 32.4f;
static const float IUIaGEL_U4 = 27.6f;

static const UINT32 IUIaGEL_T1_MAX  = 9*3600; //9h
static const UINT32 IUIaGEL_T1plusT2_MAX = 14*3600; //14h
static const UINT32 IUIaGEL_T3_MAX = 4*3600; //4h
static const UINT32 IUIaGEL_T3_MIN = 1*3600; //1h
typedef struct{
    //Configuration
	enum_IUIaGEL_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T3;
	UINT32 T[10];
} _st_IUIaGEL;
//==================================================================================================
//IU0UZenith========================================================================================
typedef enum{
	IU0UZenith_CL_0,
	IU0UZenith_CL_1,	
	IU0UZenith_CL_2,
	IU0UZenith_CL_3,
	IU0UZenith_CL_MAX,
} enum_IU0UZenith_CurrentLevel;	
static const float IU0UZenith_I1[IU0UZenith_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IU0UZenith_U2 = 29.2f;
static const float IU0UZenith_U3 = 27.0f;

static const UINT32 IU0UZenith_T1_MAX  = 10*3600; //10h
static const UINT32 IU0UZenith_T2_MAX = 6*3600; //6h
static const UINT32 IU0UZenith_T2_MIN = 2*3600; //2h
typedef struct{
    //Configuration
	enum_IU0UZenith_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T2;
	UINT32 T[10];
} _st_IU0UZenith;
//==================================================================================================

//LithiumRelion========================================================================================
typedef enum{
	LithiumRelion_CL_0,
	LithiumRelion_CL_1,	
	LithiumRelion_CL_2,
	LithiumRelion_CL_3,
	LithiumRelion_CL_MAX,
} enum_LithiumRelion_CurrentLevel;	
static const float LithiumRelion_I1[LithiumRelion_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float LithiumRelion_I2[LithiumRelion_CL_MAX] =  {1.0f,1.5f,1.5f,2.0f};
static const float LithiumRelion_U2 = 28.8f;

static const UINT32 LithiumRelion_T1_MAX  = 6*3600; //10h
static const UINT32 LithiumRelion_T2_MAX = 2*3600; //6h

typedef struct{
    //Configuration
	enum_LithiumRelion_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T[10];
} _st_LithiumRelion;
//==================================================================================================
//IUIaAGMTrojan========================================================================================
typedef enum{
	IUIaAGMTrojan_CL_0,
	IUIaAGMTrojan_CL_1,	
	IUIaAGMTrojan_CL_2,
	IUIaAGMTrojan_CL_3,
	IUIaAGMTrojan_CL_MAX,
} enum_IUIaAGMTrojan_CurrentLevel;	
static const float IUIaAGMTrojan_I1[IUIaAGMTrojan_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float IUIaAGMTrojan_I3[IUIaAGMTrojan_CL_MAX] =  {2.7f,3.4f,3.7f,5.3f};
static const float IUIaAGMTrojan_Slope = 0.1f; //0.2V/30min
static const float IUIaAGMTrojan_U2 = 28.7f;
static const float IUIaAGMTrojan_U3 = 32.4f;
static const UINT32 IUIaAGMTrojan_T1_MAX  = 10*3600; //10h
static const UINT32 IUIaAGMTrojan_T2_MAX = 6*3600; //6h
static const UINT32 IUIaAGMTrojan_T3_MAX = 3*3600; //3h
static const UINT32 IUIaAGMTrojan_T3_MIN = 1800; //0.5h
static const UINT32 IUIaAGMTrojan_T3_SlopeCheck = 1800; //30min
static const UINT32 IUIaAGMTrojan_T1T2T3_MAX = 16*3600; //3h

typedef struct{
    //Configuration
	enum_IUIaAGMTrojan_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
    enum_CC_Proccess Proccess;
	float Vout_T3;
	float Vout_T3_old;
	UINT32 T3;
	UINT32 T[10];
} _st_IUIaAGMTrojan;
//==================================================================================================
//TrojanTrillium========================================================================================
typedef enum{
	TrojanTrillium_CL_0,
	TrojanTrillium_CL_1,	
	TrojanTrillium_CL_2,
	TrojanTrillium_CL_3,
	TrojanTrillium_CL_MAX,
} enum_TrojanTrillium_CurrentLevel;	
static const float TrojanTrillium_I1[TrojanTrillium_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float TrojanTrillium_I2[TrojanTrillium_CL_MAX] =  {0.75f,1.0f,1.25f,1.5f};

static const float TrojanTrillium_U2 = 29.2f;

static const UINT32 TrojanTrillium_T1_MAX  = 4*3600; //4h
static const UINT32 TrojanTrillium_T2_MAX = 1*3600; //1h

typedef struct{
    //Configuration
	enum_TrojanTrillium_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T[10];
} _st_TrojanTrillium;
//==================================================================================================
//AGMHoppecke========================================================================================
typedef enum{
	AGMHoppecke_CL_0,
	AGMHoppecke_CL_1,	
	AGMHoppecke_CL_2,
	AGMHoppecke_CL_3,
	AGMHoppecke_CL_MAX,
} enum_AGMHoppecke_CurrentLevel;	
static const float AGMHoppecke_I1[AGMHoppecke_CL_MAX] =  {15.0f,20.0f,25.0f,30.0f};
static const float AGMHoppecke_Ids[AGMHoppecke_CL_MAX] =  {1.0f,1.32f,1.65f,2.0f};
static const float AGMHoppecke_I2[AGMHoppecke_CL_MAX] =  {1.6f,2.14f,2.68f,3.2f};
static const float AGMHoppecke_UT0 = 18.0f;
static const float AGMHoppecke_UT1 = 22.8f;
static const float AGMHoppecke_UT3 = 28.8f;
static const float AGMHoppecke_UT4 = 31.2f;
static const float AGMHoppecke_UT5 = 27.0f;
static const UINT32 AGMHoppecke_T1_MAX  = 3*3600; //3h
static const UINT32 AGMHoppecke_T2T3_MAX = 8*3600; //15h
static const UINT32 AGMHoppecke_T2T3T4_MAX = 16*3600; //16h

typedef struct{
    //Configuration
	enum_AGMHoppecke_CurrentLevel CurrnetLevel;				
    //Instance
    enum_CC_Phase PhaseOld;
    enum_CC_Phase Phase;
	enum_CC_Proccess Proccess;
	UINT32 T4;
	UINT32 T[10];
} _st_AGMHoppecke;
//==================================================================================================

extern void (*API_CC_Battery_Run[BP_MAX])(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2);
extern void (*API_CC_Battery_Init[BP_MAX])(BF_U32 bf, UINT8 para0, UINT64 para1, UINT64 para2);
extern enum_CC_Proccess (*API_CC_Battery_getProccess[BP_MAX])(void);
extern enum_CC_Phase (*API_CC_Battery_getPhase[BP_MAX])(void);

#endif /* BSW_COMPONENT_ChargeCharacteristic_H_ */

