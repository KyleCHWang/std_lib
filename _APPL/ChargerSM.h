#ifndef CHARGERSM_H
#define CHARGERSM_H

#include "TypeDef.h"

typedef enum
{
    CHARGER_IDLE,         /*!< Persistent state, following state can be INIT if all required
                          input conditions are satisfied or STOP if there is a fault
                          condition */	
    CHARGER_INIT,
    CHARGER_CHARGE_START,       //Voltage control for ORING
    CHARGER_CHARGE_RUN,        /*!< Persistent state with converter running. The following state
                         is STOP if there is a fault condition */

    CHARGER_STOP,           /*!< "Pass-through" state where PWM are disabled. The state machine
                          can be moved from any condition directly to this state by
                          SMPS_FaultCheck function. following state is FAULT */

    CHARGER_FAULT,          /*!< Persistent state after a fault. Following state is normally
                         WAIT as soon as conditions for moving state machine are detected */

    CHARGER_WAIT,         /*!< Persistent state where the application is intended to stay
                         after the fault conditions disappeared for a defined wait time.
                         Following state is normally IDLE */
    CHARGER_MAX,

} ChargerSM_State_t;

typedef struct{
    //configuration
    //instance
    ChargerSM_State_t state;
    ChargerSM_State_t stateold;
    ChargerSM_State_t statenext;
    UINT32 delaycnt[10];

} ChargerSM_t;

extern void UDF_CSM_Init(ChargerSM_t* tempCSM);
extern void API_CSM_Task(void);
extern void ChargerFlashWrite(void);
extern void ChargerFlashRead(void);
#endif
