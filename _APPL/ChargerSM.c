#include "TypeDef.h"
#include "MCAL_PWM.h"
#include "Object.h"
#include "ChargerSM.h"
#include "IQ_Math.h"

//User define Function===============================================
#include "user_flash.h"

//1ms
#define CSM_IDLE_PROTECT 2000
#define CSM_IDLE_READY 2200


#define CSM_INIT_AC_READY_DELAY 800
#define CSM_INIT_PFC_DELAY 1100
#define CSM_INIT_PFC_PROTECT 1700
#define CSM_INIT_HWReset_ON 1900
#define CSM_INIT_HWReset_OFF 1950
#define CSM_INIT_LLC_DELAY 2000
#define CSM_INIT_SR_DELAY 2400
#define CSM_INIT_END_DELAY1 2500
#define CSM_INIT_END_DELAY2 2600

#define CSM_RUN_DETACH_DELAY  5000
#define CSM_RUN_DERATE_PERIOD 15000
#define CSM_RUN_TEST_PERIOD 40000

#define CSM_STOP_LLC_DELAY 0
#define CSM_STOP_SR_DELAY 0
#define CSM_STOP_PFC_DELAY 200
#define CSM_STOP_ORING_DELAY 250
#define CSM_STOP_FAN_DELAY 150
#define CSM_STOP_INRUSH_RELAY_DELAY 500
#define CSM_STOP_END_DELAY 10000

#define CSM_FAULT_REF_DELAY 0
#define CSM_FAULT_EN_DELAY  5
#define CSM_FAULT_PTC_DELAY 500
#define CSM_FAULT_HWReset_ON 600
#define CSM_FAULT_HWReset_OFF 650

#define CSM_FAULT_END_DELAY 10000


extern UINT16 tempCV;
extern UINT16 tempCC;
extern UINT16 testLED;
extern UINT16 testRELAY;

static UINT8 bf_LEDBlink = 0;



void UDF_CSM_Init(ChargerSM_t* tempCSM){
  
    tempCSM->state = CHARGER_IDLE;
    tempCSM->stateold = CHARGER_STOP;
}
//====================================================================
void OutputArea(void);
void OutputPowerLimit(void);
void OutputDerating(void);
void AllProtectEnable(void);
void AllProtectDisable(void);
UINT8 FaultCheck(void);
void FaultBufferUpdate(void);
void LEDControl(void);
void LEDControl_SPE(void);
float OutputDerating_AC(void);


static float temp_voltage;
static float temp_current;
UINT8 LEDstate = 0,LEDstateold = 1;
extern UINT8 DIAG_testdata;

void API_CSM_Task(void){
	BF_U32 tempbf;
	tempbf.all = 0;
    switch(ChargerSM.state){
        case CHARGER_IDLE:
            if(ChargerSM.state != ChargerSM.stateold){
                ChargerSM.stateold = ChargerSM.state;
				ChargerSM.delaycnt[0] = 0;
				System.batteryfault = 0;
				if(!testLED){
	                SET_GPIO_LED2;
	                SET_GPIO_LED4;
	                SET_GPIO_LED6;
				}
            }
			if(ChargerSM.delaycnt[0] == CSM_IDLE_PROTECT){
				#if EMI_TEST
				System.ONOFF = System_ON;
				Control.vref_LV = 36.0f;
				Control.iref_LV_Charge = 30.0f;
				#else
				if(System.System==System_Charger){
					API_Protect_ONOFF(&SW_UVP_LV, ENABLE);
				}
				#endif
				if(System.System==System_Charger){
					API_Protect_ONOFF(&Vin_UVP, ENABLE);
					PMIC.Protect_EN.bits.AC_UVP_EN = 1;
					API_Protect_ONOFF(&BAT_REV, ENABLE);
				}
			}
			if(ChargerSM.delaycnt[0] >= CSM_IDLE_READY){
				if(System.ONOFF == System_ON){
					if(FaultCheck()==0){
						System.fault = 0;
		            	ChargerSM.state = CHARGER_INIT; 
					}					
            	}
			}
			if(ChargerSM.delaycnt[0]<=65535){
				ChargerSM.delaycnt[0]++;
			}	
            break;
    	case CHARGER_INIT:
			if(ChargerSM.state != ChargerSM.stateold){
                ChargerSM.stateold = ChargerSM.state;
				ChargerSM.delaycnt[0] = 0;		
				System.vref_LV = 0.0f;
				System.iref_LV_Charge = 0.0f;
				if(!testLED){
				}
			}
			if(ChargerSM.delaycnt[0]==CSM_INIT_AC_READY_DELAY){//DC bus voltage >= AC * 0.8
				//AC_inrush relay ON
				SET_GPIO_PTC_RELAY;
			}

			if(ChargerSM.delaycnt[0]==CSM_INIT_PFC_DELAY){
				SET_GPIO_PFC_EN;	
			}
			if(ChargerSM.delaycnt[0]==CSM_INIT_PFC_PROTECT){
				if(System.System==System_Charger){
					API_Protect_ONOFF(&PFC_OVP,ENABLE);	
					API_Protect_ONOFF(&PFC_UVP,ENABLE);
				}
			}
			if(ChargerSM.delaycnt[0]==CSM_INIT_HWReset_ON){
				SET_GPIO_HWRESET;				
			}
			if(ChargerSM.delaycnt[0]==CSM_INIT_HWReset_OFF){
				CLR_GPIO_HWRESET;
			}
			if(ChargerSM.delaycnt[0]==CSM_INIT_LLC_DELAY){
				SET_GPIO_LLC_EN;
			}
			if(ChargerSM.delaycnt[0]==CSM_INIT_SR_DELAY){
				SET_GPIO_VCCS2_EN;
			}
			if(ChargerSM.delaycnt[0]>=CSM_INIT_END_DELAY1){
				if(System.System==System_Charger){
					AllProtectEnable();
				}
			}
			if(ChargerSM.delaycnt[0]>=CSM_INIT_END_DELAY2){
				if(System.fault==0){
					if(!testRELAY){
						ChargerSM.state = CHARGER_CHARGE_START;
					}
					else{
						ChargerSM.state = CHARGER_CHARGE_RUN;
					}
				}
			}
			if(System.ONOFF==System_OFF){
				ChargerSM.state = CHARGER_IDLE;
			}
			if(System.fault==1){
				ChargerSM.state = CHARGER_FAULT;
			}
			if(ChargerSM.delaycnt[0]<=65535){
				ChargerSM.delaycnt[0]++;
			}	
			break;
        case CHARGER_CHARGE_START:
            if(ChargerSM.state != ChargerSM.stateold){
                ChargerSM.stateold = ChargerSM.state;
				
            }
			API_CP_Follow_float(&CP_V_Charge,&System.vref_LV,0.005f,1,VS_vLV_BAT.Volt + 0.0f);
			System.iref_LV_Charge = 3.0f;
            API_Oring_Control(&OR_Battery, VS_vLV_DCBUS.Volt ,VS_vLV_BAT.Volt);
            if(API_Oring_Get_status(&OR_Battery)==Oring_ON){
                ChargerSM.state = CHARGER_CHARGE_RUN; 
            }
            if(System.mode != Mode_Charge){
                ChargerSM.state = CHARGER_STOP;
            }
            if(System.ONOFF == System_OFF){
                ChargerSM.state = CHARGER_STOP;
            }
			if(System.fault==1){
				 ChargerSM.state = CHARGER_FAULT;			
			}
            break;
        case CHARGER_CHARGE_RUN:
            if(ChargerSM.state != ChargerSM.stateold){
                ChargerSM.stateold = ChargerSM.state;
				ChargerSM.delaycnt[0] = 0;
				ChargerSM.delaycnt[1] = 0;
				ChargerSM.delaycnt[9] = 0;
				System.chargetime = 0;
				System.derating = 1.0f;
				LEDstate = 0;
				LEDstateold = 100;
				#if EMI_TEST==0
				if(System.System==System_Charger){
					(*API_CC_Battery_Init[System.ChargeProfile])(tempbf, System.ChargeCurrentLevel,0, 0);
				}
				#endif
            }
			if(ChargerSM.delaycnt[1] >= 1000){
				ChargerSM.delaycnt[1] = 0;
				#if EMI_TEST==0
				if(System.System==System_Charger){
					(*API_CC_Battery_Run[System.ChargeProfile])(tempbf, System.ChargeCurrentLevel, 0, 0);
				}
				#endif
				if(!testLED){
					LEDControl_SPE();
				}
			}
			if(ChargerSM.delaycnt[9] >= 60000){
				ChargerSM.delaycnt[9] = 0;
				System.chargetime++;
			}
			if(System.System==System_Charger){
				if(API_CS_Get_Ampere(&CS_iLV_Charge) > 0.0f){
					temp_voltage = Control.vref_LV + (API_CS_Get_Ampere(&CS_iLV_Charge) * CableResistance);
				}
                                else{
                                     temp_voltage = Control.vref_LV;
                                }
			}
			else if(System.System==System_ATS){
				temp_voltage = Control.vref_LV;
			}
			OutputArea();
			OutputPowerLimit();
			if(ChargerSM.delaycnt[0] > CSM_RUN_DERATE_PERIOD){
				ChargerSM.delaycnt[0] = 0;
				OutputDerating();	
			}
			System.iref_LV_Charge_Derating = temp_current * System.derating*OutputDerating_AC();
			API_CP_Follow_float(&CP_V_Charge,&System.vref_LV,0.01f,1,temp_voltage);
			API_CP_Follow_float(&CP_I_Charge,&System.iref_LV_Charge,0.005f,1,System.iref_LV_Charge_Derating);
			#if EMI_TEST==0
			if(System.System==System_Charger){
				if((*API_CC_Battery_getProccess[System.ChargeProfile])()==PROC_FINISH){
					System.ONOFF = System_OFF;
					ChargerSM.state = CHARGER_STOP;
				}
				else if((*API_CC_Battery_getProccess[System.ChargeProfile])()==PROC_ABORT){
					System.ONOFF = System_OFF;
					System.batteryfault = 1;
					ChargerSM.state = CHARGER_FAULT;
				}
			}
			#endif
            if(System.mode != Mode_Charge){
                ChargerSM.state = CHARGER_STOP;
            }
            if(System.ONOFF == System_OFF){
                ChargerSM.state = CHARGER_STOP;
            }
			if(System.fault==1){
				 ChargerSM.state = CHARGER_FAULT;			
			}
			if(ChargerSM.delaycnt[0]<=65535){
				ChargerSM.delaycnt[0]++;
			}
            if(ChargerSM.delaycnt[1]<=65535){
				ChargerSM.delaycnt[1]++;
			}
			if(ChargerSM.delaycnt[9]<=65535){
				ChargerSM.delaycnt[9]++;
			}
            break;
        case CHARGER_STOP:
            if(ChargerSM.state != ChargerSM.stateold){
                ChargerSM.stateold = ChargerSM.state;
				AllProtectDisable();
                ChargerSM.delaycnt[0] = 0;
				System.vref_LV = 0;
				System.iref_LV_Charge = 0;
            }
			if(ChargerSM.delaycnt[0]==CSM_STOP_LLC_DELAY){
				
				CLR_GPIO_LLC_EN;
			}
			if(ChargerSM.delaycnt[0]==CSM_STOP_SR_DELAY){
				CLR_GPIO_VCCS2_EN;
			}
			if(ChargerSM.delaycnt[0]==CSM_STOP_PFC_DELAY){
				CLR_GPIO_PFC_EN;
			}
			if(ChargerSM.delaycnt[0]==CSM_STOP_ORING_DELAY){
				CLR_GPIO_ORING;
			}
			if(ChargerSM.delaycnt[0]==CSM_STOP_FAN_DELAY){
				//API_VR_Set_out(&VR_FanADJ,0);
			}
			if(ChargerSM.delaycnt[0]==CSM_STOP_INRUSH_RELAY_DELAY){
				CLR_GPIO_PTC_RELAY; //Inrush relay off
			}
            if(ChargerSM.delaycnt[0]==CSM_STOP_END_DELAY){
                ChargerSM.state = CHARGER_IDLE;
            }
			ChargerSM.delaycnt[0]++;
            break;

        case CHARGER_FAULT:
            if(ChargerSM.state != ChargerSM.stateold){
                ChargerSM.stateold = ChargerSM.state;
				ChargerSM.delaycnt[0] = 0;
				if(!testLED){
					
				}
            }

			if(ChargerSM.delaycnt[0] == CSM_FAULT_REF_DELAY){
				System.ONOFF = System_OFF;
				System.vref_LV = 0;
				System.iref_LV_Charge = 0;
			}
			if(ChargerSM.delaycnt[0] == CSM_FAULT_EN_DELAY){
				CLR_GPIO_VCCS2_EN;
				CLR_GPIO_LLC_EN;
				CLR_GPIO_PFC_EN;
			}
			if(ChargerSM.delaycnt[0] >= CSM_FAULT_PTC_DELAY){
				CLR_GPIO_ORING;
				CLR_GPIO_PTC_RELAY;
			}
			if(ChargerSM.delaycnt[0]==CSM_FAULT_HWReset_ON){
				SET_GPIO_HWRESET;				
			}
			if(ChargerSM.delaycnt[0]==CSM_FAULT_HWReset_OFF){
				CLR_GPIO_HWRESET;
			}
			if(ChargerSM.delaycnt[0] >= CSM_FAULT_END_DELAY){
	            if(FaultCheck()==0){ //Fault status is sent & no fault
	                ChargerSM.state = CHARGER_IDLE;
					AllProtectDisable();
					System.fault = 0;
	            }
			}
			if(ChargerSM.delaycnt[0]<=65535){
				ChargerSM.delaycnt[0]++;
			}
            break;
        case CHARGER_MAX:
            break;
            
    }
}



void AllProtectEnable(void){

	API_Protect_ONOFF(&Vin_UVP, ENABLE);
	API_Protect_ONOFF(&PFC_OVP, ENABLE);
	API_Protect_ONOFF(&PFC_UVP, ENABLE);
	API_Protect_ONOFF(&HW_Protect, ENABLE);
	API_Protect_ONOFF(&SW_OVP_LV, ENABLE);
	API_Protect_ONOFF(&SW_OCP_LV, ENABLE);
	API_Protect_ONOFF(&OTP, ENABLE);
}

void AllProtectDisable(void){

	API_Protect_ONOFF(&Vin_UVP, DISABLE);
	API_Protect_ONOFF(&PFC_OVP, DISABLE);
	API_Protect_ONOFF(&PFC_UVP, DISABLE);
	API_Protect_ONOFF(&HW_Protect, DISABLE);
	API_Protect_ONOFF(&SW_OVP_LV, DISABLE);
	API_Protect_ONOFF(&SW_OCP_LV, DISABLE);
	API_Protect_ONOFF(&OTP, DISABLE);

}

void GreenLED(ONOFF_t ONOFF){
	if (ONOFF){
		CLR_GPIO_LED2;
		CLR_GPIO_LED1;
	}
	else{
		SET_GPIO_LED2;
		SET_GPIO_LED1;
	}
}
void YellowLED(ONOFF_t ONOFF){
	if (ONOFF){
		CLR_GPIO_LED3;
		CLR_GPIO_LED4;
	}
	else{
		SET_GPIO_LED3;
		SET_GPIO_LED4;
	}
}
void RedLED(ONOFF_t ONOFF){
	if (ONOFF){
		CLR_GPIO_LED5;
		CLR_GPIO_LED6;
	}
	else{
		SET_GPIO_LED5;
		SET_GPIO_LED6;
	}
}



void LEDControl_SPE(void){
	enum_CC_Phase tempPhase = (enum_CC_Phase)0;
	if(bf_LEDBlink){bf_LEDBlink = 0;}
	else{bf_LEDBlink = 1;}
	tempPhase = API_CC_Battery_getPhase[System.ChargeProfile]();
	switch(System.ChargeProfile){
		case BP_IUIaWET12h:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;	
				default:
					break;
			}
			break;	
		case BP_IUIaWET8h:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;	
				default:
					break;
			}
			break;
		case BP_IUIU0AGMDiscover:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;		
				default:
					break;
			}
			break;
		case BP_IU0UAGMGeneric:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;		
				default:
					break;
			}
			break;
		case BP_IUU0AGMFULLRIVER:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;	
				default:
					break;
			}
			break;
		case BP_IUIaGEL:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;		
				default:
					break;
			}
			break;
		case BP_IU0UZenith:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;
				default:
					break;
			}
			break;
		case BP_LithiumRelion:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;
				default:
					break;
			}
			break;
		case BP_IUIaAGMTrojan:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;	
				default:
					break;
			}
			break;
		case BP_TrojanTrillium:
			switch(tempPhase){
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;
				default:
					break;
			}
			break;
		case BP_AGMHoppecke:
			switch(tempPhase){
				case PHASE_STOP:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_1:
					RedLED(_ON);
					YellowLED(_OFF);
					GreenLED(_OFF);
					break;
				case PHASE_2:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_3:
					RedLED(_OFF);
					YellowLED(_ON);
					GreenLED(_OFF);
					break;
				case PHASE_4:
					RedLED(_OFF);
					YellowLED(_OFF);
					if(bf_LEDBlink == 1){GreenLED(_ON);}
					else{GreenLED(_OFF);}
					break;
				case PHASE_5:
					RedLED(_OFF);
					YellowLED(_OFF);
					GreenLED(_ON);
					break;	
				default:
					break;
			}
			break;
	}
}
void LEDControl(void){
	switch(LEDstate){
		case 0:
			if(LEDstate != LEDstateold){
                LEDstateold = LEDstate;
				SET_GPIO_LED2;
				CLR_GPIO_LED4;
				
            }
			if(VS_vLV_BAT.Volt >= 35.5f){
				LEDstate = 100;
			}
			else if(VS_vLV_BAT.Volt >= 34.3f){
				LEDstate = 90;
			}
			else if(VS_vLV_BAT.Volt >= 29.2f){
				LEDstate = 60;
			}
			else if(VS_vLV_BAT.Volt >= 24.1f){
				LEDstate = 30;
			}
        case 30:
            if(LEDstate != LEDstateold){
                LEDstateold = LEDstate;
				SET_GPIO_LED2;
				CLR_GPIO_LED4;
            }
			if(VS_vLV_BAT.Volt >= 35.5f){
				LEDstate = 100;
			}
			else if(VS_vLV_BAT.Volt >= 34.3f){
				LEDstate = 90;
			}
			else if(VS_vLV_BAT.Volt >= 29.2f){
				LEDstate = 60;
			}
		break;
		case 60:	
			if(LEDstate != LEDstateold){
                LEDstateold = LEDstate;
				SET_GPIO_LED2;
				CLR_GPIO_LED4;
				
            }
			if(VS_vLV_BAT.Volt >= 35.5f){
				LEDstate = 100;
			}
			else if(VS_vLV_BAT.Volt >= 34.3f){
				LEDstate = 90;
			}
		break;
		case 90:	
			if(LEDstate != LEDstateold){
                LEDstateold = LEDstate;
				CLR_GPIO_LED2;
				SET_GPIO_LED4;
				
            }
			if(VS_vLV_BAT.Volt >= 35.5f){
				LEDstate = 100;
			}
		break;
		case 100:
			if(LEDstate != LEDstateold){
                LEDstateold = LEDstate;
				CLR_GPIO_LED2;
				SET_GPIO_LED4;
            }
		break;
	}


}

void OutputArea(void){
	if(temp_voltage >= 36.0f){
		temp_voltage = 36.0f;
	}
	else if(temp_voltage <= 19.0f){
		temp_voltage = 19.0f;
	}
	else{
		//temp_voltage = temp_voltage;
	}
	if(Control.iref_LV_Charge <= 1.0f){
		temp_current = 1.0f;
	}
	else if(Control.iref_LV_Charge >= 30.0f){
		temp_current = 30.0f;
	}
	else{
		temp_current = Control.iref_LV_Charge;
	}
}
void OutputPowerLimit(void){
	float CP_current=0;
	CP_current = (CHARGER_POWER/VS_vLV_DCBUS.Volt);
	if(temp_current > CP_current){
		temp_current = CP_current;
	}
}

void OutputDerating(void){
	if(TS_LLC.Cels > T_DERATE){ 
		System.derating += (POWER_DERATE_SLOPE*(T_DERATE - TS_LLC.Cels));
		if(System.derating < MAX_DERATE){
			System.derating = MAX_DERATE;
		}
	}
	else{
		System.derating += (POWER_RATE_SLOPE*(T_DERATE - TS_LLC.Cels));
		if(System.derating > 1.0f){
			System.derating = 1.0f;
		}
	}

}
float OutputDerating_AC(void){
	float tempfloat = 0.0f;
	if(API_VS_Get_Volt(&VS_vAC_L)<V_AC_DERATE){
		tempfloat = (V_AC_DERATE - API_VS_Get_Volt(&VS_vAC_L))*0.01f;
		tempfloat = 1.0f - tempfloat;
		if(tempfloat <= V_AC_MAX_DERATE){
			tempfloat = V_AC_MAX_DERATE;
		}
	}
	else{
		tempfloat = 1.0f;
	}
	return tempfloat;
}
UINT8 FaultCheck(void){

	return PFC_UVP.fault|PFC_OVP.fault|Vin_UVP.fault|HW_Protect.fault|SW_OVP_LV.fault\
		|SW_UVP_LV.fault|SW_OCP_LV.fault|OTP.fault|BAT_REV.fault|System.batteryfault;
 }
void ChargerFlashWrite(void){
	if(ChargerConfig[0]!=System.ChargeProfile||ChargerConfig[1]!=System.ChargeCurrentLevel){
		ChargerConfig[0] = System.ChargeProfile;
		ChargerConfig[1] = System.ChargeCurrentLevel;
		Flash_erase(PROFILE_ADDR,1);
		Flash_write(PROFILE_ADDR, ChargerConfig, 2);
	}
}

void ChargerFlashRead(void){
	Flash_read(PROFILE_ADDR, ChargerConfig, 2);
	if(ChargerConfig[0] >= BP_MAX){
		ChargerConfig[0] = 0;
	}
	System.ChargeProfile = (enum_BatteryProfile)ChargerConfig[0];
	if(ChargerConfig[1] > 3){
		ChargerConfig[1] = 0; 
	}
	System.ChargeCurrentLevel = ChargerConfig[1];
	Control.ChargeCurrent = 15+5*System.ChargeCurrentLevel;
}

