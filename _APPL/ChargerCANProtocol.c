#include "TypeDef.h"
#include "Object.h"
#include "MCAL_CAN.h"
#include "ChargerCANProtocol.h"
#include "IQ_Math.h"

//User define Function===============================================
extern float VS_vLV_BAT_avg;
extern float CS_iLV_Charge_avg;
#define CCANP_Tx_Info_period 10
#define CCANP_Tx_SN_period 100
#define CCANP_INIT_DELAY 1000
#define CCANP_RUN_Timeout 100
const UINT32 Rx_ID[CCANP_RxID_MAX] = {
	0x060,			
	0x072,
	0x074,
};
const UINT32 Tx_ID[CCANP_TxID_MAX] = {
	0x061,			//1//Charger Info
	0x6A0,			//2//Charger SN
	0x06A1FFF0,
	0x06A1FFF1,
	0x06A1FFF2,
	0x06A1FFF3,
	0x06A1FFF4,
	0x06A1FFF5,
	0x06A1FFF6,
	0x06A1FFF7,
	0x06A1FFF8,
	0x06A1FFF9,
	0x06A1FFFA,
	0x06A1FFFB,
	0x06A1FFFC,
	0x06A1FFFD,
	0x06A1FFFE,
	0x06A1FFFF,
	
};

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
void UDF_CCANP_Init(CCANP_t * tempCCANP){
	tempCCANP->stateold = CCANP_MAX;
	tempCCANP->state = CCANP_IDLE;
	tempCCANP->statenext = CCANP_IDLE;
	tempCCANP->channel = CAN_CHL_BAT;
	tempCCANP->fault = 0;
	API_CAN_Config_RxBuffer(CAN_CHL_BAT,tempCCANP->Rxframe.RxData);
	API_CAN_Config_TxBuffer(CAN_CHL_BAT,tempCCANP->Txframe[CCANP_TxID_Info].TxData); 
	
	
}

//====================================================================
void API_CCANP_RxDecoder(CCANP_t * tempCCANP){
	BF_U8 tempbyte;
	INT16 tempS16;
	UINT16 tempU16;
	float tempfloat;
	float tempfloat1;

	tempCCANP->Rxframe.RxHeader.id			=  API_CAN_Get_RxID(tempCCANP->channel); 
	tempCCANP->Rxframe.RxHeader.idtype		=  API_CAN_Get_RxIDtype(tempCCANP->channel); 
	tempCCANP->Rxframe.RxHeader.frame			=  API_CAN_Get_RxFrameType(tempCCANP->channel);
	tempCCANP->Rxframe.RxHeader.datalength	=  API_CAN_Get_RxDataLength(tempCCANP->channel);

	if((tempCCANP->Rxframe.RxHeader.id == Rx_ID[CCANP_RxID_Ctrl]) && (tempCCANP->Rxframe.RxHeader.idtype == CAN_ID_STD)\
		&&(tempCCANP->Rxframe.RxHeader.frame == CAN_FRAME_DATA)&&(tempCCANP->Rxframe.RxHeader.datalength == 8))                                      
    {
		tempbyte.all = tempCCANP->Rxframe.RxData[0];
		System.ONOFF  = (System_ONOFF_t) tempbyte.bit.b7;
		//System.mode = (ChargerSystemMode_t) tempbyte.bit.b6;	
		//Control.controller = tempbyte.bit.b5;
		//System.controller = tempbyte.bit.b5;
		if(tempbyte.bit.b4){//Error Clear
			tempCCANP->faultbuffer[0] = 0;
			tempCCANP->faultbuffer[1] = 0;
		}
		if(tempbyte.bit.b0){//FW Info request

		}
		//Current cmd
		tempS16 = ((tempCCANP->Rxframe.RxData[1]<<8) + tempCCANP->Rxframe.RxData[2]);
		tempfloat = tempS16 * 0.01f;
		Control.iref_LV_Charge = tempfloat;
		//Voltage cmd
		tempU16 = ((tempCCANP->Rxframe.RxData[3]<<8) + tempCCANP->Rxframe.RxData[4]);
		tempfloat = tempU16 * 0.001f;
		Control.vref_LV = tempfloat;

		tempCCANP->timeoutcnt = 0;
		tempCCANP->fault = 0;
		
    }
	if((tempCCANP->Rxframe.RxHeader.id == Rx_ID[CCANP_RxID_Reserved0]) && (tempCCANP->Rxframe.RxHeader.idtype == CAN_ID_STD)\
		&&(tempCCANP->Rxframe.RxHeader.frame == CAN_FRAME_DATA)&&(tempCCANP->Rxframe.RxHeader.datalength == 8))                                      
    {
    	tempbyte.all = tempCCANP->Rxframe.RxData[0];
		System.System = (System_MODE_t)tempbyte.bit.b0;
		if(tempbyte.bit.b0==1){
				
		}
    }
	if((tempCCANP->Rxframe.RxHeader.id == Rx_ID[CCANP_RxID_Reserved1]) && (tempCCANP->Rxframe.RxHeader.idtype == CAN_ID_STD)\
		&&(tempCCANP->Rxframe.RxHeader.frame == CAN_FRAME_DATA)&&(tempCCANP->Rxframe.RxHeader.datalength == 8))                                      
    {
    	//tempCCANP->Rxframe.RxData[0];//Calibration item index
    	//tempCCANP->Rxframe.RxData[1];//Set point index
    	tempU16 = ((tempCCANP->Rxframe.RxData[2]<<8) + tempCCANP->Rxframe.RxData[3]);
		tempfloat = tempU16 * 0.001f;
		if((Calibration_item_t)tempCCANP->Rxframe.RxData[0]==Calibration_item_0){
			tempfloat1 = VS_vLV_BAT_avg;
		}
		else if((Calibration_item_t)tempCCANP->Rxframe.RxData[0]==Calibration_item_1){
			tempfloat1 = API_VR_Get_out(&VR_VADJ);
		}
		else if((Calibration_item_t)tempCCANP->Rxframe.RxData[0]==Calibration_item_2){
			tempfloat1 = CS_iLV_Charge_avg;
		}
		else if((Calibration_item_t)tempCCANP->Rxframe.RxData[0]==Calibration_item_3){
			tempfloat1 = API_VR_Get_out(&VR_IADJ);
		}
		//API_Calibrate_SetPoint(&CalibrationDelta, (Calibration_item_t)tempCCANP->Rxframe.RxData[0], tempCCANP->Rxframe.RxData[1], tempfloat, tempfloat1);

    }
	
}
void API_CCANP_Task(CCANP_t * tempCCANP){
	BF_U8 tempbyte;
	INT8 tempS8;
	UINT16 tempdata;
	float tempfloat;
    switch(tempCCANP->state){
        case CCANP_IDLE:
            if(tempCCANP->state != tempCCANP->stateold){
                tempCCANP->stateold = tempCCANP->state;
            }
			tempCCANP->state = CCANP_INIT;
			
            break;
		case CCANP_INIT:
			if(tempCCANP->state != tempCCANP->stateold){
                tempCCANP->stateold = tempCCANP->state;
				tempCCANP->delaycnt[0] = 0;			
            }
			if(tempCCANP->delaycnt[0] >= CCANP_INIT_DELAY){
				tempCCANP->state = CCANP_RUN;	
			}
			if(tempCCANP->delaycnt[0]<65535){
				tempCCANP->delaycnt[0]++;
			}		
			break;
		case CCANP_RUN:
			if(tempCCANP->state != tempCCANP->stateold){
                tempCCANP->stateold = tempCCANP->state;
				tempCCANP->delaycnt[0] = 0;
				tempCCANP->delaycnt[1] = 0;
				tempCCANP->timeoutcnt = 0;
            }
			if(tempCCANP->timeoutcnt>=CCANP_RUN_Timeout){
				tempCCANP->fault = 1;
				//System.fault = 1;
				//tempCCANP->faultbuffer[1] |= tempCCANP->fault << 1;
			}
			if(tempCCANP->delaycnt[0] >= CCANP_Tx_Info_period){// CAN tx send CCANP_TxID_Info
				tempCCANP->delaycnt[0] = 0;
				tempCCANP->Txframe[CCANP_TxID_Info].TxHeader.idtype = CAN_ID_STD;
				tempCCANP->Txframe[CCANP_TxID_Info].TxHeader.id = Tx_ID[CCANP_TxID_Info];
				tempCCANP->Txframe[CCANP_TxID_Info].TxHeader.datalength = 8;
				tempbyte.bit.b7 = System.ONOFF;
				tempbyte.bit.b6 = 0;
				tempbyte.bit.b5 = 0;
				tempbyte.bit.b4 = System.fault;
				tempbyte.bit.b3 = 0;
				tempbyte.bit.b2 = System.derating == 1.0 ? 0:1; // De-rating
				tempbyte.bit.b1 = 0;
				tempbyte.bit.b0 = 0;
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[0] = tempbyte.all;
				tempfloat = (API_CS_Get_Ampere(&CS_iLV_Charge));
				if(tempfloat<0){
					tempfloat = 0;
				}
				tempdata = (UINT16)(100 * tempfloat);				
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[1] = tempdata >> 8;
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[2] = tempdata & 0xFF;
				tempfloat = (API_VS_Get_Volt(&VS_vLV_BAT));
				if(tempfloat<0){
					tempfloat = 0;
				}
				tempdata = (UINT16)(1000 * tempfloat);
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[3] = tempdata >> 8;
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[4] = tempdata & 0xFF;
				tempfloat = API_TS_Get_Cels(&TS_ORING);
                                tempS8 = (INT8)tempfloat;                
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[5] = tempS8;
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[6] = tempCCANP->faultbuffer[0]; //Fault1
				tempCCANP->Txframe[CCANP_TxID_Info].TxData[7] = tempCCANP->faultbuffer[1]; //Fault2
				tempCCANP->Txframe[CCANP_TxID_Info].transmit = 1;
				
			}
			if(tempCCANP->delaycnt[1] >= CCANP_Tx_SN_period){// CAN tx send CCANP_TxID_SN
				tempCCANP->delaycnt[1] = 0;
				tempCCANP->Txframe[CCANP_TxID_SN].TxHeader.idtype = CAN_ID_STD;
				tempCCANP->Txframe[CCANP_TxID_SN].TxHeader.id = Tx_ID[CCANP_TxID_SN];
				tempCCANP->Txframe[CCANP_TxID_SN].TxHeader.datalength = 8;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[0] = 0x01;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[1] = 0x23;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[2] = 0x45;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[3] = 0x67;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[4] = 0x89;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[5] = 0xAB;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[6] = 0xCD;
				tempCCANP->Txframe[CCANP_TxID_SN].TxData[7] = System.System;
				tempCCANP->Txframe[CCANP_TxID_SN].transmit = 1;

				tempCCANP->Txframe[CCANP_TxID_Debug0].TxHeader.idtype = CAN_ID_EXT;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxHeader.id = Tx_ID[CCANP_TxID_Debug0];
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxHeader.datalength = 8;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[0] = VR_VADJ.out>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[1] = VR_VADJ.out&0xFF;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[2] = VR_IADJ.out>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[3] = VR_IADJ.out&0xFF;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[4] = 0x89;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[5] = 0xAB;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[6] = 0xCD;
				tempCCANP->Txframe[CCANP_TxID_Debug0].TxData[7] = 0xEF;
				tempCCANP->Txframe[CCANP_TxID_Debug0].transmit = 1;
				
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxHeader.idtype = CAN_ID_EXT;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxHeader.id = Tx_ID[CCANP_TxID_Debug1];
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxHeader.datalength = 8;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[0] = VS_vLV_DCBUS.valueQ12_filtered>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[1] = VS_vLV_DCBUS.valueQ12_filtered&0xFF;
				tempfloat = (API_VS_Get_Volt(&VS_vLV_DCBUS));
				if(tempfloat<0){
					tempfloat = 0;
				}
				tempdata = (UINT16)(1000 * tempfloat);
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[2] = tempdata>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[3] = tempdata&0xFF;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[4] = VS_vLV_BAT.valueQ12_filtered>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[5] = VS_vLV_BAT.valueQ12_filtered&0xFF;
				tempfloat = (API_VS_Get_Volt(&VS_vLV_BAT));
				if(tempfloat<0){
					tempfloat = 0;
				}
				tempdata = (UINT16)(1000 * tempfloat);
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[6] = tempdata>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug1].TxData[7] = tempdata&0xFF;
				tempCCANP->Txframe[CCANP_TxID_Debug1].transmit = 1;
				
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxHeader.idtype = CAN_ID_EXT;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxHeader.id = Tx_ID[CCANP_TxID_Debug2];
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxHeader.datalength = 8;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[0] = CS_iLV_Charge.valueQ12_filtered>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[1] = CS_iLV_Charge.valueQ12_filtered&0xFF;
				tempfloat = (API_VS_Get_Volt(&VS_vAC_L));
				if(tempfloat<0){
					tempfloat = 0;
				}
				tempdata = (UINT16)(100 * tempfloat);	
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[2] = tempdata>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[3] = tempdata&0xFF;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[4] = PMIC.ADC_AC_POWER_DET_AVG>>8;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[5] = PMIC.ADC_AC_POWER_DET_AVG&0xFF;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[6] = PMIC.Fault.bits.AC_UVP;
				tempCCANP->Txframe[CCANP_TxID_Debug2].TxData[7] = ChargerSM.state;
				tempCCANP->Txframe[CCANP_TxID_Debug2].transmit = 1;

				tempCCANP->Txframe[CCANP_TxID_Debug3].TxHeader.idtype = CAN_ID_EXT;
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxHeader.id = Tx_ID[CCANP_TxID_Debug3];
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxHeader.datalength = 8;
				tempfloat = API_TS_Get_Cels(&TS_ORING);
                tempS8 = (INT8)tempfloat;                
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxData[0] = tempS8;
				tempfloat = API_TS_Get_Cels(&TS_LLC);
                tempS8 = (INT8)tempfloat;
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxData[1] = tempS8;
				tempfloat = API_TS_Get_Cels(&TS_EXT);
                tempS8 = (INT8)tempfloat;
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxData[2] = tempS8;
				tempfloat = API_TS_Get_Cels(&TS_PRI_LLC);
                tempS8 = (INT8)tempfloat; 
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxData[3] = tempS8;
				tempfloat = API_TS_Get_Cels(&TS_PRI_PFC);
                tempS8 = (INT8)tempfloat; 
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxData[4] = tempS8;
				tempfloat = API_TS_Get_Cels(&TS_PRI_BRIDGE);
                tempS8 = (INT8)tempfloat;
				tempCCANP->Txframe[CCANP_TxID_Debug3].TxData[5] = tempS8;
				tempCCANP->Txframe[CCANP_TxID_Debug3].transmit = 1;
				
			}
			if(tempCCANP->delaycnt[0]<65535){
				tempCCANP->delaycnt[0]++;
			}
			if(tempCCANP->delaycnt[1]<65535){
				tempCCANP->delaycnt[1]++;
			}
			if(tempCCANP->timeoutcnt<65535){
				tempCCANP->timeoutcnt++;
			}
			break;
    }
}

void API_CCANP_Tx(CCANP_t * tempCCANP,CCANP_TxID_t frameindex, CAN_IDType_t idtype, UINT8 DLC, UINT8 data0,UINT8 data1,UINT8 data2,UINT8 data3,UINT8 data4,UINT8 data5,UINT8 data6,UINT8 data7){
	tempCCANP->Txframe[frameindex].TxHeader.idtype = idtype;
    tempCCANP->Txframe[frameindex].TxHeader.id = Tx_ID[frameindex];
    tempCCANP->Txframe[frameindex].TxHeader.datalength = DLC;
	tempCCANP->Txframe[frameindex].TxData[0] = data0;
	tempCCANP->Txframe[frameindex].TxData[1] = data1;
	tempCCANP->Txframe[frameindex].TxData[2] = data2;
	tempCCANP->Txframe[frameindex].TxData[3] = data3;
	tempCCANP->Txframe[frameindex].TxData[4] = data4;
	tempCCANP->Txframe[frameindex].TxData[5] = data5;
	tempCCANP->Txframe[frameindex].TxData[6] = data6;
	tempCCANP->Txframe[frameindex].TxData[7] = data7;
    tempCCANP->Txframe[frameindex].transmit = 1;
}

void API_CCANP_Transmit(CCANP_t * tempCCANP){
	UINT8 cnt = 0;
	for(cnt=0; cnt<CCANP_TxID_MAX;cnt++){
		if(tempCCANP->Txframe[cnt].transmit==1){
			if(API_CAN_GetTxFIFOFreeLevel(tempCCANP->channel) > 0){
				tempCCANP->Txframe[cnt].transmit = 0;
				API_CAN_Config_TxBuffer(tempCCANP->channel,tempCCANP->Txframe[cnt].TxData);
				API_CAN_SendData(tempCCANP->channel,tempCCANP->Txframe[cnt].TxHeader.idtype, tempCCANP->Txframe[cnt].TxHeader.id , tempCCANP->Txframe[cnt].TxHeader.datalength);
			}
		}
	}
}

