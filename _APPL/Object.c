/*
 * Object.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */
#include "Object.h"

UINT64 ChargerConfig[10] = {0};
ChargerSystem_t System=
{
	System_Charger,
    System_OFF,
    Mode_Charge,
    Mode_CV,
    0,
    (enum_BatteryProfile)0,
    0,
    0,
    0,
    0,
    0,
    1.0f,
    0,
};
ChargerControl_t Control = {
	
	Charger_Normal,
    Mode_Charge,
    Mode_CC,
    0.0f,
    0.0f,
};

VoltageSensor_t VS_vHV_PFC ={
 (enumADC_CHL_t)0,	//ADCchanel; // I/O expander
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
 0, //Volt;
};

// I/O expander 
VoltageSensor_t VS_vAC_L ={
 (enumADC_CHL_t)0,	//ADCchanel; // I/O expander
 0.070757434f,
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
 0, //Volt;
};


VoltageSensor_t VS_vLV_BAT =
{
 ADC_CHL_BAT_DET,//ADCchanel;
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
 0, //Volt;
};
 
 VoltageSensor_t VS_vLV_DCBUS =
{
 ADC_CHL_VOUT_DET,//ADCchanel;
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 0.0f,
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
 0, //Volt;
};

CurrentSensor_t CS_iLV_Charge=
{
 ADC_CHL_IOUT_DET,    //ADCchanel;
 0, //coeA;
 0, //coeB;
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
 0, //Amp;
};

TemperatureSensor_t TS_LLC = {
	
   ADC_CHL_NTC_LLC,	//ADCchanel;
   0,				//Tmax;
   0,				//float Tmin;
   0,				//T0index;
   0,				//Ttablesize;
   0,				//Ttable;
   //Instance
   0,				//valueQ12_raw;
   0,				//valueQ12_filtered;
   0,				//integer;
   0,				//point;
   0,				//Cels;
   0,				//Fahr;
   0,				//fault;
};

TemperatureSensor_t TS_ORING = {
	
   ADC_CHL_NTC_RELAY,	//ADCchanel;
   0,				//Tmax;
   0,				//float Tmin;
   0,				//T0index;
   0,				//Ttablesize;
   0,				//Ttable;
   //Instance
   0,				//valueQ12_raw;
   0,				//valueQ12_filtered;
   0,				//integer;
   0,				//point;
   0,				//Cels;
   0,				//Fahr;
   0,				//fault;
};

TemperatureSensor_t TS_EXT = {
	
   ADC_CHL_NTC_EXT,	//ADCchanel;
   0,				//Tmax;
   0,				//float Tmin;
   0,				//T0index;
   0,				//Ttablesize;
   0,				//Ttable;
   //Instance
   0,				//valueQ12_raw;
   0,				//valueQ12_filtered;
   0,				//integer;
   0,				//point;
   0,				//Cels;
   0,				//Fahr;
   0,				//fault;
};
   

TemperatureSensor_t TS_PRI_LLC = {
	
   (enumADC_CHL_t)0,				//ADCchanel; // I/O expander
   0,				//Tmax;
   0,				//float Tmin;
   0,				//T0index;
   0,				//Ttablesize;
   0,				//Ttable;
   //Instance
   0,				//valueQ12_raw;
   0,				//valueQ12_filtered;
   0,				//integer;
   0,				//point;
   0,				//Cels;
   0,				//Fahr;
   0,				//fault;
};

TemperatureSensor_t TS_PRI_PFC = {
	
   (enumADC_CHL_t)0,				//ADCchanel; // I/O expander
   0,				//Tmax;
   0,				//float Tmin;
   0,				//T0index;
   0,				//Ttablesize;
   0,				//Ttable;
   //Instance
   0,				//valueQ12_raw;
   0,				//valueQ12_filtered;
   0,				//integer;
   0,				//point;
   0,				//Cels;
   0,				//Fahr;
   0,				//fault;
};
    
TemperatureSensor_t TS_PRI_BRIDGE = {
	
   (enumADC_CHL_t)0,				//ADCchanel; // I/O expander  
   0,				//Tmax;
   0,				//float Tmin;
   0,				//T0index;
   0,				//Ttablesize;
   0,				//Ttable;
   //Instance
   0,				//valueQ12_raw;
   0,				//valueQ12_filtered;
   0,				//integer;
   0,				//point;
   0,				//Cels;
   0,				//Fahr;
   0,				//fault;
};



Diagnostic_t DIAG_converter =
{
  //Configuration
  UART_CHL_BLEnDIAG, //UARTchannel
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//void (*cmdarray[DIAG_CMD_MAX])(UINT8 tempU8);
  //Instance
  0,
  0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};
  
VoltageRegulator_t VR_IADJ = {

 (enumPWM_CHL_t)0,
 0,
 
};
 
VoltageRegulator_t VR_VADJ = {

 (enumPWM_CHL_t)0,
 0,
 
};
 
CCANP_t CCANP_Battery;


ChargerSM_t ChargerSM =
{
    //configuration
    //instance
    CHARGER_IDLE,
    CHARGER_MAX,
    CHARGER_IDLE,
    {0,0,0,0,0,0,0,0,0,0}
};

ChargeProfile_t CP_V_Charge =
{
    //Configuration
    (ChargeNotes_t*)&Charge_Notes, //noteAddr;
    Note_Charge_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};
	
ChargeProfile_t CP_I_Charge =
{
    //Configuration
    (ChargeNotes_t*)&Charge_Notes, //noteAddr;
    Note_Charge_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};

Oring_t OR_Battery =
{

    //Configuration
    -1.0f,     //lowthreshold;
    0.3f,     //upthreshold;
    0.7f,     //uplimit;
    //Instance
    0,      //status;

} ;

SysManage_t SYS = {
	0,
	{0,0,0,0,0,0,0,0,0,0}
};

PMIC_t PMIC = {
	0,
	0,
	0,
	0,
	0,
	0,
};
	
EasyUART_t PRI_COMM = {
	//Configuration
	UART_CHL_PRI,
   //Instance
	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
	{0,0,0},
};

BLEUART_t BLE_COMM = {
	//Configuration
	UART_CHL_BLEnDIAG,
   //Instance
	{0,0,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,},
	{0,0,0},
};
Protection_t BAT_REV;
Protection_t Vin_UVP;
Protection_t PFC_OVP;
Protection_t PFC_UVP;
Protection_t HW_Protect;
Protection_t SW_OVP_LV;
Protection_t SW_UVP_LV;
Protection_t SW_OCP_LV;
Protection_t OTP;
CalibrationData_t CalibrationDelta;

const UINT16 LLC_NTC_table[166] = {		
      111	,
      117	,
      123	,
      130	,
      136	,
      143	,
      151	,
      159	,
      167	,
      175	,
      184	,
      193	,
      203	,
      213	,
      223	,
      234	,
      245	,
      257	,
      269	,
      281	,
      294	,
      308	,
      322	,
      336	,
      351	,
      367	,
      383	,
      400	,
      417	,
      434	,
      453	,
      471	,
      491	,
      511	,
      531	,
      552	,
      574	,
      596	,
      619	,
      642	,
      666	,
      691	,
      716	,
      742	,
      768	,
      795	,
      822	,
      850	,
      878	,
      907	,
      937	,
      967	,
      997	,
      1028	,
      1060	,
      1091	,
      1124	,
      1156	,
      1189	,
      1223	,
      1256	,
      1290	,
      1325	,
      1359	,
      1394	,
      1429	,
      1464	,
      1500	,
      1535	,
      1571	,
      1607	,
      1643	,
      1679	,
      1715	,
      1751	,
      1787	,
      1823	,
      1859	,
      1894	,
      1930	,
      1966	,
      2001	,
      2036	,
      2072	,
      2106	,
      2141	,
      2176	,
      2210	,
      2244	,
      2277	,
      2310	,
      2343	,
      2376	,
      2408	,
      2440	,
      2472	,
      2503	,
      2534	,
      2564	,
      2594	,
      2623	,
      2653	,
      2681	,
      2710	,
      2737	,
      2765	,
      2792	,
      2818	,
      2845	,
      2870	,
      2895	,
      2920	,
      2945	,
      2969	,
      2992	,
      3015	,
      3038	,
      3060	,
      3082	,
      3103	,
      3124	,
      3144	,
      3165	,
      3184	,
      3204	,
      3222	,
      3241	,
      3259	,
      3277	,
      3294	,
      3311	,
      3328	,
      3344	,
      3360	,
      3376	,
      3391	,
      3406	,
      3420	,
      3435	,
      3449	,
      3462	,
      3476	,
      3489	,
      3501	,
      3514	,
      3526	,
      3538	,
      3550	,
      3561	,
      3572	,
      3583	,
      3594	,
      3604	,
      3614	,
      3624	,
      3634	,
      3643	,
      3652	,
      3661	,
      3670	,
      3679	,
      3687	,
      3695	,
      3703	,
      3711	,
      3719	,

};

const UINT16 LLC_NTC_table_4120[166] = {		
	86	,
	90	,
	95	,
	100 ,
	106 ,
	111 ,
	117 ,
	123 ,
	129 ,
	136 ,
	143 ,
	150 ,
	158 ,
	165 ,
	174 ,
	182 ,
	191 ,
	200 ,
	210 ,
	220 ,
	230 ,
	241 ,
	252 ,
	264 ,
	276 ,
	288 ,
	301 ,
	314 ,
	328 ,
	342 ,
	357 ,
	372 ,
	388 ,
	404 ,
	421 ,
	438 ,
	456 ,
	474 ,
	493 ,
	512 ,
	532 ,
	553 ,
	574 ,
	595 ,
	617 ,
	640 ,
	663 ,
	686 ,
	711 ,
	735 ,
	760 ,
	786 ,
	812 ,
	839 ,
	866 ,
	894 ,
	922 ,
	951 ,
	980 ,
	1009	,
	1039	,
	1070	,
	1100	,
	1132	,
	1163	,
	1195	,
	1227	,
	1259	,
	1292	,
	1325	,
	1358	,
	1392	,
	1426	,
	1459	,
	1493	,
	1528	,
	1562	,
	1596	,
	1631	,
	1665	,
	1700	,
	1734	,
	1769	,
	1803	,
	1838	,
	1872	,
	1907	,
	1941	,
	1975	,
	2009	,
	2042	,
	2076	,
	2109	,
	2143	,
	2175	,
	2208	,
	2241	,
	2273	,
	2305	,
	2336	,
	2367	,
	2398	,
	2429	,
	2459	,
	2489	,
	2519	,
	2548	,
	2577	,
	2605	,
	2633	,
	2661	,
	2688	,
	2715	,
	2742	,
	2768	,
	2793	,
	2819	,
	2844	,
	2868	,
	2892	,
	2916	,
	2939	,
	2962	,
	2985	,
	3007	,
	3028	,
	3050	,
	3070	,
	3091	,
	3111	,
	3131	,
	3150	,
	3169	,
	3188	,
	3206	,
	3224	,
	3242	,
	3259	,
	3276	,
	3292	,
	3308	,
	3324	,
	3340	,
	3355	,
	3370	,
	3384	,
	3399	,
	3413	,
	3426	,
	3440	,
	3453	,
	3466	,
	3478	,
	3491	,
	3503	,
	3514	,
	3526	,
	3537	,
	3548	,
	3559	,
	3570	,
	3580	,
	3590	,
	3600	,
	3610	,
	3619	,
};


/*
HKComm_t HKC_Converter =
{
 UART_CHL_DIAG, //UARTchannel
 0,  //checkdelay_ticks
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   //rxdatabuffer[HKC_RXBYTE_MAX]
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   //txframe[48];
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,   //rxdata
};
GateCtrl_t GC_HV1 =
{
  //Configuration
  PWM_CHL_HV1,   //PWMchannel
  Fsw_max,  //frequency_max
  Fsw_min,  //frequency_min
  600000,   //deadtime_lead_ps_max
  100000,   //deadtime_lead_ps_min
  600000,   //deadtime_follow_ps_max
  0,    //deadtime_follow_ps_min
  0,    //alpha_max
  0,    //gamma_max
  0,    //theta_max
  0,    //theta_min
  //Instance
  0,    //period
  0,    //alpha
  0,    //gamma
  0,    //theta

};
GateCtrl_t GC_HV2 =
{
  //Configuration
  PWM_CHL_HV2,   //PWMchannel
  Fsw_max,  //frequency_max
  Fsw_min,  //frequency_min
  600000,   //deadtime_lead_ps_max
  100000,   //deadtime_lead_ps_min
  600000,   //deadtime_follow_ps_max
  0,    //deadtime_follow_ps_min
  0,    //alpha_max
  0,    //gamma_max
  0,    //theta_max
  0,    //theta_min
  //Instance
  0,    //period
  0,    //alpha
  0,    //gamma
  0,    //theta

};
GateCtrl_t GC_LV3B =
{
  //Configuration
  PWM_CHL_LV3B,   //PWMchannel
  Fsw_max,  //frequency_max
  Fsw_min,  //frequency_min
  600000,   //deadtime_lead_ps_max
  100000,   //deadtime_lead_ps_min
  600000,   //deadtime_follow_ps_max
  0,    //deadtime_follow_ps_min
  19660,    //alpha_max
  19660,    //gamma_max
  4915,    //theta_max
  -4915,    //theta_min
  //Instance
  0,    //period
  0,    //alpha
  5000,    //gamma
  0,    //theta

}; //Theta pair

GateCtrl_t GC_LV4B =
{
  //Configuration
  PWM_CHL_LV4B,   //PWMchannel
  Fsw_max,  //frequency_max
  Fsw_min,  //frequency_min
  600000,   //deadtime_lead_ps_max
  100000,   //deadtime_lead_ps_min
  600000,   //deadtime_follow_ps_max
  0,    //deadtime_follow_ps_min
  19660,    //alpha_max
  19660,    //gamma_max
  0,    //theta_max
  0,    //theta_min
  //Instance
  0,    //period
  0,    //alpha
  5000,    //gamma
  0,    //theta

}; //Alpha pair
TemperatureSensor_t TS_Board = {
 0, //ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};
TemperatureSensor_t TS_Heatsink = {
 0, //ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};
VoltageSensor_t VS_vHV =
{
 ADC_CHL_V_HV, //ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};
VoltageSensor_t VS_vLV =
{
 ADC_CHL_V_LV,//ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};
VoltageSensor_t VS_vLV_Oring =
{
 ADC_CHL_V_LV_O,//ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};

CurrentSensor_t CS_iLVDischarge=
{
 ADC_CHL_I_LV_D,    //ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};
CurrentSensor_t CS_iLVCharge=
{
 ADC_CHL_I_LV_C,    //ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};
CurrentSensor_t CS_iHVCharge=
{
 ADC_CHL_I_HV,    //ADCchanel;
 0, //  filter;
 0, //simplefilterbits;
 0, //coefficient; //Q16
 0, //calibration; //Q16
 //Instance
 0, //valueQ12_raw;
 0, //valueQ12_filtered;
 0, //integer;
 0, //point;
};

PID_Struct_test_t vLV_Controller=
{
   1200*16, //hKp_Gain;
   512,     //hKp_Divisor;
   300*16,  //hKi_Gain;
   2048,    //hKi_Divisor;
   0,       //wLower_Limit_Output;     //Lower Limit for Output limitation
   65535,   //wUpper_Limit_Output;     //Lower Limit for Output limitation
   -((65536*2048)-1),   //wLower_Limit_Integral;   //Lower Limit for Integral term limitation
   ((65536*2048)-1),     //wUpper_Limit_Integral;   //Lower Limit for Integral term limitation
   0,       //wIntegral;
   0,       //wError;
   // Actually used only if DIFFERENTIAL_TERM_ENABLED is enabled
   0,       //hKd_Gain;
   0,       //hKd_Divisor;
   0,       //wPreviousError;
 };
PID_Struct_test_t vHV_Controller=
{
   1500*16, //hKp_Gain;
   256,     //hKp_Divisor;
   250*16,  //hKi_Gain;
   2048,    //hKi_Divisor;
   0,       //wLower_Limit_Output;     //Lower Limit for Output limitation
   65535,   //wUpper_Limit_Output;     //Lower Limit for Output limitation
   -((65536*2048)-1),   //wLower_Limit_Integral;   //Lower Limit for Integral term limitation
   ((65536*2048)-1),     //wUpper_Limit_Integral;   //Lower Limit for Integral term limitation
   0,       //wIntegral;
   0,       //wError;
   // Actually used only if DIFFERENTIAL_TERM_ENABLED is enabled
   0,       //hKd_Gain;
   0,       //hKd_Divisor;
   0,       //wPreviousError;
 };
PID_Struct_test_t iLVCharge_Controller=
{
   8000, //hKp_Gain;
   4096,     //hKp_Divisor;
   600,  //hKi_Gain;
   4096,    //hKi_Divisor;
   0,       //wLower_Limit_Output;     //Lower Limit for Output limitation
   65535,   //wUpper_Limit_Output;     //Lower Limit for Output limitation
   -((65536*4096)-1),   //wLower_Limit_Integral;   //Lower Limit for Integral term limitation
   ((65536*4096)-1),     //wUpper_Limit_Integral;   //Lower Limit for Integral term limitation
   0,       //wIntegral;
   0,       //wError;
   // Actually used only if DIFFERENTIAL_TERM_ENABLED is enabled
   0,       //hKd_Gain;
   0,       //hKd_Divisor;
   0,       //wPreviousError;
 };
PID_Struct_test_t iLVDischarge_Controller =
{
   750*16, //hKp_Gain;
   1024,     //hKp_Divisor;
   3600,  //hKi_Gain;
   4096,    //hKi_Divisor;
   0,       //wLower_Limit_Output;     //Lower Limit for Output limitation
   65535,   //wUpper_Limit_Output;     //Lower Limit for Output limitation
   -((65536*4096)-1),   //wLower_Limit_Integral;   //Lower Limit for Integral term limitation
   ((65536*4096)-1),     //wUpper_Limit_Integral;   //Lower Limit for Integral term limitation
   0,       //wIntegral;
   0,       //wError;
   // Actually used only if DIFFERENTIAL_TERM_ENABLED is enabled
   0,       //hKd_Gain;
   0,       //hKd_Divisor;
   0,       //wPreviousError;
 };

BurstModeCtrl_t BM_HVtoLV =
{
    //Configuration
    500,  //burstactive;     //Q16
    500,  //burstinactive;   //Q16
    1000,  //burstoff;        //Q16
    //instance
    0,  //ONOFF;
    0,  //burstmode;
};
BurstModeCtrl_t BM_LVtoHV =
{
    //Configuration
    250,  //burstactive;     //Q16
    250,  //burstinactive;   //Q16
    1000,  //burstoff;        //Q16
    //instance
    0,  //ONOFF;
    0,  //burstmode;
};

ChargeProfile_t CP_Charge =
{
    //Configuration
    (ChargeNotes_t*)&Charge_Notes, //noteAddr;
    Note_Charge_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};
ChargeProfile_t CP_Charge_V =
{
    //Configuration
    (ChargeNotes_t*)&Charge_Notes, //noteAddr;
    Note_Charge_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};
ChargeProfile_t CP_Charge_I =
{
    //Configuration
    (ChargeNotes_t*)&Charge_Notes, //noteAddr;
    Note_Charge_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};
ChargeProfile_t CP_Discharge =
{
    //Configuration
     (ChargeNotes_t*)&Discharge_Notes, //noteAddr;
     Note_Discharge_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};
ChargeProfile_t CP_UPS =
{
    //Configuration
     (ChargeNotes_t*)&UPS_Notes, //noteAddr;
     Note_UPS_MAX,
    //Instance
    0,  //note;
    0,  //time;
    0,  //time_tick;
    0,  //vref;
    0,  //iref;
};


Protection_t OVP_LV =
{

    //Configuration
    _ON,    //enable;
    3549,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OVP_Battery =
{

    //Configuration
    _ON,    //enable;
    3549,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t UVP_LV =
{

    //Configuration
    _ON,    //enable;
    1623,   //value_protect;
    2000,      //uplimit_Q16;
    0,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t UVP_Battery =
{

    //Configuration
    _ON,    //enable;
    3549,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OVP_HV_Charge =
{

    //Configuration
    _ON,    //enable;
    3631,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t UVP_HV_Charge =
{

    //Configuration
    _ON,    //enable;
    2955,   //value_protect;
    2000,      //uplimit_Q16;
    0,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OCP_Charge =
{

    //Configuration
    _ON,    //enable;
    3617,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OCP_Discharge =
{

    //Configuration
    _ON,    //enable;
    3490,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OVP_HV_UPS =
{

    //Configuration
    _ON,    //enable;
    3631,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OVP_HV_Discharge =
{

    //Configuration
    _ON,    //enable;
    3631,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;

};
Protection_t OTP_Heatsink = {

    //Configuration
    _ON,    //enable;
    105,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;
};
Protection_t OTP_Board = {

    //Configuration
    _ON,    //enable;
    100,   //value_protect;
    0,      //uplimit_Q16;
    2000,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;
};

Protection_t OCP_HW = {

    //Configuration
    _ON,    //enable;
    0,   //value_protect;
    0,      //uplimit_Q16;
    0,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;
};
Protection_t OVP_HW = {

    //Configuration
    _ON,    //enable;
    0,   //value_protect;
    0,      //uplimit_Q16;
    0,   //lowlimit_Q16;
    //instance
    0,      //value_uplimit;
    0,      //value_lowlimit;
    0,      //fault;
};


Oring_t OR_Battery =
{

    //Configuration
    50,     //lowthreshold;
    20,     //upthreshold;
    50,     //uplimit;
    //Instance
    0,      //status;

} ;
*/
