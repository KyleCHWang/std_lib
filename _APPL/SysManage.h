/*
 * SysMangage.h
 *
 *  Created on: April 1, 2020
 *      Author: KYLE.CH.WANG
 */

#ifndef SYS_MANAGE_H_
#define SYS_MANAGE_H_

#include "TypeDef.h"
#include "MCAL_UART.h"
//User Definition=======================================================
#define L_100us 1
#define L_1ms 10
#define L_50ms 500
#define LOOP_NUM 10
//======================================================================

typedef struct
 {
    UINT8  tick;
    UINT16 timer[LOOP_NUM];
   
 } SysManage_t;

typedef enum{
    _char,
    _usigned_char,
    _short,
    _unsigned_short,
    _long,
    _unsigned_long,

}Varable_Type_t;
 
extern void API_SYS_Loop(SysManage_t* tempinstance);
extern void API_SYS_Set_tick(SysManage_t* tempinstance);
extern void API_SYS_Loop0(SysManage_t* tempinstance);
extern void API_SYS_Loop1(SysManage_t* tempinstance);
extern void delay_us(UINT32 cnt);
extern void delay_ms(UINT32 cnt);

extern void _accumlate(UINT8* ptr_in, Varable_Type_t type);

#define API_SYS_Plus(var) _accumlate((UINT8*)&var, (Varable_Type_t)type_idx(var))


#endif /* System_Management_H_ */
