/*
 * Object.h
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */

#ifndef COMPONENT_OBJECT_H_
#define COMPONENT_OBJECT_H_

#include "ChargerCANProtocol.h"
#include "VoltageRegulator.h"
#include "CurrentSensor.h"
#include "VoltageSensor.h"
#include "TempSensor.h"
#include "Diagnostic.h"
#include "ChargeProfile.h"
#include "ChargerSM.h"
#include "Oring.h"
#include "MCAL_GPIO.h"
#include "Protection.h"
#include "SysManage.h"
#include "EasyUART.h"
#include "BLEUART.h"
#include "ChargeCharacteristic.h"
#include "Calibration.h"

#define NOCOMPILE 0

#define CAL_ADDR    	(0x0801E000) /* Base @ of Page 31, 4 Kbytes */
#define PROFILE_ADDR    (0x0801F000) /* Base @ of Page 31, 4 Kbytes */

#define FREQ_TEST 0
#define Fsw_min 86250
#define Fsw_max 400000
#define EMI_TEST 0
#define EE_TEST 0
//#define DIAGCOMM_ENABLE
#define CHARGER_POWER (720.0f + 2.7f) //Charger功率需求+DC bus到connector的功耗 = 722.7W
#define V_AC_DERATE 97.0f
#define V_AC_MAX_DERATE 0.85f
#define T_DERATE 90.0f
#define MAX_DERATE 0.3f
#define POWER_DERATE_SLOPE 0.034f
#define POWER_RATE_SLOPE 0.006f
#define CableResistance 0.015f

//Theory=====================================================
//#define V_DCBUS_coe_A  46.85575758f
//#define V_DCBUS_coe_B  2015.976727f
//
//#define V_BAT_coe_A  46.85575758f
//#define V_BAT_coe_B  2015.976727f
//
//
//#define I_coe_A 61.93648485f
//#define I_coe_B 618.7194182f
//
//#define Vo_coe_A 1518.2275f
//#define Vo_coe_B 0.0f
//
//#define Io_coe_A 990.9686364f
//#define Io_coe_B 9899.359636f

//S5 Label S/N: XXXC2035000003====================================
//#define V_DCBUS_coe_A  47.06927176f
//#define V_DCBUS_coe_B  2010.920071f
//
//#define V_BAT_coe_A  46.98780921f
//#define V_BAT_coe_B  2010.044857f
//
//#define I_coe_A 62.29793824f
//#define I_coe_B 632.8864002f
//
//#define Vo_coe_A 1537.122558f
//#define Vo_coe_B -68.98756661f
//
//#define Io_coe_A 1009.621527f
//#define Io_coe_B 10315.73261f

//S7 Label S/N: XXXC2035000002=====================================
//#define V_DCBUS_coe_A  46.72619048f
//#define V_DCBUS_coe_B  2013.014881f
//
//#define V_BAT_coe_A  46.60714286f
//#define V_BAT_coe_B  2017.700714f
//
//#define I_coe_A 61.94912068f
//#define I_coe_B 620.9197126f
//
//#define Vo_coe_A 1540.833333f
//#define Vo_coe_B -46.25833333f
//
//#define Io_coe_A 1003.425373f
//#define Io_coe_B 10164.34283f

//#define Io_coe_B 8453.023798f

////S8 Label S/N: XXXC2035000001===================================== 
//#define V_DCBUS_coe_A  46.56891496f
//#define V_DCBUS_coe_B  2012.396481f
//
//#define V_BAT_coe_A  46.699912f
//#define V_BAT_coe_B  2008.514872f
//
//#define I_coe_A 61.7587226f
//#define I_coe_B 516.8387134f
//
//#define Vo_coe_A 1522.697947f
//#define Vo_coe_B -24.6228739f
//
//#define Io_coe_A 1000.882816f
//#define Io_coe_B 8453.023798f


//S15=====================================================
#define V_DCBUS_coe_A  46.70687005f
#define V_DCBUS_coe_B  2011.157503f

#define V_BAT_coe_A  46.62721893f
#define V_BAT_coe_B  2008.27929f


#define I_coe_A 61.14167085f
#define I_coe_B 604.7716079f

#define Vo_coe_A 1527.218935f
#define Vo_coe_B -33.71005917f

#define Io_coe_A 990.0155252f
#define Io_coe_B 9911.693996f

typedef enum{

	Charger_100,
	Charger_90,
	Charger_80,
	Charger_70,
	Charger_0,
	
}ChargerDerating_t;
	
typedef enum{

	Charger_Normal,
    Charger_STOP,
    Charger_STOP_MAX,
    
} Charger_STOP_t;


typedef enum{

	System_OFF,
    System_ON,
    System_ONOFF_MAX,
    
} System_ONOFF_t;


typedef enum{

    Mode_AUX,
    Mode_Charge,
    Mode_MAX,

} ChargerSystemMode_t;

typedef enum{
    Mode_CC,
    Mode_CV,

}ControllerMode_t;
typedef enum{

	System_Charger,
    System_ATS,
    System_MAX
    
} System_MODE_t;

typedef struct
{
	System_MODE_t System;
    System_ONOFF_t ONOFF;
    ChargerSystemMode_t mode;
    ControllerMode_t controller;
	UINT16 ChargeCurrentLevel;
	enum_BatteryProfile ChargeProfile;
    UINT64 SN;
    float vref_LV;
    float iref_LV_Charge;
	float iref_LV_Charge_Derating;
	UINT8 fault;
	UINT8 batteryfault;
	float derating;
	UINT16 chargetime;

} ChargerSystem_t;

typedef struct
{

    Charger_STOP_t ChargerSTOP;
    ChargerSystemMode_t mode;
    ControllerMode_t controller;
	//used in charge profile control
	UINT32 ChargeCurrent;		
	//used only Vcmd,Icmd control
    float vref_LV;
    float iref_LV_Charge;		

} ChargerControl_t;

typedef struct
{
	union
	{
		unsigned char byte;
		struct
		{
		    unsigned char UC1_RELAY:1;
		    unsigned char UC1_PFC_EN:1;
		    unsigned char EN_LLC:1;
		} bits;
	} GPO;
	union
	{
		unsigned char byte;
		struct
		{
		    unsigned char AC_UVP:1;
		    unsigned char PFC_OVP:1;
			unsigned char PFC_UVP:1;
		} bits;
	} Fault;
	union
	{
		unsigned char byte;
		struct
		{
		    unsigned char AC_UVP_EN:1;

		} bits;
	} Protect_EN;
	UINT16 ADC_AC_POWER_DET;
	UINT16 ADC_AC_POWER_DET_AVG;
	UINT16 ADC_VBoost_DET;
	UINT16 ADC_T_PFCMOS;
	UINT16 ADC_T_LLCMOS;
	UINT16 ADC_T_BRIDGE;

} PMIC_t;



extern Oring_t OR_Battery;
extern ChargeProfile_t CP_V_Charge;
extern ChargeProfile_t CP_I_Charge;
extern ChargerSystem_t System;
extern ChargerControl_t Control;
extern ChargerSM_t ChargerSM;
extern CCANP_t CCANP_Battery;

extern VoltageSensor_t VS_vHV_PFC;
extern VoltageSensor_t VS_vAC_L;
extern VoltageSensor_t VS_vLV_BAT;
extern VoltageSensor_t VS_vLV_DCBUS;
extern CurrentSensor_t CS_iLV_Charge;
extern TemperatureSensor_t TS_LLC;
extern TemperatureSensor_t TS_ORING;
extern TemperatureSensor_t TS_EXT;
extern TemperatureSensor_t TS_PRI_LLC;
extern TemperatureSensor_t TS_PRI_PFC;
extern TemperatureSensor_t TS_PRI_BRIDGE;

extern Diagnostic_t DIAG_converter;
extern VoltageRegulator_t VR_IADJ;
extern VoltageRegulator_t VR_VADJ;
extern Protection_t BAT_REV;
extern Protection_t Vin_UVP;
extern Protection_t PFC_OVP;
extern Protection_t PFC_UVP;
extern Protection_t HW_Protect;
extern Protection_t SW_OVP_LV;
extern Protection_t SW_UVP_LV;
extern Protection_t SW_OCP_LV;
extern Protection_t OTP;
extern SysManage_t SYS;
extern PMIC_t PMIC;
extern EasyUART_t PRI_COMM;
extern BLEUART_t BLE_COMM;
extern const UINT16 LLC_NTC_table[166];
extern const UINT16 LLC_NTC_table_4120[166];
extern UINT64 ChargerConfig[10];
extern CalibrationData_t CalibrationDelta;

#endif /* COMPONENT_OBJECT_H_ */
