#ifndef CCANP_H
#define CCANP_H

#include "TypeDef.h"
#include "MCAL_CAN.h"

typedef enum
{
    CCANP_RxID_Ctrl,      
    CCANP_RxID_Reserved0,		
    CCANP_RxID_Reserved1,        
    CCANP_RxID_MAX,

} CCANP_RxID_t;

typedef enum
{
    CCANP_TxID_Info,      
    CCANP_TxID_SN,		
    CCANP_TxID_Debug0,      
    CCANP_TxID_Debug1,
    CCANP_TxID_Debug2,
    CCANP_TxID_Debug3,
    CCANP_TxID_Debug4,
    CCANP_TxID_Debug5,
    CCANP_TxID_Debug6,
    CCANP_TxID_Debug7,
    CCANP_TxID_Debug8,
    CCANP_TxID_Debug9,
    CCANP_TxID_Debug10,
    CCANP_TxID_Debug11,
    CCANP_TxID_Debug12,
    CCANP_TxID_Debug13,
    CCANP_TxID_Debug14,
    CCANP_TxID_Debug15,
    CCANP_TxID_MAX,

} CCANP_TxID_t;

typedef enum
{
    CCANP_IDLE,      //CAN device idle
    CCANP_INIT,		 //CAN device INIT	
    CCANP_RUN,       //CAN communication is active
    CCANP_MAX,

} CCANP_State_t;

typedef struct{
    //configuration
    //instance
	UINT32 				id;
	CAN_IDType_t 		idtype;
	UINT16 				datalength;
	CAN_FrameType_t 	frame;
} CCANP_Header_t;

typedef struct{
    //configuration
    //instance
    UINT8 RxData[8];
	CCANP_Header_t RxHeader;
} CCANP_Rxframe_t;
	
typedef struct{
    //configuration
    //instance
    UINT8 transmit:1;
	UINT8 TxData[8];
	CCANP_Header_t TxHeader;
} CCANP_Txframe_t;


typedef struct{
    //configuration
    enumCAN_CHL_t channel;
    //instance
    CCANP_State_t state;
    CCANP_State_t stateold;
    CCANP_State_t statenext;
    UINT8 faultbuffer[2];
    UINT32 delaycnt[10];
	CCANP_Rxframe_t Rxframe;
	CCANP_Txframe_t Txframe[CCANP_TxID_MAX];
	UINT8 fault;
	UINT32 timeoutcnt;

} CCANP_t;

extern void UDF_CCANP_Init(CCANP_t* tempCCANP);
extern void API_CCANP_RxDecoder(CCANP_t * tempCCANP);
extern void API_CCANP_Task(CCANP_t * tempCCANP);
extern void API_CCANP_Transmit(CCANP_t * tempCCANP);

#endif
