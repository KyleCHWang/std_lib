/*
 * EasyUART.c
 *
 *  Created on: Jun 24, 2019
 *      Author: KYLE.CH.WANG
 */
#include "TypeDef.h"
#include "SysManage.h"
#include "MCAL_GPIO.h"
#include "MCAL_ADC.h"
#include "MCAL_CAN.h"
#include "Object.h"
UINT16 tempCV =0;
UINT16 tempCC =0;

extern UINT8 FaultCheck(void);

extern volatile UINT32 ADCresultarray[ADC_CHL_MAX];
extern BF_U8 faultbuffer[2];

float VS_vLV_DCBUS_avg = 0;
float VS_vLV_BAT_avg = 0;
float CS_iLV_Charge_avg = 0;
float T_LLC_avg = 0;
float TS_ORING_avg = 0;
float TS_EXT_avg = 0;
float VS_vHV_PFC_avg = 0;
float VS_vAC_L_avg = 0;
float TS_PRI_LLC_avg = 0;
float TS_PRI_PFC_avg = 0;
float TS_PRI_BRIDGE_avg = 0;

inline void delay_us(UINT32 cnt)
{
	volatile UINT64 i=9*cnt;

	while(i)
		i--;
}
// delay time: ms
inline void delay_ms(UINT32 cnt)
{
	volatile UINT64 i=8386*cnt;

	while(i)
		i--;
}

void _accumlate(UINT8* ptr_in, Varable_Type_t type){
        
    switch(type){
        case _char:
        {
            INT8 *ptr = (INT8*)ptr_in;
            if(*ptr < 127){
                *ptr = *ptr + 1;
            }
            
            break;
        }
        case _usigned_char:
        {
            UINT8 *ptr = (UINT8*)ptr_in;
            if(*ptr < 255){
                *ptr = *ptr + 1;
            }
            
            break;
        }
        case _short:
        {
            INT16* ptr = (INT16*)ptr_in;
            if(*ptr < 32767){
                *ptr = *ptr + 1;
            }
            break;
        }
        case _unsigned_short:
        {
            UINT16* ptr = (UINT16*)ptr_in;
            if(*ptr < 65535){
                *ptr = *ptr + 1;
            }
            break;
        }
        case _long:
        {
            INT32* ptr = (INT32*)ptr_in;
            if(*ptr < 2147483647){
                *ptr = *ptr + 1;
            }
            break;
        }
        case _unsigned_long:
        {
            UINT32* ptr = (UINT32*)ptr_in;
            if(*ptr < 4294967295){
                *ptr = *ptr + 1;
            }
            break;
        }
    }
    
}   


void API_SYS_Set_tick(SysManage_t* tempinstance){
	tempinstance->tick = 1;
}
void API_SYS_Loop(SysManage_t* tempinstance){
	UINT8 cnt = 0;
	if(tempinstance->tick){
		tempinstance->tick = 0;
		for(cnt=0;cnt<LOOP_NUM;cnt++){
			tempinstance->timer[cnt]++;
		}	
	}
	if(tempinstance->timer[0] >= L_100us){
		tempinstance->timer[0] = 0;
		API_Protect_resetFlag(&HW_Protect, DAT_GPIO_HW_PROTECT,System.fault);
	}
	if(tempinstance->timer[1] >= L_1ms){
		tempinstance->timer[1] = 0;
	  	//Data processing==============================================================================================
	  	API_EZUART_RxTask(&PRI_COMM);
		API_BLEUART_RxTask(&BLE_COMM);
	    VS_vLV_DCBUS.valueQ12_raw = ADCresultarray[ADC_CHL_VOUT_DET];
	    VS_vLV_BAT.valueQ12_raw = ADCresultarray[ADC_CHL_BAT_DET];
	    CS_iLV_Charge.valueQ12_raw = ADCresultarray[ADC_CHL_IOUT_DET];
	    TS_LLC.valueQ12_raw = ADCresultarray[ADC_CHL_NTC_LLC]; 
		TS_ORING.valueQ12_raw = ADCresultarray[ADC_CHL_NTC_RELAY];
		TS_EXT.valueQ12_raw = ADCresultarray[ADC_CHL_NTC_EXT];
		VS_vHV_PFC.valueQ12_raw = PMIC.ADC_VBoost_DET;
		VS_vAC_L.valueQ12_raw = PMIC.ADC_AC_POWER_DET_AVG;
		TS_PRI_LLC.valueQ12_raw = PMIC.ADC_T_LLCMOS;
		TS_PRI_PFC.valueQ12_raw = PMIC.ADC_T_PFCMOS;
		TS_PRI_BRIDGE.valueQ12_raw = PMIC.ADC_T_BRIDGE;

	
		VS_vLV_DCBUS_avg = VS_vLV_DCBUS_avg + ((VS_vLV_DCBUS.valueQ12_raw - VS_vLV_DCBUS_avg)/150.0f);
		VS_vLV_DCBUS.valueQ12_filtered = (UINT16)VS_vLV_DCBUS_avg;

		VS_vLV_BAT_avg = VS_vLV_BAT_avg + ((VS_vLV_BAT.valueQ12_raw - VS_vLV_BAT_avg)/150.0f);
		VS_vLV_BAT.valueQ12_filtered = (UINT16)VS_vLV_BAT_avg;

		CS_iLV_Charge_avg = CS_iLV_Charge_avg + ((CS_iLV_Charge.valueQ12_raw - CS_iLV_Charge_avg)/150.0f);
		CS_iLV_Charge.valueQ12_filtered = (UINT16)CS_iLV_Charge_avg;	
		
 		T_LLC_avg = T_LLC_avg + ((TS_LLC.valueQ12_raw - T_LLC_avg)/1500.0f);
		TS_LLC.valueQ12_filtered = (UINT16)T_LLC_avg;

		TS_ORING_avg = TS_ORING_avg + ((TS_ORING.valueQ12_raw - TS_ORING_avg)/1500.0f);
		TS_ORING.valueQ12_filtered = (UINT16)TS_ORING_avg;

		TS_EXT_avg = TS_EXT_avg + ((TS_EXT.valueQ12_raw - TS_EXT_avg)/1500.0f);
		TS_EXT.valueQ12_filtered = (UINT16)TS_EXT_avg;

		VS_vHV_PFC_avg = VS_vHV_PFC_avg + ((VS_vHV_PFC.valueQ12_raw - VS_vHV_PFC_avg)/1500.0f);
		VS_vHV_PFC.valueQ12_filtered = (UINT16)VS_vHV_PFC_avg;

		VS_vAC_L.valueQ12_filtered = VS_vAC_L.valueQ12_raw;	

		TS_PRI_LLC_avg = TS_PRI_LLC_avg + ((TS_PRI_LLC.valueQ12_raw - TS_PRI_LLC_avg)/1500.0f);
		TS_PRI_LLC.valueQ12_filtered = (UINT16)TS_PRI_LLC_avg;	

		TS_PRI_PFC_avg = TS_PRI_PFC_avg + ((TS_PRI_PFC.valueQ12_raw - TS_PRI_PFC_avg)/1500.0f);
		TS_PRI_PFC.valueQ12_filtered = (UINT16)TS_PRI_PFC_avg;

		TS_PRI_BRIDGE_avg = TS_PRI_BRIDGE_avg + ((TS_PRI_BRIDGE.valueQ12_raw - TS_PRI_BRIDGE_avg)/1500.0f);
		TS_PRI_BRIDGE.valueQ12_filtered = (UINT16)TS_PRI_BRIDGE_avg;
		API_VS_VoltUpdate(&VS_vAC_L);
		API_VS_VoltUpdate(&VS_vLV_DCBUS);
		API_CS_AmpereUpdate(&CS_iLV_Charge);
		API_VS_VoltUpdateCompensation(&VS_vLV_BAT,API_CS_Get_Ampere(&CS_iLV_Charge));
		API_TS_Update(&TS_LLC);
		API_TS_Update(&TS_ORING);
		API_TS_Update(&TS_EXT);
		API_TS_Update(&TS_PRI_LLC);
		API_TS_Update(&TS_PRI_PFC);
		API_TS_Update(&TS_PRI_BRIDGE);
	  //Protection===================================================================================================
		//API_Protect_upHysteresis(&Vin_VF, float Input);
		//API_Protect_upHysteresis(&Vin_OFP, float Input);
		//API_Protect_lowHysteresis(&Vin_UFP, float Input);
		//API_Protect_setFlag(&PFC_F, UINT8 flag); 
		
		API_Protect_setFlag(&Vin_UVP,PMIC.Fault.bits.AC_UVP,System.fault);
		API_Protect_upHysteresis(&PFC_OVP,PMIC.ADC_VBoost_DET,System.fault);
		API_Protect_lowHysteresis(&PFC_UVP,PMIC.ADC_VBoost_DET,System.fault); 
		//API_Protect_resetFlag(&HW_Protect, DAT_GPIO_HW_PROTECT,System.fault);
		API_Protect_upHysteresis(&SW_OVP_LV, VS_vLV_BAT.Volt,System.fault);
		API_Protect_lowHysteresis(&SW_UVP_LV, VS_vLV_BAT.Volt,System.fault);
		API_Protect_lowHysteresis(&BAT_REV,VS_vLV_BAT.valueQ12_filtered,0);
		API_Protect_upHysteresis(&SW_OCP_LV, CS_iLV_Charge.Amp,System.fault);
	 	API_Protect_upHysteresis(&OTP, TS_LLC.Cels,System.fault);
		
		if(FaultCheck()){		
			CCANP_Battery.faultbuffer[0] |= Vin_UVP.fault<<7|PFC_OVP.fault<<6|PFC_UVP.fault<<5|HW_Protect.fault<<3|System.batteryfault;
			CCANP_Battery.faultbuffer[1] |= BAT_REV.fault<<7|SW_OVP_LV.fault<<6|SW_UVP_LV.fault<<5|SW_OCP_LV.fault<<4|OTP.fault<<3;
	 		System.vref_LV = 0.0f;
			System.iref_LV_Charge = 0.0f;
			if(System.ONOFF==System_ON){
				System.fault = 1;
			}
		}
  		API_CSM_Task();
  		API_CCANP_Task(&CCANP_Battery);
	  //output=======================================================================================================	
		API_VR_Output(&VR_VADJ, (System.vref_LV));
		API_VR_Output(&VR_IADJ, (System.iref_LV_Charge));
		//API_VR_Set_out(&VR_VADJ,52428);
		//API_VR_Set_out(&VR_IADJ,52428);
		API_EZUART_TxTask(&PRI_COMM);
		API_BLEUART_TxTask(&BLE_COMM);

	}
	if(tempinstance->timer[2] >= L_50ms){
		tempinstance->timer[2] = 0;
		
	}
	
	API_CCANP_Transmit(&CCANP_Battery);
    API_CAN_AutoReInit(CAN_CHL_BAT);
	
}

