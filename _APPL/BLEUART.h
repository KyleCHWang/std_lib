/*
 * BLEUART.h
 *
 *  Created on: APR 28
 *      Author: KYLE.CH.WANG
 */

#ifndef BLE_UART_H_
#define BLE_UART_H_

#include "TypeDef.h"
#include "MCAL_UART.h"

//BSW USER configuration=============================================
#define BLE_RX_TIMEOUT 6600
#define BLE_TX_PERIOD 400
#define BLE_TX_DELAY 3000
#define BLE_TX_TestIndex 4000
#define BLE_UART_RXBYTE_MAX 11
#define BLE_UART_TXBYTE_MAX 66
//==============================================================

typedef struct
 {
   //Configuration
   enumUART_CHL_t UARTchannel;
   //Instance
   UINT8 rxdatabuffer[BLE_UART_RXBYTE_MAX];
   UINT8 txdatabuffer[BLE_UART_TXBYTE_MAX];
   UINT16 delaycnt[4];
 } BLEUART_t;

extern void API_BLEUART_Config_UARTChannel(BLEUART_t* tempinstance, enumUART_CHL_t tempU8);
extern void API_BLEUART_RxTask(BLEUART_t* tempinstance);
extern void API_BLEUART_TxTask(BLEUART_t* tempinstance);

#endif /* COMPONENT_DIAGNOSTIC_H_ */
